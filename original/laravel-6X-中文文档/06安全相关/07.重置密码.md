## 重置密码

Laravel 6 中文文档 /  

本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 重置密码

+   [简介](#introduction)
+   [数据库注意事项](#resetting-database)
+   [路由](#resetting-routing)
+   [视图](#resetting-views)
+   [重置密码后](#after-resetting-passwords)
+   [自定义](#password-customization)

## 简介

> Tip：**想要快速上手？** 只需要在新建的 Laravel 应用中使用 Composer 安装 `laravel/ui` 拓展包并且运行 `php artisan ui vue --auth`， 然后在浏览器中打开 `http://your-app.test/register` 或者给你的应用分配任意一个 URL。 这个命令将会负责搭建起整个身份验证系统，包括重置密码！

大多数 web 应用都为用户提供了重置密码的功能。相较于强迫你在每个应用中都要重新实现一遍此功能，Laravel 提供了便捷的方法来发送密码提醒以及执行密码重置。

> 注意：在使用 Laravel 的密码重置功能之前，你的用户模型必须使用 `Illuminate\Notifications\Notifiable` trait。

## 数据库注意事项

首先，验证你的 `App\User` 模型是否实现了 `Illuminate\Contracts\Auth\CanResetPassword` 契约。当然，框架中包含的 App\\User 模型已经实现了该接口，并且使用了 `Illuminate\Auth\Passwords\CanResetPassword` trait来包含实现该接口所需的方法。

#### 生成重置令牌的表迁移

接下来，必须创建一张数据表来存储密码重置令牌。该数据表的迁移已包含在 Laravel 应用的 `database/migrations` 目录中。 所以，你需要做的只是执行数据库迁移命令:

```php
php artisan migrate
```

## 路由

Laravel 已在 `Auth\ForgotPasswordController` 和 `Auth\ResetPasswordController` 类中包含了发送密码重置链接电子邮件和重置用户密码的逻辑。 所有重置密码需要的路由都可以通过 Composer 的 `laravel/ui` 拓展包生成：

```php
composer require laravel/ui

php artisan ui vue --auth
```

## 视图

要生成重置密码必要的视图文件, 你可以使用 Composer 的`laravel/ui` 拓展包：

```php
composer require laravel/ui

php artisan ui vue --auth
```

这些视图在 `resources/views/auth/passwords` 中。 你可以随意根据需要为您的应用程序自定义它们。

## 重置密码后

一旦你定义了重置用户密码的路由和视图，你可以在浏览器中访问 `/password/reset` 这个路由来重置密码。框架中的 `ForgotPasswordController` 已包含发送密码重置链接电子邮件的逻辑，同时 `ResetPasswordController` 包含了重置用户密码的逻辑。

在重置密码后，用户将会自动登录并重定向到 `/home`。 你可以通过在 `ResetPasswordController` 中定义一个 `redirectTo` 属性来自定义密码重置后重定向的位置:

```php
protected $redirectTo = '/dashboard';
```

> 注意：默认情况下，密码重置令牌会在一小时后过期。你可以通过 `config/auth.php` 文件中的密码重置 `expire` 选项对此进行修改。

## 自定义

#### 自定义身份验证看守器

在你的 `auth.php` 配置文件中，你可以配置多个「看守器」，可以用来定义多个用户表的身份验证行为。你可以自定义框架中的 `ResetPasswordController` ，通过重写该控制器中的 `guard` 方法来使用你所选择的看守器。这个方法应当返回一个看守器实例：

```php
use Illuminate\Support\Facades\Auth;

/**
 * 在密码重置期间看守
 *
 * @return \Illuminate\Contracts\Auth\StatefulGuard
 */
protected function guard()
{
    return Auth::guard('guard-name');
}
```

#### 自定义密码代理

在你的 `auth.php` 配置文件中，你可以配置多个密码「代理」，可以用来重置多个用户表上的密码。你可以自定义框架中的 `ForgotPasswordController` 和 `ResetPasswordController` ， 通过重写控制器中的 `broker` 方法来使用你所选择的代理：

```php
use Illuminate\Support\Facades\Password;

/**
 * 获取在密码重置期间使用的代理。
 *
 * @return PasswordBroker
 */
public function broker()
{
    return Password::broker('name');
}
```

#### 自定义密码重置邮件

你可以轻松地修改用于向用户发送密码重置链接的通知类。首先，重写 `User` 模型中的 `sendPasswordResetNotification` 方法。 在此方法中，你可以使用任何你所选择的通知类来发送通知。该方法接收的第一个参数是密码重置令牌 `$token` ：

```php
/**
 * 发送密码重置通知。
 *
 * @param  string  $token
 * @return void
 */
public function sendPasswordResetNotification($token)
{
    $this->notify(new ResetPasswordNotification($token));
}
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/pas...](https://learnku.com/docs/laravel/6.x/passwords/5157)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/pas...](https://learnku.com/docs/laravel/6.x/passwords/5157)