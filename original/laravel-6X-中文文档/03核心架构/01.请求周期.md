本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 请求生命周期

+   [介绍](#introduction)
+   [生命周期概述](#lifecycle-overview)
+   [聚焦服务提供者](#focus-on-service-providers)

## 介绍

在「日常生活」中 使用任何工具时，如果理解了该工具的工作原理。那么用起来就会更加得心应手。应用开发也是如此，当你能真正懂得一个功能背后实现原理时，用起来会更加顺手，方便。

文档存在目的是为了让你更加清晰地了解 Laravel 框架是如何工作。通过框架进行全面了解，让一切都不再感觉很「神奇」。相信我，这有助于你更加清楚自己在做什么，对自己想做的事情更加胸有成竹。就算你不明白所有的术语，也不用因此失去信心！只要多一点尝试、学着如何运用，随着你浏览文档的其他部分，你的知识一定会因此增长。

## 生命周期概述

### 首先

Laravel 应用的所有请求入口都是 `public/index.php` 文件。而所有的请求都是经由你的 Web 服务器（Apache/Nginx）通过配置引导到这个文件。 `index.php` 文件代码并不多，但是，这里是加载框架其它部分的起点。

`index.php` 文件加载 Composer 生成的自动加载设置，然后从 `bootstrap/app.php` 脚本中检索 Laravel 应用程序的实例。 Laravel 本身采取的第一个动作是创建一个应用程序 / [服务容器](https://learnku.com/docs/laravel/6.x/container)。

### HTTP / Console 内核

接下来， 根据进入应用程序的请求类型来将传入的请求发送到 HTTP 内核或控制台内核。而这两个内核是用来作为所有请求都要通过的中心位置。 现在，我们先看看位于 `app/Http/Kernel.php` 中的 HTTP 内核。

HTTP 内核继承了 `Illuminate\Foundation\Http\Kernel` 类，该类定义了一个 `bootstrappers` 数组。 这个数组中的类在请求被执行前运行，这些 bootstrappers 配置了错误处理， 日志， [检测应用环境](https://learnku.com/docs/laravel/6.x/configuration#environment-configuration)，以及其它在请求被处理前需要执行的任务。

HTTP 内核还定义了所有请求被应用程序处理之前必须经过的 HTTP [中间件](https://learnku.com/docs/laravel/6.x/middleware) ，这些中间件处理 HTTP 会话 读写、判断应用是否处于维护模式、 [验证 CSRF 令牌](https://learnku.com/docs/laravel/6.x/csrf) 等等。

HTTP 内核的 `handle` 方法签名相当简单：获取一个 `Request` ，返回一个 `Response`。可以把该内核想象作一个代表整个应用的大黑盒子，输入 HTTP 请求，返回 HTTP 响应。

#### 服务提供者

内核启动操作中最重要的便是你应用的 [服务提供者](https://learnku.com/docs/laravel/6.x/providers) 了。所有应用下的服务提供者均配置到了 `config/app.php` 配置文件中的 `providers` 数组中。 第一步，所有服务提供者的 `register` 方法会被调用，然后一旦所有服务提供者均注册后， `boot` 方法才被调用。

服务提供者给予框架开启多种多样的组件，像数据库，队列，验证器，以及路由组件。只要被启动服务提供者就可支配框架的所有功能，所以服务提供者也是 Laravel 整个引导周期最重要组成部分。

#### 请求调度

一旦启动且所有服务提供者被注册，`Request` 会被递送给路由。路由将会调度请求，交给绑定的路由或控制器，也当然包括路由绑定的中间件。

## 聚焦服务提供者

服务提供者是 Laravel 真正意义的生命周期中的关键。应用实例一旦创建，服务提供者就被注册，然后请求被启动的应用接管。简单吧！

牢牢掌握服务提供者的构建和其对 Laravel 应用处理机制的原理是非常有价值的。当然，你的应用默认的服务提供会存放在 `app/Providers` 下面。

默认的， `AppServiceProvider` 是空白的。这个提供者是一个不错的位置，用于你添加应用自身的引导处理和服务容器绑定。当然，大型项目中，你可能希望创建数个粒度更精细的服务提供者。

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/lif...](https://learnku.com/docs/laravel/6.x/lifecycle/5130)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/lif...](https://learnku.com/docs/laravel/6.x/lifecycle/5130)