本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## Configuration

+   [介绍](#introduction)
+   [环境配置](#environment-configuration)
    +   [环境变量类型](#environment-variable-types)
    +   [检索环境配置](#retrieving-environment-configuration)
    +   [确定当前环境](#determining-the-current-environment)
    +   [在调试页面隐藏环境变量](#hiding-environment-variables-from-debug)
+   [访问配置值](#accessing-configuration-values)
+   [配置缓存](#configuration-caching)
+   [维护模式](#maintenance-mode)

## 介绍

Laravel 框架的所有配置文件都保存在 `config` 目录中。每个选项都有说明，你可随时查看这些文件并熟悉都有哪些配置选项可供你使用。

## 环境配置

对于应用程序运行的环境来说，不同的环境有不同的配置通常是很有用的。 例如，你可能希望在本地使用的缓存驱动不同于生产服务器所使用的缓存驱动。

Laravel 利用 Vance Lucas 的 PHP 库 [DotEnv](https://github.com/vlucas/phpdotenv) 使得此项功能的实现变得非常简单。在新安装好的 Laravel 应用程序中，其根目录会包含一个 `.env.example` 文件。如果是通过 Composer 安装的 Laravel，该文件会自动更名为 `.env`。否则，需要你手动更改一下文件名。

你的 `.env` 文件不应该提交到应用程序的源代码控制系统中，因为每个使用你的应用程序的开发人员 / 服务器可能需要有一个不同的环境配置。此外，在入侵者获得你的源代码控制仓库的访问权的情况下，这会成为一个安全隐患，因为任何敏感的凭据都被暴露了。

如果是团队开发，则可能希望应用程序中仍包含 `.env.example` 文件。因为通过在示例配置文件中放置占位值，团队中的其他开发人员可以清楚地看到哪些环境变量是运行应用程序所必需的。你也可以创建一个 `.env.testing` 文件，当运行 PHPUnit 测试或以 `--env=testing` 为选项执行 Artisan 命令时，该文件将覆盖 `.env` 文件中的值。

> Tip：`.env` 文件中的所有变量都可被外部环境变量（比如服务器级或系统级环境变量）所覆盖。

### 环境变量类型

`.env` 文件中的所有变量都被解析为字符串，因此创建了一些保留值以允许你从 `env()` 函数中返回更多类型的变量：

| `.env` 值 | `env()` 值 |
| --- | --- |
| true | (bool) true |
| (true) | (bool) true |
| false | (bool) false |
| (false) | (bool) false |
| empty | (string) ‘’ |
| (empty) | (string) ‘’ |
| null | (null) null |
| (null) | (null) null |

如果你需要使用包含空格的值定义环境变量，可以通过将值括在双引号中来实现。

```php
APP_NAME="我的 应用"
```

### 检索环境配置

当应用程序收到请求时，`.env` 文件中列出的所有变量将被加载到 PHP 的超级全局变量 `$_ENV` 中。你可以使用 `env` 函数检索这些变量的值。事实上，如果你查看 Laravel 的配置文件，你就能注意到有数个选项已经使用了这个函数：

```php
'debug' => env('APP_DEBUG', false),
```

传递给 `env` 函数的第二个值是「默认值」。如果给定的键不存在环境变量，则会使用该值。

### 确定当前环境

应用程序当前所处环境是通过 `.env` 文件中的 `APP_ENV` 变量确定的。你可以通过 `App` [facade](https://learnku.com/docs/laravel/6.x/facades) 中的 `environment` 方法来访问此值：

```php
$environment = App::environment();
```

你还可以传递参数给 `environment` 方法，以检查当前的环境配置是否与给定值匹配。 如果与给定的任意值匹配，该方法将返回 `true`：

```php
if (App::environment('local')) {
    // 当前环境是 local
}

if (App::environment(['local', 'staging'])) {
    // 当前的环境是 local 或 staging...
}
```

> Tip：应用程序当前所处环境检测可以被服务器级的 `APP_ENV` 环境变量覆盖。这在为相同的应用程序配置不同的环境时是非常有用的，这样你可以在你的服务器配置中为给定的主机设置与其匹配的给定的环境。

### 在调试页面隐藏环境变量

当一个异常未被捕获并且 `APP_DEBUG` 环境变量为 `true` 时，调试页面会显示所有的环境变量和内容。在某些情况下你可能想隐藏某些变量。你可以通过设置 `config/app.php` 配置文件中的 `debug_blacklist` 选项来完成这个操作。

环境变量、服务器或者请求数据中都有一些变量是可用的。因此，你可能需要将 `$_ENV` 和 `$_SERVER` 的变量加入到黑名单中：

```php
return [

    // ...

    'debug_blacklist' => [
        '_ENV' => [
            'APP_KEY',
            'DB_PASSWORD',
        ],

        '_SERVER' => [
            'APP_KEY',
            'DB_PASSWORD',
        ],

        '_POST' => [
            'password',
        ],
    ],
];
```

## 访问配置值

你可以轻松地在应用程序的任何位置使用全局 config 函数来访问配置值。配置值的访问可以使用「点」语法，这其中包含了要访问的文件和选项的名称。还可以指定默认值，如果配置选项不存在，则返回默认值：

```php
$value = config('app.timezone');
```

要在运行时设置配置值，传递一个数组给 `config` 函数：

```php
config(['app.timezone' => 'America/Chicago']);
```

## 配置缓存

为了给你的应用程序提升速度，你应该使用 Artisan 命令 `config:cache` 将所有的配置文件缓存到单个文件中。这会把你的应用程序中所有的配置选项合并成一个单一的文件，然后框架会快速加载这个文件。

通常来说，你应该把运行 `php artisan config:cache` 命令作为生产环境部署常规工作的一部分。这个命令不应在本地开发环境下运行，因为配置选项在应用程序开发过程中是经常需要被更改的。

> 注意：如果在部署过程中执行 `config:cache` 命令，那你应该确保只从配置文件内部调用 env 函数。一旦配置被缓存，`.env` 文件将不再被加载，所有对 env 函数的调用都将返回 `null`

## 维护模式

当应用程序处于维护模式时，所有对应用程序的请求都显示为一个自定义视图。这样可以在更新或执行维护时轻松地「关闭」你的应用程序。 维护模式检查包含在应用程序的默认中间件栈中。如果应用程序处于维护模式，则将抛出一个状态码为 503 的 `MaintenanceModeException` 异常。

要启用维护模式，只需执行下面的 Artisan 的 `down` 命令：

```php
php artisan down
```

你还可以向 `down` 命令提供 `message` 和 `retry` 选项。其中 `message` 选项的值可用于显示或记录自定义消息，而 `retry` 值可用于设置 `HTTP` 响应头中 `Retry-After` 的值：

```php
php artisan down --message="Upgrading Database" --retry=60
```

即使在维护模式下，也可以使用命令 `allow` 选项允许特定的 IP 地址或网络访问应用程序：

```php
php artisan down --allow=127.0.0.1 --allow=192.168.0.0/16
```

要关闭维护模式，请使用 `up` 命令：

```php
php artisan up
```

> Tip：你可以通过修改 `resources/views/errors/503.blade.php` 模板文件来自定义默认维护模式模板。

#### 维护模式 & 队列

当应用程序处于维护模式时，不会处理 [队列任务](https://learnku.com/docs/laravel/6.x/queues)。而这些任务会在应用程序退出维护模式后再继续处理。

#### 维护模式的替代方案

维护模式会导致应用程序有数秒的停机（不响应）时间，因此你可以考虑使用像 [Envoyer](https://envoyer.io/) 这样的替代方案，以便与 Laravel 完成零停机时间部署。

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/con...](https://learnku.com/docs/laravel/6.x/configuration/5125)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/con...](https://learnku.com/docs/laravel/6.x/configuration/5125)