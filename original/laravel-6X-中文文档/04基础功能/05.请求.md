本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## HTTP 请求

+   [接收请求](#accessing-the-request)
    +   [请求路径 & 方法](#request-path-and-method)
    +   [PSR-7 请求Requests](#psr7-requests)
+   [输入预处理 & 规范化](#input-trimming-and-normalization)
+   [接收数据](#retrieving-input)
    +   [旧数据](#old-input)
    +   [Cookies](#cookies)
+   [文件](#files)
    +   [接收上传的文件](#retrieving-uploaded-files)
    +   [存储上传的文件](#storing-uploaded-files)
+   [配置信任代理](#configuring-trusted-proxies)

## 接收请求

要通过依赖注入获取当前 HTTP 请求实例，你应该在控制器上引入 `Illuminate\Http\Request` 类， 传入的请求实例将会由 [服务容器](https://learnku.com/docs/laravel/6.x/container) 自动注入：

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * 存储一个新用户。
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');

        //
    }
}
```

#### 依赖注入 & 路由参数

如果你的控制器需要从路由参数中获取数据，你应该在其他依赖项之后列入参数。举个例子，你的路由是这样定义的：

```php
Route::put('user/{id}', 'UserController@update');
```

你可以通过下面的方法来定义控制器，使用 `Illuminate\Http\Request` 类来获取你的路由参数 `id`：

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
```

#### 通过闭包路由获取参数

你也可以在路由闭包中使用 `Illuminate\Http\Request` 类， 服务容器会自动的将请求参数注入到路由闭包中：

```php
use Illuminate\Http\Request;

Route::get('/', function (Request $request) {
    //
});
```

### 请求路径 & 方法

`Illuminate\Http\Request` 提供了各种方法来检查应用程序的 http 请求，并扩展了 `Symfony\Component\HttpFoundation\Request` 类。 我们将在下面讨论一些最重要的方法。

#### 检索请求路径

`path` 方法返回请求的路径信息。因此，如果接收到的请求目标是 `http://domain.com/foo/bar`，则 `path` 方法会返回 `foo/bar`：

```php
$uri = $request->path();
```

`is` 方法验证请求的路径是否与给定的模式匹配。使用此方法时，可以将 `*` 字符作为通配符：

```php
if ($request->is('admin/*')) {
    //
}
```

#### 检索请求 URL

要获取请求的完整 url ，可以使用 `url` 或 `fullUrl` 方法。 `url` 方法返回不带查询条件的 url 字符串，而 `fullUrl` 方法包含查询字符串：

```php
// 没有查询字符串...
$url = $request->url();

// 带查询字符串...
$url = $request->fullUrl();
```

#### 检索请求方法

`method` 方法将会返回请求的 HTTP 动词。你也可以使用 `isMethod` 方法验证 HTTP 动词是否与给定的字符串匹配：

```php
$method = $request->method();

if ($request->isMethod('post')) {
    //
}
```

### PSR-7 请求

[PSR-7 标准](https://www.php-fig.org/psr/psr-7/) 指定了 HTTP 的消息接口，包含请求和响应。如果你想要获取 PSR-7请求实例而不是 Laravel 请求， 那么你首先需要安装几个库。 Laravel 使用 *Symfony HTTP Message Bridge* 组件将典型的 Laravel 请求和响应转换为 PSR-7 的兼容实现：

```php
composer require symfony/psr-http-message-bridge
composer require zendframework/zend-diactoros
```

安装这些库后，可以通过路由闭包和控制器方法的请求接口类型提示来获取 PSR-7 请求：

```php
use Psr\Http\Message\ServerRequestInterface;

Route::get('/', function (ServerRequestInterface $request) {
    //
});
```

> {提示} 如果从路由或控制器返回 PSR-7 响应实例，框架将会自动将其转换为 Laravel 的响应实例并显示。

## 输入过滤 & 规范化

默认情况下， Laravel 应用程序的全局中间件堆栈中包含了 `TrimStrings` 和 `ConvertEmptyStringsToNull` 中间件。它们被放在 `App\Http\Kernel` 类的堆栈列表中。 这些中间件将自动过滤掉请求中的字符串字段，并将空字符串字段转换为 `null`。这样一来，你将不用担心路由和控制器的约束规范问题。

如果你想禁用这些行为， 你可以在应用程序中 `App\Http\Kernel` 类的 `$middleware` 属性中移除这两个中间件。

## Retrieving Input

#### 获取所有输入数据

你可以用 `all` 方法获取请求数据数组：

```php
$input = $request->all();
```

#### 获取一个值

使用一些简单方法，你可以从 `Illuminate\Http\Request` 获取所有的用户输入数据，不用担心用户使用的是哪种 HTTP 动作。不管是什么 HTTP 动词，都可以使用 `input` 方法获取用户输入的数据：

```php
$name = $request->input('name');
```

你可以给 `input` 方法传第二个参数。如果请求中没有呈递该参数则会返回此值：

```php
$name = $request->input('name', 'Sally');
```

当有呈递数组输入的数据时，可以使用「点」 运算符访问数据：

```php
$name = $request->input('products.0.name');

$names = $request->input('products.*.name');
```

你可以使用不带参数的 `input` 方法，它将返回一个包含所有用户输入数据的数组：

```php
$input = $request->input();
```

#### 从查询字符串获取输入

`input` 方法从整个请求体获取值（包含字符串）， `query` 仅从查询字符串中获取输入值：

```php
$name = $request->query('name');
```

如果请求中不存在查询的字符串，则第二个参数将作为默认值返回：

```php
$name = $request->query('name', 'Helen');
```

你可以使用不带任何参数的 `query` 方法来获取所有查询条件的关联数组：

```php
$query = $request->query();
```

#### 通过动态属性获取输入

可以通过 `Illuminate\Http\Request` 实例的动态属性访问用户输入。例如，如果应用表单包含 `name` 域，可以像下面这样访问该域的值：

```php
$name = $request->name;
```

在使用动态属性时，Laravel 首先会在请求载体中查找参数的值。如果该值不存在，Lavarel 将在路由参数中搜索。

#### 获取 JSON 输入

当向应用传递 JSON 请求时，可以通过 `input` 方法访问 JSON 数据，只要将请求的 `Content-Type` 头设置为 `application/json`。 同样可以使用 「点」语法访问 JSON 数组：

```php
$name = $request->input('user.name');
```

#### 获取部分输入数据

如果需要获取输入数据的子集，可以使用 `only` 或 `except` 方法。它们接受单个 array 或者动态参数列表：

```php
$input = $request->only(['username', 'password']);

$input = $request->only('username', 'password');

$input = $request->except(['credit_card']);

$input = $request->except('credit_card');
```

> Tip：`only` 方法返回请求中的全部键值对；但是它不返回请求中不存在的键值对。

#### 判断输入值是否存在

`has` 方法用于判定请求中是否存在指定的值。如果请求中存在该值， `has` 方法返回 `true` ：

```php
if ($request->has('name')) {
    //
}
```

如果给出一个数组， `has` 方法将判断在请求中，指定的值是否全部存在：

```php
if ($request->has(['name', 'email'])) {
    //
}
```

如果想要判断一个值在请求中是否存在，并且不为空，需要使用 `filled` 方法：

```php
if ($request->filled('name')) {
    //
}
```

### 旧数据

Laravel 允许你在两次请求之间保持数据。这个特性在有效性校验出错后重新填充表单时非常有用。不过，如果你使用 Laravel 自带 [验证特性](https://learnku.com/docs/laravel/6.x/validation), 不需要自己手动调用这些方法因为一些 Laravel 内置的验证功能会自动调用它们。

#### 将输入数据传送到 Session

`Illuminate\Http\Request` 类的 flash 方法将把当前的输入传送到 [session](https://learnku.com/docs/laravel/6.x/session) ,在用户向应用发出这一次请求时它们仍然可用：

```php
$request->flash();
```

你也可以使用 `flashOnly` 或 `flashExcept` 方法将请求数据的子集传送给 session。这些方法常用于将密码之类的敏感数据排除在 session 保持之外：

```php
$request->flashOnly(['username', 'email']);

$request->flashExcept('password');
```

#### 传送数据并跳转

当你经常需要将输入传送至 session 并紧接着跳转至之前的页面，可以通过在跳转函数后链接调用 `withInput` 方法轻易地实现：

```php
return redirect('form')->withInput();

return redirect('form')->withInput(
    $request->except('password')
);
```

#### 获取旧数据

若要获取上一次请求中闪存的输入，则可以使用 `Request` 实例的 `old` 方法。 `old` 方法会从 [session](https://learnku.com/docs/laravel/6.x/session) 取出之前被闪存的输入数据：

```php
$username = $request->old('username');
```

Laravel 也提供了全局辅助函数 `old` 。如果你要在 [Blade 模板](https://learnku.com/docs/laravel/6.x/blade) 中显示旧的输入，使用 `old` 会更加方便。如果给定字段没有旧的输入，则返回 `null` ：

```php
<input type="text" name="username" value="{{ old('username') }}">
```

### Cookies

#### 从请求中获取 Cookies

Laravel 框架生成的全部 cookies 都是加密的，并且已经用授权码签名，这意味着如果它们被客户端改变就会失效。使用 `Illuminate\Http\Request` 实例的 cookie 方法可以从请求中获取 `cookie` 值：

```php
$value = $request->cookie('name');
```

或者也可以使用 `Cookie` facade 访问 cookie 值：

```php
use Illuminate\Support\Facades\Cookie;

$value = Cookie::get('name');
```

#### 将 Cookies 附加到响应中

你可以使用 `cookie` 将 cookie 附加到传出的 `Illuminate\Http\Response` 实例。你需要传递名称、值、cookie 的过期时间（以分钟为单位）给该方法：

```php
return response('Hello World')->cookie(
    'name', 'value', $minutes
);
```

cookie 方法还接受一些不太频繁使用的参数。通常这些参数和 PHP 内置的 [setcookie](https://secure.php.net/manual/en/function.setcookie.php) 方法的参数有着相同的作用和意义：

```php
return response('Hello World')->cookie(
    'name', 'value', $minutes, $path, $domain, $secure, $httpOnly
);
```

或者，你可以使用 `Cookie` facade 来「排列」 用于从应用中附加到输出响应的 cookies。`queue` 方法接受一个 `Cookie` 实例或者用于创建 `Cookie` 所需的参数列表。在发送到浏览器之前，这些 cookie 将被附加到输出响应:

```php
Cookie::queue(Cookie::make('name', 'value', $minutes));

Cookie::queue('name', 'value', $minutes);
```

#### 生成 Cookie 实例

如果你想要在一段时间以后生成一个可以给定`Symfony\Component\HttpFoundation\Cookie`的响应实例，可以使用全局辅助函数 `cookie` 。除非此 cookie 被附加到响应实例，否则不会发送回客户端：

```php
$cookie = cookie('name', 'value', $minutes);

return response('Hello World')->cookie($cookie);
```

## 文件

### 获取上传的文件

你可以使用 `file` 方法或使用动态属性从 `Illuminate\Http\Request` 实例中访问上传的文件。 该`file` 方法返回 `Illuminate\Http\UploadedFile` 类的实例，该类继承了 PHP 的 `SplFileInfo` 类的同时也提供了各种与文件交互的方法：

```php
$file = $request->file('photo');

$file = $request->photo;
```

当然你也可以使用 `hasFile` 方法判断请求中是否存在指定文件：

```php
if ($request->hasFile('photo')) {
    //
}
```

#### 验证成功上传

除了检查上传的文件是否存在外，你也可以通过 `isValid` 方法验证上传的文件是否有效：

```php
if ($request->file('photo')->isValid()) {
    //
}
```

#### 文件路径 & 扩展名

`UploadedFile` 类还包含访问文件的全路径和扩展名的方法。 `extension` 方法会根据文件内容判断文件的扩展名。该扩展名可能会和客户端提供的扩展名不同：

```php
$path = $request->photo->path();

$extension = $request->photo->extension();
```

#### 其它的文件方法

`UploadedFile` 实例上还有许多可用的方法。可以查看该类的 [API 文档](https://api.symfony.com/3.0/Symfony/Component/HttpFoundation/File/UploadedFile.html) 了解这些方法的详细信息。

### 存储上传文件

要存储上传的文件，先配置好 [文件系统](https://learnku.com/docs/laravel/6.x/filesystem)。 你可以使用 `UploadedFile` 的 `store` 方法把上传文件移动到你的某个磁盘上，该文件可能是本地文件系统中的一个位置，甚至像 Amazon S3 这样的云存储位置。

`store` 方法接受相对于文件系统配置的存储文件根目录的路径。这个路径不能包含文件名，因为系统会自动生成唯一的 ID 作为文件名。

`store` 方法还接受可选的第二个参数，用于存储文件的磁盘名称。这个方法会返回相对于磁盘根目录的文件路径：

```php
$path = $request->photo->store('images');

$path = $request->photo->store('images', 's3');
```

如果你不想自动生成文件名，那么可以使用 `storeAs` 方法，它接受路径、文件名和磁盘名作为其参数：

```php
$path = $request->photo->storeAs('images', 'filename.jpg');

$path = $request->photo->storeAs('images', 'filename.jpg', 's3');
```

## 配置可信代理

如果你的应用程序运行在失效的 TLS / SSL 证书的负载均衡器后，你可能会注意到你的应用程序有时不能生成 HTTPS 链接。通常这是因为你的应用程序正在从端口 80 上的负载均衡器转发流量，却不知道是否应该生成安全链接。

解决这个问题需要在 Laravel 应用程序中包含 `App\Http\Middleware\TrustProxies` 中间件，这使得你可以快速自定义应用程序信任的负载均衡器或代理。你的可信代理应该作为这个中间件的 `$proxies` 属性的数组列出。除了配置受信任的代理之外，还可以配置应该信任的代理 `$header`:

```php
<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Fideloper\Proxy\TrustProxies as Middleware;

class TrustProxies extends Middleware
{
    /**
     * 应用程序的可信代理列表
     *
     * @var array
     */
    protected $proxies = [
        '192.168.1.1',
        '192.168.1.2',
    ];

    /**
     * 应该用来检测代理的头信息
     *
     * @var string
     */
    protected $headers = Request::HEADER_X_FORWARDED_ALL;
}
```

> Tip：如果你使用 AWS 弹性负载平衡，你的 `$header` 值应该是 `Request::HEADER_X_FORWARDED_AWS_ELB`。如果您想查看更多可用于 `$headers` 的属性信息，请查阅 Symfony 的 文档 [信任代理](https://symfony.com/doc/current/deployment/proxies.html).

#### 信任所有代理

如果你使用 Amazon AWS 或其他的「云」的负载均衡服务，你可能不知道负载均衡器的实际 IP 地址。在这种情况下，你可以使用 `*`来信任所有代理：

```php
/**
 * 应用程序的可信代理列表
 *
 * @var array
 */
protected $proxies = '*';
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/req...](https://learnku.com/docs/laravel/6.x/requests/5139)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/req...](https://learnku.com/docs/laravel/6.x/requests/5139)