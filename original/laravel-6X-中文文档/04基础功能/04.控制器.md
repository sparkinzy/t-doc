本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 控制器

+   [简介](#introduction)
+   [基础控制器](#basic-controllers)
    +   [定义控制器](#defining-controllers)
    +   [控制器 & 命名空间](#controllers-and-namespaces)
    +   [单个行为控制器](#single-action-controllers)
+   [控制器中间件](#controller-middleware)
+   [资源控制器](#resource-controllers)
    +   [部分资源路由](#restful-partial-resource-routes)
    +   [命名资源路由](#restful-naming-resource-routes)
    +   [命名资源路由参数](#restful-naming-resource-route-parameters)
    +   [本地资源化 URIs](#restful-localizing-resource-uris)
    +   [补充资源控制器](#restful-supplementing-resource-controllers)
+   [依赖注入 & 控制器](#dependency-injection-and-controllers)
+   [路由缓存](#route-caching)

## 简介

为了替代在路由文件中以闭包形式定义的所有的请求处理逻辑， 你可能想要使用控制类来组织这些行为。控制器能将相关的请求处理逻辑组成一个单独的类。控制器被存放在 `app/Http/Controllers` 目录。

## 基础控制器

### 定义控制器

下面是一个基础控制器类的例子。需要注意的是，该控制器继承了Laravel的基类控制器。该基类控制器提供了一些便利的方法，比如 `middleware` 方法，该方法可以为控制器行为添加中间件：

```php
<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * 显示给用户的概要文件.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
}
```

你可以这样定义一个指向控制器行为的路由：

```php
Route::get('user/{id}', 'UserController@show');
```

现在，当一个请求与指定路由的 URI 匹配时，`UserController` 控制器中的 `show` 方法将被执行。路由参数也将会被传递给该方法。

> Tip：控制器并不是 **必需** 继承基础类。然而，如果控制器没有继承基础类，你将无法使用一些便捷的功能，比如`middleware`，`validate` 和 `dispatch` 方法。

### 控制器 & 命名空间

需要着重指出的是，在定义控制器路由时我们不需要指定完整的控制器命名空间。因为 `RouteServiceProvider` 会在一个包含命名空间的路由组中加载路由文件，我们只需要指令类名中 `App\Http\Controllers` 命名空间之后的部分就可以了。

如果你选择将控制器放在 `App\Http\Controllers` 更深层次的目录中，需要使用相对于 `App\Http\Controllers` 作为根命名空间的指定类名。因此，如果你完整的控制器类名为 `App\Http\Controllers\Photos\AdminController` ，你在路由中应该采用如下的形式注册：

```php
Route::get('foo', 'Photos\AdminController@method');
```

### 单个行为控制器

如果你想要定义一个只处理单个行为的控制器，你可以在控制器中放置一个 `__invoke` 方法：

```php
<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class ShowProfile extends Controller
{
    /**
     * 显示给定用户的资料.
     *
     * @param  int  $id
     * @return View
     */
    public function __invoke($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
}
```

当注册单个行为控制器的路由时，你不需要指明方法：

```php
Route::get('user/{id}', 'ShowProfile');
```

你可以通过Artisan命令工具里的 `make:controller` 命令中的 `--invokable` 选项来生成一个可调用的控制器：

```php
php artisan make:controller ShowProfile --invokable
```

## 控制器中间件

[Middleware](https://learnku.com/docs/laravel/6.x/middleware) 可以在路由文件中分配给控制器的路由：

```php
Route::get('profile', 'UserController@show')->middleware('auth');
```

然而，在控制器的构造函数中指定中间件更为方便。使用控制器构造函数中的 `middleware` 方法，可以轻松地将中间件分配给控制器的操作。你甚至可以将中间件限制为控制器类上的某些方法。

```php
class UserController extends Controller
{
    /**
     * 实例化一个新的控制器实例.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('log')->only('index');

        $this->middleware('subscribed')->except('store');
    }
}
```

同时，控制器还允许你使用一个闭包来注册中间件。这为不定义整个中间件类的情况下为单个控制器定义中间件提供了一种方便的方法：

```php
$this->middleware(function ($request, $next) {
    // ...

    return $next($request);
});
```

> Tip：你可以将中间件分配给控制器操作的一个子集；然而，它可能表明你的控制器正在变的复杂。建议你将控制器拆分为多个较小的控制器。

## 资源控制器

Laravel的资源路由将典型的「CURD (增删改查)」路由分配给具有单行代码的控制器。例如，你希望创建一个控制器来处理保存"照片"应用的所有HTTP请求。使用Artisan命令 `make:controller` ，我们可以快速创建这样一个控制器：

```php
php artisan make:controller PhotoController --resource
```

这个命令将会生成一个控制器 `app/Http/Controllers/PhotoController.php` 。其中包括每个可用资源操作的方法。

接下来，你可以给控制器注册一个资源路由：

```php
Route::resource('photos', 'PhotoController');
```

这个单一的路由声明创建了多个路由来处理资源上的各种行为。生成的控制器为每个行为保留了方法，包括了关于处理 HTTP 动词和 URLs 的声明注释。

你可以通过将数组传参到 `resources` 方法中的方式来一次性的创建多个资源控制器：

```php
Route::resources([
    'photos' => 'PhotoController',
    'posts' => 'PostController'
]);
```

#### 资源控制器操作处理

| HTTP方法 | URI | 动作 | 路由名称 |
| --- | --- | --- | --- |
| GET | `/photos` | index | photos.index |
| GET | `/photos/create` | create | photos.create |
| POST | `/photos` | store | photos.store |
| GET | `/photos/{photo}` | show | photos.show |
| GET | `/photos/{photo}/edit` | edit | photos.edit |
| PUT/PATCH | `/photos/{photo}` | update | photos.update |
| DELETE | `/photos/{photo}` | destroy | photos.destroy |

#### 指定资源模型

如果你使用了路由模型绑定，并且想在资源控制器的方法中使用类型提示，你可以在生成控制器的时候使用 `--model` 选项：

```php
php artisan make:controller PhotoController --resource --model=Photo
```

#### 伪造表单方法

因为 HTML 的表单不能生成 `PUT`，`PATCH`，和 `DELETE` 请求，所以你需要添加一个隐藏的 `_method` 字段来伪造 HTTP 动作。这个 Blade 指令 `@method` 可以为你创造这个字段：

```php
<form action="/foo/bar" method="POST">
    @method('PUT')
</form>
```

### 部分资源路由

当声明资源路由时，你可以指定控制器处理的部分行为，而不是所有默认的行为：

```php
Route::resource('photos', 'PhotoController')->only([
    'index', 'show'
]);

Route::resource('photos', 'PhotoController')->except([
    'create', 'store', 'update', 'destroy'
]);
```

#### API 资源路由

当声明用于 APIs 的资源路由时，通常需要排除显示 HTML 模板的路由（如 `create` 和 `edit` ）。为了方便起见，你可以使用 apiResource 方法自动排除这两个路由：

```php
Route::apiResource('photos', 'PhotoController');
```

你可以传递一个数组给 `apiResources` 方法来同时注册多个 API 资源控制器：

```php
Route::apiResources([
    'photos' => 'PhotoController',
    'posts' => 'PostController'
]);
```

要快速生成不包含 `create` 或 `edit` 方法的用于开发接口的资源控制器，请在执行 `make:controller` 命令时使用 `--api` 开关:

```php
php artisan make:controller API/PhotoController --api
```

### 命名资源路由

默认情况下，所有的资源控制器行为都有一个路由名称。你可以传入 names 数组来覆盖这些名称：

```php
Route::resource('photos', 'PhotoController')->names([
    'create' => 'photos.build'
]);
```

### 命名资源路由参数

默认情况下，`Route::resource` 会根据资源名称的「单数」形式创建资源路由的路由参数。你可以在选项数组中传入 `parameters` 参数来轻松地覆盖每个资源。`parameters` 数组应该是资源名称和参数名称的关联数组：

```php
Route::resource('users', 'AdminUserController')->parameters([
    'users' => 'admin_user'
]);
```

上例将会为资源的 `show` 路由生成如下的 URI ：

```php
/users/{admin_user}
```

### 本地化资源 URI

默认情况下，`Route::resource` 将会用英文动词创建资源 URI。如果需要本地化 `create` 和 `edit` 行为动作名，可以在 `AppServiceProvider` 的 `boot` 中使用 `Route::resourceVerbs` 方法实现：

```php
use Illuminate\Support\Facades\Route;

/**
 * 引导任何应用服务。
 *
 * @return void
 */
public function boot()
{
    Route::resourceVerbs([
        'create' => 'crear',
        'edit' => 'editar',
    ]);
}
```

动作被自定义后，像 `Route::resource('fotos', 'PhotoController')` 这样注册的资源路由将会产生如下的 URI：

```php
/fotos/crear

/fotos/{foto}/editar
```

### 补充资源控制器

如果你想在默认的资源路由中增加额外的路由，你应该在 `Route::resource` 之前定义这些路由。否则由 resource 方法定义的路由可能会无意中优先于你补充的路由：

```php
Route::get('photos/popular', 'PhotoController@method');

Route::resource('photos', 'PhotoController');
```

> Tip：记住保持控制器的专一性。如果你需要典型的资源操作之外的方法，可以考虑将你的控制器分成两个更小的控制器。

## 依赖注入 & 控制器

#### 构造函数注入

Laravel 使用 [服务容器](https://learnku.com/docs/laravel/6.x/container) 来解析所有的控制器。因此，你可以在控制器的构造函数中使用类型提示需要的依赖项，而声明的依赖项会自动解析并注入控制器实例中：

```php
<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;

class UserController extends Controller
{
    /**
     * 用户 repository 实例.
     */
    protected $users;

    /**
     * 创建一个新的控制器实例。
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
}
```

当然，你也可以类型提示 [Laravel 契约](https://learnku.com/docs/laravel/6.x/contracts)，只要它能被解析。根据你的应用，将你的依赖项注入控制器能提供更好的可测试性。

#### 方法注入

除了构造函数注入之外，你还可以在控制器方法中类型提示依赖项。最常见的用法就是将 `Illuminate\Http\Request` 实例注入到控制器方法中：

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * 保存一个新用户。
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $name = $request->name;

        //
    }
}
```

如果控制器方法需要从路由参数中获取输入内容，只需要在其他依赖项后列出路由参数即可。比如，如果你的路由是这样定义的：

```php
Route::put('user/{id}', 'UserController@update');
```

你仍然可以类型提示 `Illuminate\Http\Request` 并通过定义控制器方法获取 id 参数，如下所示：

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * 更新给定用户的信息。
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
```

## 路由缓存

> 注意：基于闭包的路由不能被缓存。如果要使用路由缓存，你必须将所有的闭包路由转换成控制器类路由。

如果你的应用只使用了基于控制器的路由，那么你应该充分利用 Laravel 的路由缓存。使用路由缓存将极大地减少注册所有应用路由所需的时间。某些情况下，路由注册的速度甚至可以快一百倍。要生成路由缓存，只需执行 Artisan 命令 `route:cache`：

```php
php artisan route:cache
```

运行这个命令之后，每一次请求的时候都将会加载缓存的路由文件。如果你添加了新的路由，你需要生成 一个新的路由缓存。因此，你应该只在生产环境运行 `route:cache` 命令：

你可以使用 `route:clear` 命令清除路由缓存：

```php
php artisan route:clear
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/con...](https://learnku.com/docs/laravel/6.x/controllers/5138)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/con...](https://learnku.com/docs/laravel/6.x/controllers/5138)