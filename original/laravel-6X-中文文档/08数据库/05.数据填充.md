## 数据填充

Laravel 6 中文文档 /  

本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 数据库：填充

+   [简介](#introduction)
+   [编写 Seeders](#writing-seeders)
    +   [使用模型工厂](#using-model-factories)
    +   [调用其他 Seeders](#calling-additional-seeders)
+   [运行 Seeders](#running-seeders)

## 简介

Laravel 包含一个填充类可以为你的数据库填充测试数据，所有的填充类都放在 `database/seeds` 目录下。你可以随意为填充类命名，但是更建议您遵守类似 `UsersTableSeeder` 的命名规范。通常， Laravel 默认定义了一个 `DatabaseSeeder` 类。通过这个类，你可以用 `call` 方法来运行其它的 seed 类从而控制数据填充的顺序。

## 编写 Seeders

运行 [Artisan 命令](https://learnku.com/docs/laravel/6.x/artisan) `make:seeder` 生成 Seeder，框架生成的 seeders 都放在 `database/seeds` 目录下：

```php
php artisan make:seeder UsersTableSeeder
```

seeder 类只包含一个默认方法：`run` 。这个方法会在执行 `db:seed` 这个 [Artisan 命令](https://learnku.com/docs/laravel/6.x/artisan) 时被调用。在 `run` 方法里你可以根据需要在数据库中插入数据。你也可以用 [查询构造器](https://learnku.com/docs/laravel/6.x/queries) 或 [Eloquent 模型工厂](https://learnku.com/docs/laravel/6.x/database-testing#writing-factories) 来手动插入数据。

> Tip：使用数据填充时会自动禁用 [批量赋值保护](https://learnku.com/docs/laravel/6.x/eloquent#mass-assignment) 。

如下所示，在默认的 `DatabaseSeeder` 类中的 `run` 方法中添加一条数据插入语句：

```php
<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
```

> Tip：你在 `run` 的方法签名中可以用类型来约束你需要的依赖。它们会被 Laravel [服务容器](https://learnku.com/docs/laravel/6.x/container) 自动解析。

### 使用模型工厂

手动为每个模型填充指定属性很麻烦，你可以使用 [model 工厂](https://learnku.com/docs/laravel/6.x/database-testing#writing-factories) 轻松地生成大量数据库数据。首先，阅读 [model 工厂文档](https://learnku.com/docs/laravel/6.x/database-testing#writing-factories) 来学习如何定义工厂文件，一旦定义好了你的工厂文件，然后就可以使用 `factory` 这个辅助函数来向数据库中插入数据。

例如，创建 50 个用户并为每个用户创建关联：

```php
/**
 * Run the database seeds.
 *
 * @return void
 */
public function run()
{
    factory(App\User::class, 50)->create()->each(function ($user) {
        $user->posts()->save(factory(App\Post::class)->make());
    });
}
```

### 调用其它 Seeders

在 `DatabaseSeeder` 类中，你可以使用 `call` 方法来运行其它的 seed 类。使用 `call` 方法可以将数据填充拆分成多个文件，这样就不会使单个 seeder 变得非常大。只需简单传递要运行的 seeder 类名称即可：

```php
/**
 * Run the database seeds.
 *
 * @return void
 */
public function run()
{
    $this->call([
        UsersTableSeeder::class,
        PostsTableSeeder::class,
        CommentsTableSeeder::class,
    ]);
}
```

## 运行 Seeders

完成 seeder 类的编写之后，你可能需要使用 `dump-autoload` 命令重新生成 Composer 的自动加载器：

```php
composer dump-autoload
```

现在你可以使用 Artisan 命令 `db:seed` 来填充数据库了。默认情况下， `db:seed` 命令将运行 `DatabaseSeeder` 类，这个类可以用来调用其它 Seed 类。不过，你也可以使用 `--class` 选项来指定一个特定的 seeder 类：

```php
php artisan db:seed

php artisan db:seed --class=UsersTableSeeder
```

你也可以用 `migrate:refresh` 这个命令来填充数据库，该命令会回滚并重新运行所有迁移。这个命令可以用来重建数据库：

```php
php artisan migrate:refresh --seed
```

#### 在生产环境中强制使用

一些填充操作可能会导致原有数据的更新或丢失。为了保护生产环境数据库的数据，在运行填充命令前会进行确认。可以添加 `--force`选项来强制运行填充命令:

```php
php artisan db:seed --force
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/see...](https://learnku.com/docs/laravel/6.x/seeding/5174)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/see...](https://learnku.com/docs/laravel/6.x/seeding/5174)