{
    "/Yii2/": [
        {
            "text": "前言",
            "children": [
                {
                    "text": "关于 Yii",
                    "link": "01前言/01.关于 Yii.md"
                },
                {
                    "text": "从 Yii 1.1 升级",
                    "link": "01前言/02.从 Yii 1.1 升级.md"
                },
                {
                    "text": "Yii 2.0 升级说明",
                    "link": "01前言/03.Yii 2.0 升级说明.md"
                },
                {
                    "text": "贡献者指引",
                    "link": "01前言/04.贡献者指引.md"
                },
                {
                    "text": "捐献和赞助",
                    "link": "01前言/05.捐献和赞助.md"
                }
            ]
        },
        {
            "text": "版本管理",
            "children": [
                {
                    "text": "发行说明",
                    "link": "02版本管理/01.发行说明.md"
                },
                {
                    "text": "版本说明",
                    "link": "02版本管理/02.版本说明.md"
                },
                {
                    "text": "更新记录",
                    "link": "02版本管理/03.更新记录.md"
                }
            ]
        },
        {
            "text": "快速入门",
            "children": [
                {
                    "text": "安装 Yii",
                    "link": "03快速入门/01.安装 Yii.md"
                },
                {
                    "text": "目录结构",
                    "link": "03快速入门/02.目录结构.md"
                },
                {
                    "text": "部署",
                    "link": "03快速入门/03.部署.md"
                },
                {
                    "text": "Hello world",
                    "link": "03快速入门/04.Hello world.md"
                },
                {
                    "text": "使用表单",
                    "link": "03快速入门/05.使用表单.md"
                },
                {
                    "text": "使用数据库",
                    "link": "03快速入门/06.使用数据库.md"
                },
                {
                    "text": "使用 Gii 生成代码",
                    "link": "03快速入门/07.使用 Gii 生成代码.md"
                },
                {
                    "text": "编辑器与 IDE",
                    "link": "03快速入门/08.编辑器与 IDE.md"
                }
            ]
        },
        {
            "text": "核心架构",
            "children": [
                {
                    "text": "请求生命周期",
                    "link": "04核心架构/01.请求生命周期.md"
                },
                {
                    "text": "单一入口",
                    "link": "04核心架构/02.单一入口.md"
                },
                {
                    "text": "应用主体",
                    "link": "04核心架构/03.应用主体.md"
                },
                {
                    "text": "组件",
                    "link": "04核心架构/04.组件.md"
                },
                {
                    "text": "属性",
                    "link": "04核心架构/05.属性.md"
                },
                {
                    "text": "事件",
                    "link": "04核心架构/06.事件.md"
                },
                {
                    "text": "行为",
                    "link": "04核心架构/07.行为.md"
                },
                {
                    "text": "配置",
                    "link": "04核心架构/08.配置.md"
                },
                {
                    "text": "别名",
                    "link": "04核心架构/09.别名.md"
                },
                {
                    "text": "类自动加载",
                    "link": "04核心架构/10.类自动加载.md"
                },
                {
                    "text": "服务定位器",
                    "link": "04核心架构/11.服务定位器.md"
                },
                {
                    "text": "依赖注入容器",
                    "link": "04核心架构/12.依赖注入容器.md"
                }
            ]
        },
        {
            "text": "高级应用模板",
            "children": [
                {
                    "text": "安装",
                    "link": "05高级应用模板/01.安装.md"
                },
                {
                    "text": "框架结构",
                    "link": "05高级应用模板/02.框架结构.md"
                },
                {
                    "text": "运行测试",
                    "link": "05高级应用模板/03.运行测试.md"
                },
                {
                    "text": "环境与配置",
                    "link": "05高级应用模板/04.环境与配置.md"
                },
                {
                    "text": "自定义应用模板",
                    "link": "05高级应用模板/05.自定义应用模板.md"
                }
            ]
        },
        {
            "text": "基础功能",
            "children": [
                {
                    "text": "路由",
                    "link": "06基础功能/01.路由.md"
                },
                {
                    "text": "控制器",
                    "link": "06基础功能/02.控制器.md"
                },
                {
                    "text": "过滤器",
                    "link": "06基础功能/03.过滤器.md"
                },
                {
                    "text": "请求",
                    "link": "06基础功能/04.请求.md"
                },
                {
                    "text": "响应",
                    "link": "06基础功能/05.响应.md"
                },
                {
                    "text": "模型",
                    "link": "06基础功能/06.模型.md"
                },
                {
                    "text": "视图",
                    "link": "06基础功能/07.视图.md"
                },
                {
                    "text": "模块",
                    "link": "06基础功能/08.模块.md"
                },
                {
                    "text": "小部件",
                    "link": "06基础功能/09.小部件.md"
                },
                {
                    "text": "前端资源",
                    "link": "06基础功能/10.前端资源.md"
                },
                {
                    "text": "扩展",
                    "link": "06基础功能/11.扩展.md"
                },
                {
                    "text": "Sessions 和 Cookies",
                    "link": "06基础功能/12.Sessions 和 Cookies.md"
                },
                {
                    "text": "错误处理",
                    "link": "06基础功能/13.错误处理.md"
                },
                {
                    "text": "日志",
                    "link": "06基础功能/14.日志.md"
                }
            ]
        },
        {
            "text": "数据库",
            "children": [
                {
                    "text": "数据库访问对象（DAO）",
                    "link": "07数据库/01.数据库访问对象（DAO）.md"
                },
                {
                    "text": "查询构造器",
                    "link": "07数据库/02.查询构造器.md"
                },
                {
                    "text": "活动记录",
                    "link": "07数据库/03.活动记录.md"
                },
                {
                    "text": "数据库迁移",
                    "link": "07数据库/04.数据库迁移.md"
                },
                {
                    "text": "Redis",
                    "link": "07数据库/05.Redis.md"
                },
                {
                    "text": "Sphinx",
                    "link": "07数据库/06.Sphinx.md"
                },
                {
                    "text": "Elasticsearch",
                    "link": "07数据库/07.Elasticsearch.md"
                },
                {
                    "text": "MongoDB",
                    "link": "07数据库/08.MongoDB.md"
                }
            ]
        },
        {
            "text": "安全",
            "children": [
                {
                    "text": "认证",
                    "link": "08安全/01.认证.md"
                },
                {
                    "text": "授权",
                    "link": "08安全/02.授权.md"
                },
                {
                    "text": "加密",
                    "link": "08安全/03.加密.md"
                },
                {
                    "text": "Email 认证",
                    "link": "08安全/04.Email 认证.md"
                },
                {
                    "text": "验证码",
                    "link": "08安全/05.验证码.md"
                },
                {
                    "text": "社会化登录",
                    "link": "08安全/06.社会化登录.md"
                },
                {
                    "text": "密码",
                    "link": "08安全/07.密码.md"
                }
            ]
        },
        {
            "text": "进阶功能",
            "children": [
                {
                    "text": "缓存",
                    "link": "09进阶功能/01.缓存.md"
                },
                {
                    "text": "发送邮件",
                    "link": "09进阶功能/02.发送邮件.md"
                },
                {
                    "text": "HTTP 客户端",
                    "link": "09进阶功能/03.HTTP 客户端.md"
                },
                {
                    "text": "国际化",
                    "link": "09进阶功能/04.国际化.md"
                },
                {
                    "text": "队列",
                    "link": "09进阶功能/05.队列.md"
                },
                {
                    "text": "控制台应用",
                    "link": "09进阶功能/06.控制台应用.md"
                },
                {
                    "text": "助手类",
                    "link": "09进阶功能/07.助手类.md"
                },
                {
                    "text": "验证器",
                    "link": "09进阶功能/08.验证器.md"
                }
            ]
        },
        {
            "text": "RESTful Web 服务",
            "children": [
                {
                    "text": "快速入门",
                    "link": "10RESTful Web 服务/01.快速入门.md"
                },
                {
                    "text": "资源",
                    "link": "10RESTful Web 服务/02.资源.md"
                },
                {
                    "text": "控制器",
                    "link": "10RESTful Web 服务/03.控制器.md"
                },
                {
                    "text": "路由",
                    "link": "10RESTful Web 服务/04.路由.md"
                },
                {
                    "text": "格式化响应",
                    "link": "10RESTful Web 服务/05.格式化响应.md"
                },
                {
                    "text": "认证",
                    "link": "10RESTful Web 服务/06.认证.md"
                },
                {
                    "text": "速率限制（限流）",
                    "link": "10RESTful Web 服务/07.速率限制（限流）.md"
                },
                {
                    "text": "版本化",
                    "link": "10RESTful Web 服务/08.版本化.md"
                },
                {
                    "text": "错误处理",
                    "link": "10RESTful Web 服务/09.错误处理.md"
                }
            ]
        },
        {
            "text": "开发工具",
            "children": [
                {
                    "text": "调试工具栏和调试器",
                    "link": "11开发工具/01.调试工具栏和调试器.md"
                },
                {
                    "text": "使用 Gii 生成代码",
                    "link": "11开发工具/02.使用 Gii 生成代码.md"
                }
            ]
        }
    ]
}