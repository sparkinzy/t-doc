export const laravel5_5zh = {
    "/laravel-5.5-中文文档/": [
        {
            "text": "前言",
            "children": [
                {
                    "text": "翻译说明",
                    "link": "01前言/01.翻译说明.md"
                },
                {
                    "text": "发行说明",
                    "link": "01前言/02.发行说明.md"
                },
                {
                    "text": "升级说明",
                    "link": "01前言/03.升级说明.md"
                },
                {
                    "text": "贡献导引",
                    "link": "01前言/04.贡献导引.md"
                }
            ]
        },
        {
            "text": "入门指南",
            "children": [
                {
                    "text": "安装",
                    "link": "02入门指南/01.安装.md"
                },
                {
                    "text": "配置信息",
                    "link": "02入门指南/02.配置信息.md"
                },
                {
                    "text": "文件夹结构",
                    "link": "02入门指南/03.文件夹结构.md"
                },
                {
                    "text": "Homestead",
                    "link": "02入门指南/04.Homestead.md"
                },
                {
                    "text": "Valet",
                    "link": "02入门指南/05.Valet.md"
                },
                {
                    "text": "部署",
                    "link": "02入门指南/06.部署.md"
                }
            ]
        },
        {
            "text": "核心架构",
            "children": [
                {
                    "text": "请求周期",
                    "link": "03核心架构/01.请求周期.md"
                },
                {
                    "text": "服务容器",
                    "link": "03核心架构/02.服务容器.md"
                },
                {
                    "text": "服务提供者",
                    "link": "03核心架构/03.服务提供者.md"
                },
                {
                    "text": "Facades",
                    "link": "03核心架构/04.Facades.md"
                },
                {
                    "text": "Contracts",
                    "link": "03核心架构/05.Contracts.md"
                }
            ]
        },
        {
            "text": "基础功能",
            "children": [
                {
                    "text": "路由",
                    "link": "04基础功能/01.路由.md"
                },
                {
                    "text": "中间件",
                    "link": "04基础功能/02.中间件.md"
                },
                {
                    "text": "CSRF 保护",
                    "link": "04基础功能/03.CSRF 保护.md"
                },
                {
                    "text": "控制器",
                    "link": "04基础功能/04.控制器.md"
                },
                {
                    "text": "请求",
                    "link": "04基础功能/05.请求.md"
                },
                {
                    "text": "响应",
                    "link": "04基础功能/06.响应.md"
                },
                {
                    "text": "视图",
                    "link": "04基础功能/07.视图.md"
                },
                {
                    "text": "URL",
                    "link": "04基础功能/08.URL.md"
                },
                {
                    "text": "Session",
                    "link": "04基础功能/09.Session.md"
                },
                {
                    "text": "表单验证",
                    "link": "04基础功能/10.表单验证.md"
                },
                {
                    "text": "错误与日志",
                    "link": "04基础功能/11.错误与日志.md"
                }
            ]
        },
        {
            "text": "前端开发",
            "children": [
                {
                    "text": "Blade 模板",
                    "link": "05前端开发/01.Blade 模板.md"
                },
                {
                    "text": "本地化",
                    "link": "05前端开发/02.本地化.md"
                },
                {
                    "text": "前端指南",
                    "link": "05前端开发/03.前端指南.md"
                },
                {
                    "text": "编辑资源 Mix",
                    "link": "05前端开发/04.编辑资源 Mix.md"
                }
            ]
        },
        {
            "text": "安全相关",
            "children": [
                {
                    "text": "用户认证",
                    "link": "06安全相关/01.用户认证.md"
                },
                {
                    "text": "Passport OAuth 认证",
                    "link": "06安全相关/02.Passport OAuth 认证.md"
                },
                {
                    "text": "用户授权",
                    "link": "06安全相关/03.用户授权.md"
                },
                {
                    "text": "加密解密",
                    "link": "06安全相关/04.加密解密.md"
                },
                {
                    "text": "哈希",
                    "link": "06安全相关/05.哈希.md"
                },
                {
                    "text": "重置密码",
                    "link": "06安全相关/06.重置密码.md"
                }
            ]
        },
        {
            "text": "综合话题",
            "children": [
                {
                    "text": "Artisan 命令行",
                    "link": "07综合话题/01.Artisan 命令行.md"
                },
                {
                    "text": "广播系统",
                    "link": "07综合话题/02.广播系统.md"
                },
                {
                    "text": "缓存系统",
                    "link": "07综合话题/03.缓存系统.md"
                },
                {
                    "text": "集合",
                    "link": "07综合话题/04.集合.md"
                },
                {
                    "text": "事件系统",
                    "link": "07综合话题/05.事件系统.md"
                },
                {
                    "text": "文件存储",
                    "link": "07综合话题/06.文件存储.md"
                },
                {
                    "text": "辅助函数",
                    "link": "07综合话题/07.辅助函数.md"
                },
                {
                    "text": "邮件发送",
                    "link": "07综合话题/08.邮件发送.md"
                },
                {
                    "text": "消息通知",
                    "link": "07综合话题/09.消息通知.md"
                },
                {
                    "text": "扩展包开发",
                    "link": "07综合话题/10.扩展包开发.md"
                },
                {
                    "text": "队列",
                    "link": "07综合话题/11.队列.md"
                },
                {
                    "text": "任务调度",
                    "link": "07综合话题/12.任务调度.md"
                }
            ]
        },
        {
            "text": "数据库",
            "children": [
                {
                    "text": "快速入门",
                    "link": "08数据库/01.快速入门.md"
                },
                {
                    "text": "查询构造器",
                    "link": "08数据库/02.查询构造器.md"
                },
                {
                    "text": "分页",
                    "link": "08数据库/03.分页.md"
                },
                {
                    "text": "数据库迁移",
                    "link": "08数据库/04.数据库迁移.md"
                },
                {
                    "text": "数据填充",
                    "link": "08数据库/05.数据填充.md"
                },
                {
                    "text": "Redis",
                    "link": "08数据库/06.Redis.md"
                }
            ]
        },
        {
            "text": "Eloquent ORM",
            "children": [
                {
                    "text": "快速入门",
                    "link": "09Eloquent ORM/01.快速入门.md"
                },
                {
                    "text": "模型关联",
                    "link": "09Eloquent ORM/02.模型关联.md"
                },
                {
                    "text": "Eloquent 集合",
                    "link": "09Eloquent ORM/03.Eloquent 集合.md"
                },
                {
                    "text": "修改器",
                    "link": "09Eloquent ORM/04.修改器.md"
                },
                {
                    "text": "API 资源",
                    "link": "09Eloquent ORM/05.API 资源.md"
                },
                {
                    "text": "序列化",
                    "link": "09Eloquent ORM/06.序列化.md"
                }
            ]
        },
        {
            "text": "测试相关",
            "children": [
                {
                    "text": "快速入门",
                    "link": "10测试相关/01.快速入门.md"
                },
                {
                    "text": "HTTP 测试",
                    "link": "10测试相关/02.HTTP 测试.md"
                },
                {
                    "text": "浏览器测试 Dusk",
                    "link": "10测试相关/03.浏览器测试 Dusk.md"
                },
                {
                    "text": "数据库测试",
                    "link": "10测试相关/04.数据库测试.md"
                },
                {
                    "text": "测试模拟器",
                    "link": "10测试相关/05.测试模拟器.md"
                }
            ]
        },
        {
            "text": "官方扩展包",
            "children": [
                {
                    "text": "Cashier 交易工具包",
                    "link": "11官方扩展包/01.Cashier 交易工具包.md"
                },
                {
                    "text": "Envoy 部署工具",
                    "link": "11官方扩展包/02.Envoy 部署工具.md"
                },
                {
                    "text": "Horizon",
                    "link": "11官方扩展包/03.Horizon.md"
                },
                {
                    "text": "Scout 全文搜索",
                    "link": "11官方扩展包/04.Scout 全文搜索.md"
                },
                {
                    "text": "Socialite 社会化登录",
                    "link": "11官方扩展包/05.Socialite 社会化登录.md"
                }
            ]
        }
    ]
}