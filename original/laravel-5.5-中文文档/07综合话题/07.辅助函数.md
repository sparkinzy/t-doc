本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## Laravel 的辅助函数列表

+   [简介](#introduction)
+   [可用方法](#available-methods)

## 简介

Laravel 包含各种各样的全局「辅助」PHP 函数，框架本身也大量地使用了这些功能；如果你觉得方便，你可以在你的应用中自由的使用它们。

## 可用方法

### 数组 & 对象

[array\_add](#method-array-add)  
[array\_collapse](#method-array-collapse)  
[array\_divide](#method-array-divide)  
[array\_dot](#method-array-dot)  
[array\_except](#method-array-except)  
[array\_first](#method-array-first)  
[array\_flatten](#method-array-flatten)  
[array\_forget](#method-array-forget)  
[array\_get](#method-array-get)  
[array\_has](#method-array-has)  
[array\_last](#method-array-last)  
[array\_only](#method-array-only)  
[array\_pluck](#method-array-pluck)  
[array\_prepend](#method-array-prepend)  
[array\_pull](#method-array-pull)  
[array\_random](#method-array-random)  
[array\_set](#method-array-set)  
[array\_sort](#method-array-sort)  
[array\_sort\_recursive](#method-array-sort-recursive)  
[array\_where](#method-array-where)  
[array\_wrap](#method-array-wrap)  
[data\_fill](#method-data-fill)  
[data\_get](#method-data-get)  
[data\_set](#method-data-set)  
[head](#method-head)  
[last](#method-last)

### 路径

[app\_path](#method-app-path)  
[base\_path](#method-base-path)  
[config\_path](#method-config-path)  
[database\_path](#method-database-path)  
[mix](#method-mix)  
[public\_path](#method-public-path)  
[resource\_path](#method-resource-path)  
[storage\_path](#method-storage-path)

### 字符串

[\_\_](#method-__)  
[camel\_case](#method-camel-case)  
[class\_basename](#method-class-basename)  
[e](#method-e)  
[ends\_with](#method-ends-with)  
[kebab\_case](#method-kebab-case)  
[preg\_replace\_array](#method-preg-replace-array)  
[snake\_case](#method-snake-case)  
[starts\_with](#method-starts-with)  
[str\_after](#method-str-after)  
[str\_before](#method-str-before)  
[str\_contains](#method-str-contains)  
[str\_finish](#method-str-finish)  
[str\_is](#method-str-is)  
[str\_limit](#method-str-limit)  
[str\_plural](#method-str-plural)  
[str\_random](#method-str-random)  
[str\_replace\_array](#method-str-replace-array)  
[str\_replace\_first](#method-str-replace-first)  
[str\_replace\_last](#method-str-replace-last)  
[str\_singular](#method-str-singular)  
[str\_slug](#method-str-slug)  
[str\_start](#method-str-start)  
[studly\_case](#method-studly-case)  
[title\_case](#method-title-case)  
[trans](#method-trans)  
[trans\_choice](#method-trans-choice)

### URLs

[action](#method-action)  
[asset](#method-asset)  
[secure\_asset](#method-secure-asset)  
[route](#method-route)  
[secure\_url](#method-secure-url)  
[url](#method-url)

### 其他

[abort](#method-abort)  
[abort\_if](#method-abort-if)  
[abort\_unless](#method-abort-unless)  
[app](#method-app)  
[auth](#method-auth)  
[back](#method-back)  
[bcrypt](#method-bcrypt)  
[blank](#method-blank)  
[broadcast](#method-broadcast)  
[cache](#method-cache)  
[class\_uses\_recursive](#method-class-uses-recursive)  
[collect](#method-collect)  
[config](#method-config)  
[cookie](#method-cookie)  
[csrf\_field](#method-csrf-field)  
[csrf\_token](#method-csrf-token)  
[dd](#method-dd)  
[decrypt](#method-decrypt)  
[dispatch](#method-dispatch)  
[dispatch\_now](#method-dispatch-now)  
[dump](#method-dump)  
[encrypt](#method-encrypt)  
[env](#method-env)  
[event](#method-event)  
[factory](#method-factory)  
[filled](#method-filled)  
[info](#method-info)  
[logger](#method-logger)  
[method\_field](#method-method-field)  
[now](#method-now)  
[old](#method-old)  
[optional](#method-optional)  
[policy](#method-policy)  
[redirect](#method-redirect)  
[report](#method-report)  
[request](#method-request)  
[rescue](#method-rescue)  
[resolve](#method-resolve)  
[response](#method-response)  
[retry](#method-retry)  
[session](#method-session)  
[tap](#method-tap)  
[today](#method-today)  
[throw\_if](#method-throw-if)  
[throw\_unless](#method-throw-unless)  
[trait\_uses\_recursive](#method-trait-uses-recursive)  
[transform](#method-transform)  
[validator](#method-validator)  
[value](#method-value)  
[view](#method-view)  
[with](#method-with)

## 方法列表

## 数组

#### `array_add()`

如果给定的键不在数组中，那么 `array_add` 函数将会把给定的键/值对添加到数组中：

```php
$array = array_add(['name' => 'Desk'], 'price', 100);

// ['name' => 'Desk', 'price' => 100]
```

#### `array_collapse()`

`array_collapse` 函数将多个单数组合并成一个数组：

```php
$array = array_collapse([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);

// [1, 2, 3, 4, 5, 6, 7, 8, 9]
```

#### `array_divide()`

`array_divide` 函数返回两个数组，一个包含原始数组的健，另一个包含原始数组的值：

```php
list($keys, $values) = array_divide(['name' => 'Desk']);

// $keys: ['name']

// $values: ['Desk']
```

#### `array_dot()`

`array_dot` 函数将多维数组平铺到一维数组中，该数组使用「点」符号表示深度：

```php
$array = array_dot(['foo' => ['bar' => 'baz']]);

// ['foo.bar' => 'baz'];
```

#### `array_except()`

`array_except` 函数从数组中删除给定的键/值对：

```php
$array = ['name' => 'Desk', 'price' => 100];

$array = array_except($array, ['price']);

// ['name' => 'Desk']
```

#### `array_first()`

`array_first` 函数返回数组中第一个通过指定测试的元素：

```php
$array = [100, 200, 300];

$value = array_first($array, function ($value, $key) {
    return $value >= 150;
});

// 200
```

将默认值作为第三个参数传递给该方法。如果没有值通过测试，则返回该值：

```php
$value = array_first($array, $callback, $default);
```

#### `array_flatten()`

`array_flatten` 函数将多维数组平铺为一维数组。

```php
$array = ['name' => 'Joe', 'languages' => ['PHP', 'Ruby']];

$array = array_flatten($array);

// ['Joe', 'PHP', 'Ruby'];
```

#### `array_forget()`

`array_forget` 函数使用「点」符号从深度嵌套数组中移除给定的键/值对：

```php
$array = ['products' => ['desk' => ['price' => 100]]];

array_forget($array, 'products.desk');

// ['products' => []]
```

#### `array_get()`

`array_get` 函数使用「点」符号从深度嵌套的数组中检索值：

```php
$array = ['products' => ['desk' => ['price' => 100]]];

$value = array_get($array, 'products.desk');

// ['price' => 100]
```

`array_get` 函数也接受一个默认值，如果没有找到指定的健，则返回该值：

```php
$value = array_get($array, 'names.john', 'default');
```

#### `array_has()`

`array_has` 函数使用「点」符号检查数组中是否存在给定的项目或项目组：

```php
$array = ['product' => ['name' => 'desk', 'price' => 100]];

$hasItem = array_has($array, 'product.name');

// true

$hasItems = array_has($array, ['product.price', 'product.discount']);

// false
```

#### `array_last()`

`array_last` 函数返回数组中最后一个通过指定测试的元素：

```php
$array = [100, 200, 300, 110];

$value = array_last($array, function ($value, $key) {
    return $value >= 150;
});

// 300
```

将默认值作为第三个参数传递给该方法。如果没有值通过测试，则返回该值：

```php
$last = array_last($array, $callback, $default);
```

#### `array_only()`

`array_only` 函数仅返回给定数组中指定的键/值对：

```php
$array = ['name' => 'Desk', 'price' => 100, 'orders' => 10];

$array = array_only($array, ['name', 'price']);

// ['name' => 'Desk', 'price' => 100]
```

#### `array_pluck()`

`array_pluck` 函数从数组中检索给定键的所有值：

```php
$array = [
    ['developer' => ['id' => 1, 'name' => 'Taylor']],
    ['developer' => ['id' => 2, 'name' => 'Abigail']],
];

$array = array_pluck($array, 'developer.name');

// ['Taylor', 'Abigail'];
```

你也可以指定生成的列表的键：

```php
$array = array_pluck($array, 'developer.name', 'developer.id');

// [1 => 'Taylor', 2 => 'Abigail'];
```

#### `array_prepend()`

`array_prepend` 函数将一个项目推到数组的开头：

```php
$array = ['one', 'two', 'three', 'four'];

$array = array_prepend($array, 'zero');

// $array: ['zero', 'one', 'two', 'three', 'four']
```

你可以指定用于该值的键：

```php
$array = ['price' => 100];

$array = array_prepend($array, 'Desk', 'name');

// ['name' => 'Desk', 'price' => 100]
```

#### `array_pull()`

`array_pull` 函数返回并从数组中删除键/值对：

```php
$array = ['name' => 'Desk', 'price' => 100];

$name = array_pull($array, 'name');

// $name: Desk

// $array: ['price' => 100]
```

将默认值作为第三个参数传递给该方法。如果键不存在，则返回该值：

```php
$value = array_pull($array, $key, $default);
```

#### `array_random()`

`array_random` 函数从数组中返回一个随机值：

```php
$array = [1, 2, 3, 4, 5];

$random = array_random($array);

// 4 - (随机获取)
```

你也可以指定要返回的随机数的数量作为第二个可选参数。一旦你指定了第二个参数，即使数量为 1，这个函数也会返回一个数组：

```php
$items = array_random($array, 2);

// [2, 5] - (随机获取)
```

#### `array_set()`

`array_set` 函数使用「点」符号在深度嵌套的数组中设置一个值：

```php
$array = ['products' => ['desk' => ['price' => 100]]];

array_set($array, 'products.desk.price', 200);

// ['products' => ['desk' => ['price' => 200]]]
```

#### `array_sort()`

`array_sort` 函数按照其值排序数组：

```php
$array = ['Desk', 'Table', 'Chair'];

$sorted = array_sort($array);

// ['Chair', 'Desk', 'Table']
```

你也可以按给定的闭包返回的结果对数组进行排序：

```php
$array = [
    ['name' => 'Desk'],
    ['name' => 'Table'],
    ['name' => 'Chair'],
];

$sorted = array_values(array_sort($array, function ($value) {
    return $value['name'];
}));

/*
    [
        ['name' => 'Chair'],
        ['name' => 'Desk'],
        ['name' => 'Table'],
    ]
*/
```

#### `array_sort_recursive()`

`array_sort_recursive` 函数使用 `sort` 函数递归排序数组：

```php
$array = [
    ['Roman', 'Taylor', 'Li'],
    ['PHP', 'Ruby', 'JavaScript'],
];

$sorted = array_sort_recursive($array);

/*
    [
        ['Li', 'Roman', 'Taylor'],
        ['JavaScript', 'PHP', 'Ruby'],
    ]
*/
```

#### `array_where()`

`array_where` 函数使用给定的闭包来过滤数组：

```php
$array = [100, '200', 300, '400', 500];

$array = array_where($array, function ($value, $key) {
    return is_string($value);
});

// [1 => 200, 3 => 400]
```

#### `array_wrap()`

`array_wrap` 函数将给定的值包装成一个数组。如果给定的值已经是一个数组，则不会被改变：

```php
$string = 'Laravel';

$array = array_wrap($string);

// [0 => 'Laravel']
```

#### `data_fill()`

`data_fill` 函数使用「点」符号在嵌套数组或对象内设置缺少的值：

```php
$data = ['products' => ['desk' => ['price' => 100]]];

data_fill($data, 'products.desk.price', 200);

// ['products' => ['desk' => ['price' => 100]]]

data_fill($data, 'products.desk.discount', 10);

// ['products' => ['desk' => ['price' => 100, 'discount' => 10]]]
```

该函数也接受星号「\*」作为通配符，并相应地填写目标：

```php
$data = [
    'products' => [
        ['name' => 'Desk 1', 'price' => 100],
        ['name' => 'Desk 2'],
    ],
];

data_fill($data, 'products.*.price', 200);

/*
    [
        'products' => [
            ['name' => 'Desk 1', 'price' => 100],
            ['name' => 'Desk 2', 'price' => 200],
        ],
    ]
*/
```

#### `data_get()`

`data_get` 函数使用「点」符号从嵌套数组或对象中检索值：

```php
$data = ['products' => ['desk' => ['price' => 100]]];

$price = data_get($data, 'products.desk.price');

// 100
```

`data_get` 函数还接受默认值作为第三个参数，如果找不到指定的键，将返回该值：

```php
$discount = data_get($data, 'products.desk.discount', 0);

// 0
```

#### `data_set()`

`data_set` 函数使用「点」符号在嵌套数组或对象内设置一个值：

```php
$data = ['products' => ['desk' => ['price' => 100]]];

data_set($data, 'products.desk.price', 200);

// ['products' => ['desk' => ['price' => 200]]]
```

这个函数也接受通配符「\*」，并相应地在目标上设置值：

```php
    $data = [
        'products' => [
            ['name' => 'Desk 1', 'price' => 100],
            ['name' => 'Desk 2', 'price' => 150],
        ],
    ];

    data_set($data, 'products.*.price', 200);

    /*
        [
            'products' => [
                ['name' => 'Desk 1', 'price' => 200],
                ['name' => 'Desk 2', 'price' => 200],
            ],
        ]
    */
```

默认情况下，所有现有的值都会被覆盖。如果你只想设置一个不存在值，你可以传递 `false` 作为第三个参数：

```php
$data = ['products' => ['desk' => ['price' => 100]]];

data_set($data, 'products.desk.price', 200, false);

// ['products' => ['desk' => ['price' => 100]]]
```

#### `head()`

`head` 函数返回给定数组中的第一个元素：

```php
$array = [100, 200, 300];

$first = head($array);

// 100
```

#### `last()`

`last` 函数返回给定数组中的最后一个元素：

```php
$array = [100, 200, 300];

$last = last($array);

// 300
```

## 路径

#### `app_path()`

`app_path` 返回 `app` 目录的完整路径。你还可以使用 `app_path` 函数来生成相对于 `app` 目录的文件完整路径：

```php
$path = app_path();

$path = app_path('Http/Controllers/Controller.php');
```

#### `base_path()`

`base_path` 函数返回项目根目录的完整路径。你还可以使用 `base_path` 函数生成指定文件相对于项目根目录的完整路径：

```php
$path = base_path();

$path = base_path('vendor/bin');
```

#### `config_path()`

`config_path` 函数返回应用程序配置目录的完整路径。你也可以使用 `config_path` 函数来生成应用程序配置目录中给定文件的完整路径：

```php
$path = config_path();
```

#### `database_path()`

`database_path` 函数返回应用程序数据库目录的完整路径。你也可以使用 `database_path` 函数来生成数据库目录中给定文件的完整路径：

```php
$path = database_path();
```

#### `mix()`

`mix` 函数获取 [版本化 Mix 文件](https://learnku.com/docs/laravel/5.5/mix) 的路径：

```php
mix($file);
```

#### `public_path()`

`public_path` 函数返回 `public` 目录的完整路径。你也可以使用 `public_path` 函数来生成 `public` 目录中给定文件的完整路径：

```php
$path = public_path();
```

#### `resource_path()`

`resource_path` 函数返回 `resources` 目录的完整路径。你也可以使用 `resource_path` 函数来生成相对于资源目录的指定文件的完整路径：

```php
$path = resource_path();

$path = resource_path('assets/sass/app.scss');
```

#### `storage_path()`

`storage_path` 函数返回 `storage` 目录的完整路径。你也可以使用 `storage_path` 来生成相对于储存目录的指定文件的完整路径：

```php
$path = storage_path();

$path = storage_path('app/file.txt');
```

## 字符串

#### `__()`

`__` 函数使用你的 [本地化文件](https://learnku.com/docs/laravel/5.5/localization) 来翻译给定的翻译字符串或翻译键：

```php
echo __('Welcome to our application');

echo __('messages.welcome');
```

如果指定的翻译字符串或键不存在，则 `__` 函数会简单地返回给定的值。所以，按照上面的例子，如果翻译键 `messages.welcome` 不存在，`__` 方法会将其直接返回。

#### `camel_case()`

`camel_case` 函数将给定的值符传转换为「驼峰命名」：

```php
$camel = camel_case('foo_bar');

// fooBar
```

#### `class_basename()`

`class_basename` 返回给定类删除命名空间的类名：

```php
$class = class_basename('Foo\Bar\Baz');

// Baz
```

#### `e()`

`e` 函数将 `double_encode` 选项设置为 `false` 来运行 PHP 的 `htmlspecialchars` 函数：

```php
echo e('<html>foo</html>');

// <html>foo</html>
```

#### `ends_with()`

`ends_with` 函数判断给定的字符串是否以给定的值结尾：

```php
$value = ends_with('This is my name', 'name');

// true
```

#### `kebab_case()`

`kebab_case` 函数将给定的字符串转换为「短横线命名」：

```php
$value = kebab_case('fooBar');

// foo-bar
```

#### `preg_replace_array()`

`preg_replace_array` 函数使用数组顺序替换字符串中的给定模式：

```php
$string = '活动将在 :start 和 :end 之间进行';

$replaced = preg_replace_array('/:[a-z_]+/', ['8:30', '9:00'], $string);

// 活动将在 8:30 至 9:00 之间进行
```

#### `snake_case()`

`snake_case` 函数将给定的字符串转换为「蛇形命名」：

```php
$snake = snake_case('fooBar');

// foo_bar
```

#### `starts_with()`

`starts_with` 函数判断给定的字符串的开头是否是指定值：

```php
$value = starts_with('This is my name', 'This');

// true
```

#### `str_after()`

`str_after` 函数返回字符串中指定值之后的所有内容：

```php
$slice = str_after('This is my name', 'This is');

// ' my name'
```

#### `str_before()`

`str_before` 函数返回字符串中给定值之前的所有内容：

```php
$slice = str_before('This is my name', 'my name');

// 'This is '
```

#### `str_contains()`

`str_contains` 函数判断给定的字符串是否包含给定的值：

```php
$value = str_contains('This is my name', 'my');

// true
```

你也可以传递一个值的数组来判断给定的字符串是否包含任何值：

```php
$value = str_contains('This is my name', ['my', 'foo']);

// true
```

#### `str_finish()`

`str_finish` 函数将给定字符串以给定值结尾返回（如果它尚未以给定值结尾）：

```php
$string = str_finish('this/string', '/');

// this/string/

$string2 = str_finish('this/string/', '/');

// this/string/
```

#### `str_is()`

`str_is` 函数判断给定的字符串是否匹配给定的模式。星号可以用来表示通配符：

```php
$value = str_is('foo*', 'foobar');

// true

$value = str_is('baz*', 'foobar');

// false
```

#### `str_limit()`

`str_limit` 函数按给定的长度截断给定的字符串：

```php
$truncated = str_limit('The quick brown fox jumps over the lazy dog', 20);

// The quick brown fox...
```

你也可以传递第三个参数来改变将被追加到最后的字符串：

```php
$truncated = str_limit('The quick brown fox jumps over the lazy dog', 20, ' (...)');

// The quick brown fox (...)
```

#### `str_plural()`

`str_plural` 函数将字符串转换为复数形式。这个函数目前仅支持英文：

```php
$plural = str_plural('car');

// cars

$plural = str_plural('child');

// children
```

你可以提供一个整数作为函数的第二个参数来检索字符串的单数或复数形式：

```php
$plural = str_plural('child', 2);

// children

$plural = str_plural('child', 1);

// child
```

#### `str_random()`

`str_random` 函数生成一个指定长度的随机字符串。这个函数数用 PHP 的 `random_bytes` 函数：

```php
$string = str_random(40);
```

#### `str_replace_array()`

`str_replace_array` 函数使用数组顺序替换字符串中的给定值：

```php
$string = 'The event will take place between ? and ?';

$replaced = str_replace_array('?', ['8:30', '9:00'], $string);

// The event will take place between 8:30 and 9:00
```

#### `str_replace_first()`

`str_replace_first` 函数替换字符串中给定值的第一个匹配项：

```php
$replaced = str_replace_first('the', 'a', 'the quick brown fox jumps over the lazy dog');

// a quick brown fox jumps over the lazy dog
```

#### `str_replace_last()`

`str_replace_last` 函数替换字符串中最后一次出现的给定值：

```php
$replaced = str_replace_last('the', 'a', 'the quick brown fox jumps over the lazy dog');

// the quick brown fox jumps over a lazy dog
```

#### `str_singular()`

`str_singular` 函数将字符串转换为单数形式。这个函数目前仅支持英文：

```php
$singular = str_singular('cars');

// car
```

#### `str_slug()`

`str_slug` 函数根据给定的字符串生成一个 URL 友好的「slug」：

```php
$title = str_slug('Laravel 5 Framework', '-');

// laravel-5-framework
```

#### `str_start()`

`str_start` 函数将给定值的单个实例添加到字符串（如果它尚未以值开始）：

```php
$adjusted = str_start('this/string', '/');

// /this/string

$adjusted = str_start('/this/string/', '/');

// /this/string
```

#### `studly_case()`

`studly_case` 函数将给定的字符串转换为「变种驼峰命名」：

```php
$value = studly_case('foo_bar');

// FooBar
```

#### `title_case()`

`title_case` 函数将给定的字符串转换为「首字母大写」：

```php
$title = title_case('a nice title uses the correct case');

// A Nice Title Uses The Correct Case
```

#### `trans()`

`trans` 函数使用你的 [本地化文件](https://learnku.com/docs/laravel/5.5/localization) 来翻译给定的翻译字符串或翻译键：

```php
echo trans('messages.welcome');
```

如果指定的翻译键不存在，则 `trans` 方法会简单地返回给定的键。所以，就上面的例子而言，如果翻译键不存在， `trans` 方法会返回 `messages.welcome`。

#### `trans_choice()`

`trans_choice` 函数根据词形变化来翻译给定的翻译键：

```php
echo trans_choice('messages.notifications', $unreadCount);
```

如果指定的翻译键不存在，`trans_choice` 方法会简单地返回给定的键。所以，按照上面的例子，如果翻译键不存在，`trans_choice` 方法会返回 `messages.notifications`。

## URLs

#### `action()`

`action` 函数为指定的控制器动作生成一个 URL。你不需要传递完整的控制器命名空间。只需要传递相对于 `App\Http\Controllers` 的命名空间的控制器类名称：

```php
$url = action('HomeController@getIndex');
```

如果该方法接受路由参数，则可以将它们作为方法的第二个参数传递：

```php
$url = action('UserController@profile', ['id' => 1]);
```

#### `asset()`

`asset` 函数使用当前请求的协议（ HTTP 或 HTTPS ）为资源文件生成 URL：

```php
$url = asset('img/photo.jpg');
```

#### `secure_asset()`

`secure_asset` 函数使用 HTTPS 协议为资源文件生成 URL:

```php
echo secure_asset('foo/bar.zip');
```

#### `route()`

`route` 函数为给定的命名路由生成一个 URL：

```php
$url = route('routeName');
```

如果路由接受参数，则可以将它们作为方法的第二个参数传递：

```php
$url = route('routeName', ['id' => 1]);
```

默认情况下，`route` 函数生成的是绝对 URL。如果你想生成一个相对 URL，你可以传递 `false` 作为第三个参数：

```php
$url = route('routeName', ['id' => 1], false);
```

#### `secure_url()`

`secure_url` 函数为给定的路径生成一个标准的 HTTPS URL：

```php
echo secure_url('user/profile');

echo secure_url('user/profile', [1]);
```

#### `url()`

`url` 函数生成给定路径的标准 URL：

```php
echo url('user/profile');

echo url('user/profile', [1]);
```

如果没有提供路径，则返回 `Illuminate\Routing\UrlGenerator` 实例：

```php
echo url()->current();
echo url()->full();
echo url()->previous();
```

## 其他

#### `abort()`

`abort` 函数抛出 [异常处理](https://learnku.com/docs/laravel/5.5/errors#the-exception-handler) 程序呈现的 [HTTP 异常](https://learnku.com/docs/laravel/5.5/errors#http-exceptions)：

```php
abort(401);
```

你也可以提供额外的响应文本和自定义响应标头：

```php
abort(403, 'Unauthorized.', $headers);
```

#### `abort_if()`

如果给定的布尔表达式计算结果为 `true`， `abort_if` 函数将抛出一个 HTTP 异常：

```php
abort_if(! Auth::user()->isAdmin(), 403);
```

和 `abort` 方法一样，你也可以提供异常的响应文本作为第三个参数，并提供一个自定义响应头数组作为第四个参数。

#### `abort_unless()`

如果给定的布尔表达式计算结果为 `false`，`abort_unless` 函数将抛出一个 HTTP 异常：

```php
abort_unless(Auth::user()->isAdmin(), 403);
```

和 `abort` 方法一样，你也可以提供异常的响应文本作为第三个参数，并提供一个自定义响应头数组作为第四个参数。

#### `app()`

`app` 函数返回 [服务容器](https://learnku.com/docs/laravel/5.5/container) 实例

```php
$container = app();
```

你可以传递一个类或接口名称来从容器中解析它：

```php
$api = app('HelpSpot\API');
```

#### `auth()`

`auth` 函数返回一个 [认证](https://learnku.com/docs/laravel/5.5/authentication) 实例。为了方便起见，你可以使用它来替代 `Auth` facade：

```php
$user = auth()->user();
```

如果需要，你可以指定你想要访问的认证实例：

```php
$user = auth('admin')->user();
```

#### `back()`

`back()` 函数生成一个 [重定向 HTTP 响应](https://learnku.com/docs/laravel/5.5/responses#redirects) 到用户之前的位置：

```php
return back($status = 302, $headers = [], $fallback = false);

return back();
```

#### `bcrypt()`

`bcrypt` 使用 Bcrypt 对给定的值进行散列。你可以使用它替代 `Hash` facade：

```php
$password = bcrypt('my-secret-password');
```

#### `broadcast()`

`broadcast` 函数将 [广播](https://learnku.com/docs/laravel/5.5/broadcasting) 给定的 [事件](https://learnku.com/docs/laravel/5.5/events) 到它的监听器：

```php
broadcast(new UserRegistered($user));
```

#### `blank()`

`blank` 函数判断给定的值是否为「空」：

```php
blank('');
blank('   ');
blank(null);
blank(collect());

// true

blank(0);
blank(true);
blank(false);

// false
```

要使用与 `blank` 相反的功能，请看 [filled](https://learnku.com/docs/laravel/5.5/helpers#method-filled) 方法。

#### `cache()`

`cache` 函数可以用来从缓存中获取值。如果缓存中不存在给定的健，则返回一个可选的默认值：

```php
$value = cache('key');

$value = cache('key', 'default');
```

你可以通过将一组键/值对传递给函数来将其添加到缓存中。与此同时，你还应该传递有效的分钟数或持续时间作为缓存过期时间：

```php
cache(['key' => 'value'], 5);

cache(['key' => 'value'], Carbon::now()->addSeconds(10));
```

#### `class_uses_recursive()`

`class_uses_recursive` 函数返回一个类使用的所有 traits，包括任何子类使用的 traits：

```php
$traits = class_uses_recursive(App\User::class);
```

#### `collect()`

`collect` 函数根据给定的数组创建一个 [集合](https://learnku.com/docs/laravel/5.5/collections) 实例：

```php
$collection = collect(['taylor', 'abigail']);
```

#### `config()`

`config` 函数获取 [配置](https://learnku.com/docs/laravel/5.5/configuration) 变量的值。可以使用「点」语法访问配置值，其中包括文件的名称和希望访问的选项。如果配置选项不存在，则可以指定一个默认值并返回：

```php
$value = config('app.timezone');

$value = config('app.timezone', $default);
```

可以在运行时通过传递一组键/值对来设置配置变量：

```php
config(['app.debug' => true]);
```

#### `cookie()`

`cookie` 函数创建一个新的 [cookie](https://learnku.com/docs/laravel/5.5/requests#cookies) 实例：

```php
$cookie = cookie('name', 'value', $minutes);
```

#### `csrf_field()`

`csrf_field` 函数生成包含 CSRF 令牌值的 HTML `hidden` 表单字段。例如，使用 [Blade 语法](https://learnku.com/docs/laravel/5.5/blade)：

```php
{{ csrf_field() }}
```

#### `csrf_token()`

`csrf_token` 函数获取当前 CSRF 令牌的值：

```php
$token = csrf_token();
```

#### `dd()`

`dd` 函数输出给定的值并结束脚本运行：

```php
dd($value);

dd($value1, $value2, $value3, ...);
```

如果你不想终止脚本运行，请改用 `dump` 函数。

#### `decrypt()`

`decrypt` 函数使用 Laravel 的 [加密器](https://learnku.com/docs/laravel/5.5/encryption) 来解密给定的值：

```php
$decrypted = decrypt($encrypted_value);
```

#### `dispatch()`

`dispatch` 函数将给定的 [任务](https://learnku.com/docs/laravel/5.5/queues#creating-jobs) 推送到 Laravel [任务列队](https://learnku.com/docs/laravel/5.5/queues) 中：

```php
dispatch(new App\Jobs\SendEmails);
```

#### `dispatch_now()`

`dispatch_now` 函数立即运行给定的 [任务](https://learnku.com/docs/laravel/5.5/queues#creating-jobs)，并从其 `handle` 方法返回值：

```php
$result = dispatch_now(new App\Jobs\SendEmails);
```

#### `dump()`

`dump` 函数打印给定的变量：

```php
dump($value);

dump($value1, $value2, $value3, ...);
```

如果要在打印变量后停止执行脚本，请改用 [`dd`](#method-dd) 函数。

#### `encrypt()`

`encrypt` 函数使用 Laravel 的 [加密器](https://learnku.com/docs/laravel/5.5/encryption) 对给定的值进行加密：

```php
$encrypted = encrypt($unencrypted_value);
```

#### `env()`

`env` 函数获取 [环境变量](https://learnku.com/docs/laravel/5.5/configuration#environment-configuration) 的值或者返回默认值：

```php
$env = env('APP_ENV');

// 如果环境变量不存在则返回默认值...
$env = env('APP_ENV', 'production');
```

#### `event()`

`event` 函数将给定的 [事件](https://learnku.com/docs/laravel/5.5/events) 分派给它的监听器：

```php
event(new UserRegistered($user));
```

#### `factory()`

`factory` 函数根据给定的类、名称和数量创建一个模型工厂构建器。可以在 [测试](https://learnku.com/docs/laravel/5.5/database-testing#writing-factories) or [数据填充](https://learnku.com/docs/laravel/5.5/seeding#using-model-factories) 中使用：

```php
$user = factory(App\User::class)->make();
```

#### `filled()`

`filled` 函数判断给定的值是否不为「空」：

```php
filled(0);
filled(true);
filled(false);

// true

filled('');
filled('   ');
filled(null);
filled(collect());

// false
```

要使用与 `filled` 相反的功能，请看 [blank](https://learnku.com/docs/laravel/5.5/helpers#method-blank) 方法。

#### `info()`

`info` 函数将信息写入日志：

```php
info('Some helpful information!');
```

有前后关系的数组也可以传递给函数：

```php
info('User login attempt failed.', ['id' => $user->id]);
```

#### `logger()`

`logger` 函数可以将一个 `debug` 级别的消息写入到 [日志](https://learnku.com/docs/laravel/5.5/errors#logging) 中：

```php
logger('Debug message');
```

有前后关系的数组也可以传递给函数：

```php
logger('User has logged in.', ['id' => $user->id]);
```

如果没有传值给函数则返回 [日志](https://learnku.com/docs/laravel/5.5/errors#logging) 的实例：

```php
logger()->error('You are not allowed here.');
```

#### `method_field()`

`method_field` 函数生成一个 HTML `hidden` 表单字段，其中包含表单的 HTTP 动作的欺骗值。例如，使用 [Blade 语法](https://learnku.com/docs/laravel/5.5/blade)：

```php
<form method="POST">
    {{ method_field('DELETE') }}
</form>
```

#### `now()`

`now` 函数为当前时间创建一个新的 `Illuminate\Support\Carbon` 实例:

```php
$now = now();
```

#### `old()`

`old` 函数 [获取](https://learnku.com/docs/laravel/5.5/requests#retrieving-input) 会话中闪存的 [旧输入](https://learnku.com/docs/laravel/5.5/requests#old-input) 值：

```php
$value = old('value');

$value = old('value', 'default');
```

#### `optional()`

`optional` 函数可以接受任何参数，并且允许你访问该对象的属性或者调用方法。如果给定的对象是 `null` ， 那么属性和方法会简单地返回 `null` 而不是产生一个错误：

```php
return optional($user->address)->street;

{!! old('name', optional($user)->name) !!}
```

#### `policy()`

`policy` 方法为给定的类获取一个 [策略](https://learnku.com/docs/laravel/5.5/authorization#creating-policies) 实例：

```php
$policy = policy(App\User::class);
```

#### `redirect()`

`redirect` 函数返回一个 [重定向 HTTP 响应](https://learnku.com/docs/laravel/5.5/responses#redirects)，如果没有没有传入参数，则返回重定向实例：

```php
return redirect('/home');

return redirect()->route('route.name');
```

#### `report()`

`report` 函数将使用 [异常处理程序](https://learnku.com/docs/laravel/5.5/errors#the-exception-handler) 的 `report` 方法抛出异常：

```php
report($e);
```

#### `request()`

`request` 函数返回当前 [请求](https://learnku.com/docs/laravel/5.5/requests) 实例或者获取输入项：

```php
$request = request();

$value = request('key', $default = null)
```

#### `rescue()`

`rescue` 函数执行给定的闭包并捕获执行期间发生的任何异常。所有被捕获的异常将被发送到你的 [异常处理程序](https://learnku.com/docs/laravel/5.5/errors#the-exception-handler) 的 `report` 方法。要注意的是，该请求将继续处理：

```php
return rescue(function () {
    return $this->method();
});
```

你也可以将第二个参数传递给 `rescue` 方法。如果在执行闭包时发生异常，这个参数将是应该返回的默认值：

```php
return rescue(function () {
    return $this->method();
}, false);

return rescue(function () {
    return $this->method();
}, function () {
    return $this->failure();
});
```

#### `resolve()`

`resolve` 函数使用 [服务容器](https://learnku.com/docs/laravel/5.5/container) 将给定的类或接口名称解析为其实例：

```php
$api = resolve('HelpSpot\API');
```

#### `response()`

`response` 函数创建 [响应](https://learnku.com/docs/laravel/5.5/responses) 实例或者获取响应工厂实例：

```php
return response('Hello World', 200, $headers);

return response()->json(['foo' => 'bar'], 200, $headers);
```

#### `retry()`

`retry` 函数尝试执行给定的回调，直到到达给定的最大尝试次数。如果回调没有抛出异常，则返回值将被返回。如果回调抛出异常，它将自动重试。如果超过最大尝试次数，则会抛出异常：

```php
return retry(5, function () {
    // 在 100ms 左右尝试 5 次...
}, 100);
```

#### `session()`

`session` 函数可以用来获取或者设置 [Session](https://learnku.com/docs/laravel/5.5/session) 值：

```php
$value = session('key');
```

你可以通过将一组键/值对传递给该函数来设置值：

```php
session(['chairs' => 7, 'instruments' => 3]);
```

如果没有传递值给函数，则返回 Session 实例：

```php
$value = session()->get('key');

session()->put('key', $value);
```

#### `tap()`

`tap` 函数接受两个参数：一个任意的 `$value` 和一个闭包。`$value` 将被传递给闭包，然后由 `tap` 函数返回。不需要在闭包中使用 `return` 返回值。

```php
$user = tap(User::first(), function ($user) {
    $user->name = 'taylor';

    $user->save();
});
```

如果没有闭包被传递给 `tap` 函数，你可以调用给定 `$value` 的任何方法。而你调用的方法的返回值始终为 `$value` ，无论方法在其定义中实际返回的是什么。例如，Eloquent 的 `update` 方法通常会返回一个整数。但是，我们可以强制通过 `tap` 函数链式调用 `update` 方法来返回模型本身：

```php
$user = tap($user)->update([
    'name' => $name,
    'email' => $email,
]);
```

#### `today()`

`today` 函数为当前日期创建一个新的 `Illuminate\Support\Carbon` 实例:

```php
$today = today();
```

#### `throw_if()`

如果给定的布尔表达式计算结果为 `true`，`throw_if` 函数抛出给定的异常：

```php
throw_if(! Auth::user()->isAdmin(), AuthorizationException::class);

throw_if(
    ! Auth::user()->isAdmin(),
    AuthorizationException::class,
    'You are not allowed to access this page'
);
```

#### `throw_unless()`

如果给定的布尔表达式计算结果为`false`，则 `throw_unless`函数会抛出给定的异常：

```php
throw_unless(Auth::user()->isAdmin(), AuthorizationException::class);

throw_unless(
    Auth::user()->isAdmin(),
    AuthorizationException::class,
    'You are not allowed to access this page'
);
```

#### `trait_uses_recursive()`

`trait_uses_recursive` 函数返回一个类使用的所有 trait：

```php
$traits = trait_uses_recursive(\Illuminate\Notifications\Notifiable::class);
```

#### `transform()`

如果给定的值不为空，那么 `transform` 函数对给定的值执行闭包并返回其结果：

```php
$callback = function ($value) {
    return $value * 2;
};

$result = transform(5, $callback);

// 10
```

默认值或闭包也可以作为方法的第三个参数传递。如果给定值为空白，则返回该值：

```php
$result = transform(null, $callback, 'The value is blank');

// The value is blank
```

#### `validator()`

`validator` 函数用给定的参数创建一个新的 [验证器](https://learnku.com/docs/laravel/5.5/validation) 实例。为方便起见，你可以使用它来代替 `Validator` facade ：

```php
$validator = validator($data, $rules, $messages);
```

#### `value()`

`value` 函数返回给定的值。但是，如果将一个闭包传递给该函数，则将执行该闭包并返回其结果：

```php
$result = value(true);

// true

$result = value(function () {
    return false;
});

// false
```

#### `view()`

`view` 函数获取一个 [视图](https://learnku.com/docs/laravel/5.5/views) 实例：

```php
return view('auth.login');
```

#### `with()`

`with` 函数会返回给定的值。如果传入一个闭包作为该函数的第二个参数，会返回闭包执行的结果：

```php
$callback = function ($value) {
    return (is_numeric($value)) ? $value * 2 : 0;
};

$result = with(5, $callback);

// 10

$result = with(null, $callback);

// 0

$result = with(5, null);

// 5
```

## 译者署名

| 用户名 | 头像 | 职能 | 签名 |
| --- | --- | --- | --- |
| [Seven Du](https://github.com/medz) | ![nSIABAcJ8T.jpeg!large](https://cdn.learnku.com/uploads/images/201910/21/23464/nSIABAcJ8T.jpeg!large) | 翻译 | 基于 Laravel 的社交开源系统 [ThinkSNS+](https://github.com/slimkit/thinksns-plus) 欢迎 Star。 |
| [@JokerLinly](https://learnku.com/users/5350) | ![5350_1481857380.jpg](https://cdn.learnku.com/uploads/avatars/5350_1481857380.jpg) | Review | Stay Hungry. Stay Foolish. |

* * *

> {note} 欢迎任何形式的转载，但请务必注明出处，尊重他人劳动共创开源社区。
> 
> 转载请注明：本文档由 Laravel China 社区 [laravel-china.org](https://laravel-china.org/) 组织翻译，详见 [翻译召集帖](https://learnku.com/laravel/t/5756/laravel-55-document-translation-call-come-and-join-the-translation)。
> 
> 文档永久地址： [《Laravel 中文文档》](https://learnku.com/docs/laravel)