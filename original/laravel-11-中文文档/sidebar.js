{
    "/laravel-11-中文文档/": [
        {
            "text": "前言",
            "children": [
                {
                    "text": "发行说明",
                    "link": "01前言/01.发行说明.md"
                },
                {
                    "text": "升级指南",
                    "link": "01前言/02.升级指南.md"
                },
                {
                    "text": "贡献导引",
                    "link": "01前言/03.贡献导引.md"
                }
            ]
        },
        {
            "text": "入门指南",
            "children": [
                {
                    "text": "安装",
                    "link": "02入门指南/01.安装.md"
                },
                {
                    "text": "配置信息",
                    "link": "02入门指南/02.配置信息.md"
                },
                {
                    "text": "目录结构",
                    "link": "02入门指南/03.目录结构.md"
                },
                {
                    "text": "前端",
                    "link": "02入门指南/04.前端.md"
                },
                {
                    "text": "Breeze",
                    "link": "02入门指南/05.Breeze.md"
                },
                {
                    "text": "部署",
                    "link": "02入门指南/06.部署.md"
                }
            ]
        },
        {
            "text": "核心架构",
            "children": [
                {
                    "text": "请求周期",
                    "link": "03核心架构/01.请求周期.md"
                },
                {
                    "text": "服务容器",
                    "link": "03核心架构/02.服务容器.md"
                },
                {
                    "text": "服务提供者",
                    "link": "03核心架构/03.服务提供者.md"
                },
                {
                    "text": "Facades",
                    "link": "03核心架构/04.Facades.md"
                }
            ]
        },
        {
            "text": "基础功能",
            "children": [
                {
                    "text": "路由",
                    "link": "04基础功能/01.路由.md"
                },
                {
                    "text": "中间件",
                    "link": "04基础功能/02.中间件.md"
                },
                {
                    "text": "CSRF 保护",
                    "link": "04基础功能/03.CSRF 保护.md"
                },
                {
                    "text": "控制器",
                    "link": "04基础功能/04.控制器.md"
                },
                {
                    "text": "请求",
                    "link": "04基础功能/05.请求.md"
                },
                {
                    "text": "响应",
                    "link": "04基础功能/06.响应.md"
                },
                {
                    "text": "视图",
                    "link": "04基础功能/07.视图.md"
                },
                {
                    "text": "Blade 模板",
                    "link": "04基础功能/08.Blade 模板.md"
                },
                {
                    "text": "Vite 编译 Assets",
                    "link": "04基础功能/09.Vite 编译 Assets.md"
                },
                {
                    "text": "生成 URL",
                    "link": "04基础功能/10.生成 URL.md"
                },
                {
                    "text": "登录和会话",
                    "link": "04基础功能/11.登录和会话.md"
                },
                {
                    "text": "表单验证",
                    "link": "04基础功能/12.表单验证.md"
                },
                {
                    "text": "错误处理",
                    "link": "04基础功能/13.错误处理.md"
                },
                {
                    "text": "日志",
                    "link": "04基础功能/14.日志.md"
                }
            ]
        },
        {
            "text": "继续深入",
            "children": [
                {
                    "text": "Artisan 命令行",
                    "link": "05继续深入/01.Artisan 命令行.md"
                },
                {
                    "text": "广播系统",
                    "link": "05继续深入/02.广播系统.md"
                },
                {
                    "text": "缓存系统",
                    "link": "05继续深入/03.缓存系统.md"
                },
                {
                    "text": "集合",
                    "link": "05继续深入/04.集合.md"
                },
                {
                    "text": "Context 上下文",
                    "link": "05继续深入/05.Context 上下文.md"
                },
                {
                    "text": "Contracts",
                    "link": "05继续深入/06.Contracts.md"
                },
                {
                    "text": "事件系统",
                    "link": "05继续深入/07.事件系统.md"
                },
                {
                    "text": "文件存储",
                    "link": "05继续深入/08.文件存储.md"
                },
                {
                    "text": "辅助函数",
                    "link": "05继续深入/09.辅助函数.md"
                },
                {
                    "text": "HTTP 客户端",
                    "link": "05继续深入/10.HTTP 客户端.md"
                },
                {
                    "text": "本地化",
                    "link": "05继续深入/11.本地化.md"
                },
                {
                    "text": "发送邮件",
                    "link": "05继续深入/12.发送邮件.md"
                },
                {
                    "text": "消息通知",
                    "link": "05继续深入/13.消息通知.md"
                },
                {
                    "text": "扩展包开发",
                    "link": "05继续深入/14.扩展包开发.md"
                },
                {
                    "text": "进程管理",
                    "link": "05继续深入/15.进程管理.md"
                },
                {
                    "text": "队列",
                    "link": "05继续深入/16.队列.md"
                },
                {
                    "text": "请求限流",
                    "link": "05继续深入/17.请求限流.md"
                },
                {
                    "text": "字符串操作",
                    "link": "05继续深入/18.字符串操作.md"
                },
                {
                    "text": "任务调度",
                    "link": "05继续深入/19.任务调度.md"
                }
            ]
        },
        {
            "text": "安全相关",
            "children": [
                {
                    "text": "用户认证",
                    "link": "06安全相关/01.用户认证.md"
                },
                {
                    "text": "用户授权",
                    "link": "06安全相关/02.用户授权.md"
                },
                {
                    "text": "Email 认证",
                    "link": "06安全相关/03.Email 认证.md"
                },
                {
                    "text": "加密解密",
                    "link": "06安全相关/04.加密解密.md"
                },
                {
                    "text": "哈希",
                    "link": "06安全相关/05.哈希.md"
                },
                {
                    "text": "重置密码",
                    "link": "06安全相关/06.重置密码.md"
                }
            ]
        },
        {
            "text": "数据库",
            "children": [
                {
                    "text": "数据库入门",
                    "link": "07数据库/01.数据库入门.md"
                },
                {
                    "text": "查询构造器",
                    "link": "07数据库/02.查询构造器.md"
                },
                {
                    "text": "分页",
                    "link": "07数据库/03.分页.md"
                },
                {
                    "text": "数据库迁移",
                    "link": "07数据库/04.数据库迁移.md"
                },
                {
                    "text": "数据填充",
                    "link": "07数据库/05.数据填充.md"
                },
                {
                    "text": "Redis",
                    "link": "07数据库/06.Redis.md"
                }
            ]
        },
        {
            "text": "Eloquent ORM",
            "children": [
                {
                    "text": "快速入门",
                    "link": "08Eloquent ORM/01.快速入门.md"
                },
                {
                    "text": "模型关联",
                    "link": "08Eloquent ORM/02.模型关联.md"
                },
                {
                    "text": "Eloquent 集合",
                    "link": "08Eloquent ORM/03.Eloquent 集合.md"
                },
                {
                    "text": "属性修改器",
                    "link": "08Eloquent ORM/04.属性修改器.md"
                },
                {
                    "text": "API 资源",
                    "link": "08Eloquent ORM/05.API 资源.md"
                },
                {
                    "text": "序列化",
                    "link": "08Eloquent ORM/06.序列化.md"
                },
                {
                    "text": "Eloquent 数据工厂",
                    "link": "08Eloquent ORM/07.Eloquent 数据工厂.md"
                }
            ]
        },
        {
            "text": "测试相关",
            "children": [
                {
                    "text": "快速入门",
                    "link": "09测试相关/01.快速入门.md"
                },
                {
                    "text": "HTTP 测试",
                    "link": "09测试相关/02.HTTP 测试.md"
                },
                {
                    "text": "命令行测试",
                    "link": "09测试相关/03.命令行测试.md"
                },
                {
                    "text": "Dusk",
                    "link": "09测试相关/04.Dusk.md"
                },
                {
                    "text": "数据库测试",
                    "link": "09测试相关/05.数据库测试.md"
                },
                {
                    "text": "测试模拟器 Mocking",
                    "link": "09测试相关/06.测试模拟器 Mocking.md"
                }
            ]
        },
        {
            "text": "官方扩展包",
            "children": [
                {
                    "text": "交易工具包 (Stripe)",
                    "link": "10官方扩展包/01.交易工具包 (Stripe).md"
                },
                {
                    "text": "交易工具包 (Paddle)",
                    "link": "10官方扩展包/02.交易工具包 (Paddle).md"
                },
                {
                    "text": "Envoy 部署工具",
                    "link": "10官方扩展包/03.Envoy 部署工具.md"
                },
                {
                    "text": "Fortify 授权生成器",
                    "link": "10官方扩展包/04.Fortify 授权生成器.md"
                },
                {
                    "text": "Folio 路由",
                    "link": "10官方扩展包/05.Folio 路由.md"
                },
                {
                    "text": "Homestead 虚拟机",
                    "link": "10官方扩展包/06.Homestead 虚拟机.md"
                },
                {
                    "text": "Horizon 队列管理工具",
                    "link": "10官方扩展包/07.Horizon 队列管理工具.md"
                },
                {
                    "text": "Mix",
                    "link": "10官方扩展包/08.Mix.md"
                },
                {
                    "text": "Octane（加速引擎）",
                    "link": "10官方扩展包/09.Octane（加速引擎）.md"
                },
                {
                    "text": "Passport OAuth 认证",
                    "link": "10官方扩展包/10.Passport OAuth 认证.md"
                },
                {
                    "text": "Pennant",
                    "link": "10官方扩展包/11.Pennant.md"
                },
                {
                    "text": "Pint",
                    "link": "10官方扩展包/12.Pint.md"
                },
                {
                    "text": "Precognition",
                    "link": "10官方扩展包/13.Precognition.md"
                },
                {
                    "text": "Prompts 命令行表单",
                    "link": "10官方扩展包/14.Prompts 命令行表单.md"
                },
                {
                    "text": "Pulse 性能监控",
                    "link": "10官方扩展包/15.Pulse 性能监控.md"
                },
                {
                    "text": "Reverb WebSocket",
                    "link": "10官方扩展包/16.Reverb WebSocket.md"
                },
                {
                    "text": "Sail 开发环境",
                    "link": "10官方扩展包/17.Sail 开发环境.md"
                },
                {
                    "text": "Sanctum API 授权",
                    "link": "10官方扩展包/18.Sanctum API 授权.md"
                },
                {
                    "text": "Scout 全文搜索",
                    "link": "10官方扩展包/19.Scout 全文搜索.md"
                },
                {
                    "text": "Socialite 社会化登录",
                    "link": "10官方扩展包/20.Socialite 社会化登录.md"
                },
                {
                    "text": "Telescope 调试工具",
                    "link": "10官方扩展包/21.Telescope 调试工具.md"
                },
                {
                    "text": "Valet Mac 集成环境",
                    "link": "10官方扩展包/22.Valet Mac 集成环境.md"
                }
            ]
        }
    ]
}