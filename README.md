
## 已收录文档: 
|             |               |            |
|-------------|---------------|------------|
| laravel 5.5 | laravel 6     | laravel 10 |
| laravel 11  | dcat admin 2x | Yii2       |
| 重构2       |               |            |
|             |               |            |


## 阅读方法: 
1. 可使用 Web IDE 直接查看, 点击克隆/下载左边的显示器图标, 进入 Web IDE 模式. 进入 docs 目录下即可查看相应文档, 记得切换成 markdown 预览食用最佳.

2. 启动一个web服务, 目录指向 \\docs\\.vuepress\\dist, 即可访问

3. 本地部署, 需要安装node环境.

## 本地部署

软件版本: 
- node: 20.15.0
- npm: 10.7.0

运行本地服务:
```
npm run docs:dev
```

打包:
```
npm run docs:build
```

打包后的静态资源在这个目录: docs\\.vuepress\\dist, nginx配置指向这个目录即可访问.

---
欢迎提交PR
