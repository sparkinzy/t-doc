## 目录结构

Yii 中文文档 /  

## 目录结构

安装 Yii 后，就有了一个可运行的 Yii 应用，根据配置的不同，可以通过 `http://hostname/basic/web/index.php` 或 `http://hostname/index.php` 访问。 本章节将介绍应用的内建功能，如何组织代码，以及一般情况下应用如何处理请求。

> Info: 为简单起见，在整个“入门”板块都假定你已经把 `basic/web` 设为 Web 服务器根目录并配置完毕， 你访问应用的地址会是 `http://hostname/index.php` 或类似的。 请按需调整 URL。

注意项目模板和框架完全不同，安装完之后全都归你了。你可以根据你的需要自由的添加或删除代码和 修改全部的。

## 基础应用模板目录结构

应用中最重要的目录和文件（假设应用根目录是 `basic`）：

```php
basic/                  应用根目录
    assets/             前端资源包配置
    commands/           控制台命令类
    config/             应用配置及其它配置
        console.php     控制台应用配置信息
        web.php         Web 应用配置信息
    controllers/        控制器类
    mail/               电子邮件模板
    models/             模型类
    runtime/            Yii 在运行时生成的文件，例如日志和缓存文件
    tests/              测试相关
    vagrant/            虚拟机相关配置
    vendor/             已经安装的 Composer 包，包括 Yii 框架自身
    views/              视图文件
    web/                Web 应用根目录，包含 Web 入口文件
        assets/         Yii 发布的资源文件（javascript 和 css）
        index.php       应用入口文件
    widgets/            前端小部件
    .gitignore          git 版本系统忽略的目录列表
    composer.json       Composer 配置文件, 描述包信息
    requirements.php    安装使用 Yii 需求检查器脚本
    yii                 Yii 控制台命令执行脚本
    yii.bat             Yii 控制台命令执行脚本（Windows）
```

一般来说，应用中的文件可被分为两类：在 `basic/web` 下的和在其它目录下的。 前者可以直接通过 HTTP 访问（例如浏览器），后者不能也不应该被直接访问。

Yii 实现了 [模型-视图-控制器（MVC）](http://wikipedia.org/wiki/Model-view-controller)设计模式，这点在上述目录结构中也得以体现。 `models` 目录包含了所有 [模型类](https://learnku.com/docs/yii-framework/2.0.x/models/12003)， `views` 目录包含了所有 [视图脚本](https://learnku.com/docs/yii-framework/2.0.x/views/12005)， `controllers` 目录包含了所有 [控制器类](https://learnku.com/docs/yii-framework/2.0.x/controller/12002)。

以下图表展示了一个应用的静态结构：

![应用静态结构](https://cdn.learnku.com/uploads/images/202201/29/93088/oI8mIkXDFx.png!large)

每个应用都有一个入口脚本 `web/index.php`，这是整个应用中唯一可以访问的 PHP 脚本。 入口脚本接受一个 Web 请求并创建 [应用](https://learnku.com/docs/yii-framework/2.0.x/application-subject/12000) 实例去处理它。 应用在它的 [组件](https://learnku.com/docs/yii-framework/2.0.x/application-component/12001) 辅助下解析请求，并分派请求至 MVC 元素。视图使用 [小部件](https://learnku.com/docs/yii-framework/2.0.x/widgets/12020) 去创建复杂和动态的用户界面。

> 💖喜欢本文档的，欢迎点赞、收藏、留言或转发，谢谢支持！  
> 作者邮箱：zhuzixian520@126.com，github地址：[github.com/zhuzixian520](https://github.com/zhuzixian520)