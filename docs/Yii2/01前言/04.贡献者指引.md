## 贡献者指引

+   [报告问题](#2e79fd "报告问题")
+   [Git 工作流程](#db1af3 "Git 工作流程")

## 报告问题（Report an Issue）

在创建一个问题的时候，为了能更迅速地解决问题，请遵循以下原则:

+   提供的信息包括：PHP 和 Yii 的版本，操作系统和 Web 服务器的类型，浏览器的类型和版本；
+   如果可用，请提供 **完整的** 错误调用堆栈。我们非常欢迎用截图来解释这个问题。
+   描述问题复现的步骤，当然，如果您能够提供代码来复现这个问题，那就更好了。
+   如果可能的话，你甚至可以创建一个失败的单元测试，并将其 [作为拉请求（pull request）发送](#db1af3)。

如果该问题与某个官方扩展相关，请在扩展仓库的问题跟踪器（issue tracker）中报告它。  
如果您不确定，可以 [向主仓库报告](https://github.com/yiisoft/yii2/issues/new "向主仓库报告")（[github.com/yiisoft/yii2/issues](https://github.com/yiisoft/yii2/issues)）。

**在以下情况下，请不要报告问题：**

+   当您正在问如何使用一些 Yii 的特性。此时，您应该使用 [论坛](https://forum.yiiframework.com/index.php/forum/42-general-discussions-for-yii-20/) 或 [聊天室](https://www.yiiframework.com/chat/) 来达到这个目的。
+   当您的问题是关于安全的时候，请 [直接联系我们](https://www.yiiframework.com/security/) 报告安全问题。

**避免重复问题**

在您报告问题之前，请搜索查遍 [现有的问题](https://github.com/yiisoft/yii2/issues)，来看看您的问题是否已经报告或者修复，以确保您没有报告重复的问题。而且，您还要确保您有最新版本的 Yii，并看看这个问题是否仍然存在。

## Git 工作流程

您想为Yii做贡献吗？太好了！但是，为了增加您的更改被快速接受的可能性，请遵循以下步骤。如果您是第一次接触 Git 和 GitHub ，您可能想先看看 [GitHub 帮助内容](http://help.github.com/cn)，[尝试 Git](https://docs.github.com/cn/get-started) 或者学习一些关于 [Git 内部数据模型](http://nfarina.com/post/9868516270/git-is-simpler) 的知识。

### 准备开发环境

下面的步骤将为 Yi i创建一个开发环境，您可以使用它来处理 Yii 框架的核心代码。这些步骤只需要在你第一次贡献的时候完成。

1.  在 GitHub 上 [Fork](https://docs.github.com/cn/get-started/quickstart/fork-a-repo) Yii 代码库，并将你的 Fork 克隆到你的开发环境中

```shell
git clone git@github.com:YOUR-GITHUB-USERNAME/yii2.git
```

如果你在 Linux 中使用 GitHub 设置 Git 有困难，或者出现 “Permission Denied (publickey)” 这样的错误，那么你必须 [设置你的 Git 安装来使用 GitHub](https://docs.github.com/cn/get-started/quickstart/set-up-git)

> 温馨提示：如果你对 Git 不太熟悉，我们推荐你阅读一本很棒的免费的 [Pro Git book](https://git-scm.com/book/zh/v2)。

2.  将 Yii 主仓库添加为一个额外的远程 git，称为 “upstream”。

切换到你克隆 Yii 的目录，通常是 “yii2”。然后输入以下命令：

```shell
git remote add upstream git://github.com/yiisoft/yii2.git
```

3.  准备测试环境

如果您只想处理翻译或文档，则不需要执行以下步骤。

+   运行 `composer install` 来安装依赖项（假设您已经完成 [composer 全局安装](https://getcomposer.org/doc/00-intro.md#globally)）。

如果你打算使用 JavaScript ：

+   运行 `npm install` 来安装 JavaScript 测试工具和依赖关系（假设您已经完成 [Node.js and NPM 安装](https://nodejs.org/zh-cn/download/package-manager/)）。

> Note: JavaScript tests depend on [jsdom](https://github.com/tmpvar/jsdom) library which requires Node.js 4 or newer.  
> Using of Node.js 6 or 7 is more preferable.

+   run `php build/build dev/app basic <fork>` to clone the basic app and install composer dependencies for the basic app.  
    `<fork>` is URL of your repository fork such as `[](mailto:git@github.com)[git@github.com](mailto:git@github.com):my_nickname/yii2-app-basic.git`. If you are core framework contributor you may skip specifying fork.  
    This command will install foreign composer packages as normal but will link the yii2 repo to  
    the currently checked out repo, so you have one instance of all the code installed.
    
    Do the same for the advanced app if needed: `php build/build dev/app advanced <fork>`.
    
    This command will also be used to update dependencies, it runs `composer update` internally.
    

> Note: The default git repository Urls clone from github via SSH, you may add the `--useHttp` flag to the `build` command  
> to use HTTPs instead.

**现在，您已经拥有了修改 Yii 2 的工作背景**

以下步骤是可选的。

4.  单元测试

You can execute unit tests by running `phpunit` in the repo root directory. If you do not have phpunit installed globally  
you can run `php vendor/bin/phpunit` or `vendor/bin/phpunit.bat` in case of execution from the Windows OS.

Some tests require additional databases to be set up and configured. You can create `tests/data/config.local.php` to override  
settings that are configured in `tests/data/config.php`.

You may limit the tests to a group of tests you are working on e.g. to run only tests for the validators and redis  
`phpunit --group=validators,redis`. You get the list of available groups by running `phpunit --list-groups`.

You can execute JavaScript unit tests by running `npm test` in the repo root directory.

5.  Extensions

To work on extensions you have to clone the extension repository. We have created a command that can do this for you:

```shell
php build/build dev/ext <extension-name> <fork>
```

where `<extension-name>` is the name of the extension, e.g. `redis` and `<fork>` is URL of your extension fork such as `[git@github.com](mailto:git@github.com):my_nickname/yii2-redis.git`. If you are core framework contributor you may skip specifying fork.

If you want to test the extension in one of the application templates, just add it to the `composer.json` of the application as you would  
normally do e.g. add `"yiisoft/yii2-redis": "~2.0.0"` to the `require` section of the basic app.  
Running `php build/build dev/app basic <fork>` will install the extension and its dependencies and create  
a symlink to `extensions/redis` so you are not working in the composer vendor dir but in the yii2 repository directly.

> Note: The default git repository Urls clone from github via SSH, you may add the `--useHttp` flag to the `build` command  
> to use HTTPs instead.

### 致力于 bugs 和 features

Having prepared your develop environment as explained above you can now start working on the feature or bugfix.

1.  Make sure there is an issue created for the thing you are working on if it requires significant effort to fix

All new features and bug fixes should have an associated issue to provide a single point of reference for discussion  
and documentation. Take a few minutes to look through the existing issue list for one that matches the contribution you  
intend to make. If you find one already on the issue list, then please leave a comment on that issue indicating you  
intend to work on that item. If you do not find an existing issue matching what you intend to work on, please  
[open a new issue](https://learnku.com/docs/yii-framework/2.0.x/contributor-guidelines/report-an-issue.md) or create a pull request directly if it is straightforward fix. This will allow the team to  
review your suggestion, and provide appropriate feedback along the way.

> 对于小的变化或文档问题或简单的修复，你不需要创建一个问题，一个pull request在这种情况下就足够了。

2.  从 Yii 主分支 Pull 最新的代码

```shell
git pull upstream
```

对于每一个新的贡献，您都应该从这一点开始，以确保您正在处理最新的代码。

3.  基于当前的 Yii 主分支为你的特性创建一个新的分支

> That’s very important since you will not be able to submit more than one pull request from your account if you’ll use master.

每个单独的 bug 修复或更改应该放在它自己的分支中。分支名称应该是描述性的，并从与代码相关的问题的编号开始。如果你没有解决任何特定的问题，请跳过编号。  
例如：

```shell
git checkout upstream/master
git checkout -b 999-name-of-your-branch-goes-here
```

4.  施展你的魔法，写你的代码

确保它能工作 :)

单元测试总是受欢迎的。经过测试和良好覆盖的代码极大地简化了检查贡献的任务。  
作为问题描述的失败的单元测试也被接受。

5.  更新 CHANGELOG

Edit the CHANGELOG file to include your change, you should insert this at the top of the file under the first heading (the version that is currently under development), the line in the change log should look like one of the following:

```php
Bug #999: a description of the bug fix (Your Name)
Enh #999: a description of the enhancement (Your Name)
```

`#999` is the issue number that the `Bug` or `Enh` is referring to.  
The changelog should be grouped by type (`Bug`,`Enh`) and ordered by issue number.

For very small fixes, e.g. typos and documentation changes, there is no need to update the CHANGELOG.

6.  提交您的更改

add the files/changes you want to commit to the [staging area](http://git.github.io/git-reference/basic/#add) with

```php
git add path/to/my/file.php
```

You can use the `-p` option to select the changes you want to have in your commit.

Commit your changes with a descriptive commit message. Make sure to mention the ticket number with `#XXX` so GitHub will automatically link your commit with the ticket:

```shell
git commit -m "A brief description of this change which fixes #999 goes here"
```

7.  从上游拉取最新的 Yii 代码到你的分支中

```shell
git pull upstream master
```

This ensures you have the latest code in your branch before you open your pull request. If there are any merge conflicts,  
you should fix them now and commit the changes again. This ensures that it’s easy for the Yii team to merge your changes  
with one click.

8.  当解决任何冲突后，将你的代码推送到 GitHub

```shell
git push -u origin 999-name-of-your-branch-goes-here
```

The `-u` parameter ensures that your branch will now automatically push and pull from the GitHub branch. That means  
if you type `git push` the next time it will know where to push to. This is useful if you want to later add more commits  
to the pull request.

9.  针对上游打开一个 [拉取请求（pull request）](https://help.github.com/articles/creating-a-pull-request-from-a-fork/)。

Go to your repository on GitHub and click “Pull Request”, choose your branch on the right and enter some more details in the comment box. To link the pull request to the issue put anywhere in the pull comment `#999` where 999 is the issue number.

> Note that each pull-request should fix a single change. For multiple, unrelated changes, please open multiple pull requests.

10.  有人会检查你的代码

Someone will review your code, and you might be asked to make some changes, if so go to step #6 (you don’t need to open another pull request if your current one is still open). If your code is accepted it will be merged into the main branch and become part of the next Yii release. If not, don’t be disheartened, different people need different features and Yii can’t be everything to everyone, your code will still be available on GitHub as a reference for people who need it.

11.  清理

After your code was either accepted or declined you can delete branches you’ve worked with from your local repository  
and `origin`.

```shell
git checkout master
git branch -D 999-name-of-your-branch-goes-here
git push origin --delete 999-name-of-your-branch-goes-here
```

+   注意：

To detect regressions early every merge to the Yii codebase on GitHub will be picked up by  
[Travis CI](http://travis-ci.com/) for an automated testrun. As core team doesn’t wish to overtax this service,  
[`[ci skip]`](https://docs.travis-ci.com/user/customizing-the-build/#Skipping-a-build) will be included to the merge description if the pull request:

+   affect javascript, css or image files only,
+   updates the documentation,
+   modify fixed strings only (e.g. translation updates)

Doing so will save travis from commencing testruns on changes that are not covered by tests in the first place.

+   命令行概述 (针对高级贡献者)

```shell
git clone git@github.com:YOUR-GITHUB-USERNAME/yii2.git
git remote add upstream git://github.com/yiisoft/yii2.git
```

```shell
git fetch upstream
git checkout upstream/master
git checkout -b 999-name-of-your-branch-goes-here

/* do your magic, update changelog if needed */

git add path/to/my/file.php
git commit -m "这个 fixes #999 的简要描述在这里"
git pull upstream master
git push -u origin 999-name-of-your-branch-goes-here
```

> 💖喜欢本文档的，欢迎点赞、收藏、留言或转发，谢谢支持！  
> 作者邮箱：zhuzixian520@126.com，github地址：[github.com/zhuzixian520](https://github.com/zhuzixian520)