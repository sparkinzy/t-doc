export const refactoring2 = {
    "/refactoring2/": [
        { "text": "序言", "link": "序言.md" },
        { "text": "第1章 重构, 第一个示例", "link": "ch1.md" },
        { "text": "第2章 重构的原则", "link": "ch2.md" },
        { "text": "第3章 代码的坏味道", "link": "ch3.md" },
        { "text": "第4章 构筑测试体系", "link": "ch4.md" },
        { "text": "第5章 介绍重构名录", "link": "ch5.md" },
        { "text": "第6章 第一组重构", "link": "ch6.md" },
        { "text": "第7章 封装", "link": "ch7.md" },
        { "text": "第8章 搬移特性", "link": "ch8.md" },
        { "text": "第9章 重新组织数据", "link": "ch9.md" },
        { "text": "第10章 简化条件逻辑", "link": "ch10.md" },
        { "text": "第11章 重构API", "link": "ch11.md" },
        { "text": "第12章 处理继承关系", "link": "ch12.md" },
    ]
}