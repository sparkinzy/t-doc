import{_ as n,c as s,o as a,b as e}from"./app-ColBfiOR.js";const p={},l=e(`<p>本文档最新版为 <a href="https://learnku.com/docs/laravel/10.x" target="_blank" rel="noopener noreferrer">10.x</a>，旧版本可能放弃维护，推荐阅读最新版！</p><h2 id="events" tabindex="-1"><a class="header-anchor" href="#events"><span>Events</span></a></h2><ul><li><a href="#introduction">简介</a></li><li><a href="#registering-events-and-listeners">注册事件 &amp; 监听器</a><ul><li><a href="#generating-events-and-listeners">生成事件 &amp; 监听器</a></li><li><a href="#manually-registering-events">手动注册事件</a></li><li><a href="#event-discovery">事件发现</a></li></ul></li><li><a href="#defining-events">定义事件</a></li><li><a href="#defining-listeners">定义监听器</a></li><li><a href="#queued-event-listeners">事件监听器队列</a><ul><li><a href="#manually-accessing-the-queue">手动访问队列</a></li><li><a href="#handling-failed-jobs">处理失败任务</a></li></ul></li><li><a href="#dispatching-events">分发事件</a></li><li><a href="#event-subscribers">事件订阅者</a><ul><li><a href="#writing-event-subscribers">编写事件订阅者</a></li><li><a href="#registering-event-subscribers">注册事件订阅者</a></li></ul></li></ul><h2 id="事件系统介绍" tabindex="-1"><a class="header-anchor" href="#事件系统介绍"><span>事件系统介绍</span></a></h2><p>Laravel的事件提供了一个简单的观察者实现，允许你在应用中订阅和监听各种发生的事件。事件类通常放在<code>app/Events</code>目录下，而这些事件类的监听器则放在<code>app/Listeners</code>目录下。如果在你的应用中你没有看到这些目录，不用担心，它们会在你使用 Artisan 控制台命令生成事件与监听器的时候自动创建。<br> 事件系统为应用各个方面的解耦提供了非常棒的方法，因为单个事件可以拥有多个互不依赖的监听器。举个例子，你可能希望每次订单发货时向用户推送一个 Slack 通知。你可以简单地发起一个可以被监听器接收并转化为 Slack 通知的 <code>OrderShipped</code> 事件，而不是将订单处理代码和 Slack 通知代码耦合在一起。</p><h2 id="注册事件和监听器" tabindex="-1"><a class="header-anchor" href="#注册事件和监听器"><span>注册事件和监听器</span></a></h2><p>Laravel 应用中的 <code>EventServiceProvider</code> 为注册所有的事件监听器提供了一个便利的场所。其中， <code>listen</code> 属性包含了所有事件 (键) 以及事件对应的监听器 (值) 的数组。当然，你可以根据应用的需要，添加多个事件到 <code>listen</code>属性包含的数组中。举个例子，我们来添加一个 <code>OrderShipped</code> 事件：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token doc-comment comment">/**</span>
<span class="line"> * 应用程序的事件监听器映射</span>
<span class="line"> *</span>
<span class="line"> * <span class="token keyword">@var</span> <span class="token class-name"><span class="token keyword">array</span></span></span>
<span class="line"> */</span></span>
<span class="line"><span class="token keyword">protected</span> <span class="token variable">$listen</span> <span class="token operator">=</span> <span class="token punctuation">[</span></span>
<span class="line">    <span class="token string single-quoted-string">&#39;App\\Events\\OrderShipped&#39;</span> <span class="token operator">=&gt;</span> <span class="token punctuation">[</span></span>
<span class="line">        <span class="token string single-quoted-string">&#39;App\\Listeners\\SendShipmentNotification&#39;</span><span class="token punctuation">,</span></span>
<span class="line">    <span class="token punctuation">]</span><span class="token punctuation">,</span></span>
<span class="line"><span class="token punctuation">]</span><span class="token punctuation">;</span></span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="生成事件-监听器" tabindex="-1"><a class="header-anchor" href="#生成事件-监听器"><span>生成事件 &amp; 监听器</span></a></h3><p>当然，手动创建事件和监听器的文件是件麻烦事。而在这里，你只需要将监听器和事件添加到 <code>EventServiceProvider</code> 中，而后使用 <code>event:generate</code> 命令。这个命令会生成在 <code>EventServiceProvider</code> 中列出的所有事件和监听器。当然，已经存在的事件和监听器将保持不变：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line">php artisan event<span class="token punctuation">:</span>generate</span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div></div></div><h3 id="手动注册事件" tabindex="-1"><a class="header-anchor" href="#手动注册事件"><span>手动注册事件</span></a></h3><p>通常，事件是在 <code>EventServiceProvider</code> 的 <code>$listen</code> 数组中注册；然而，你也可以在 <code>EventServiceProvider</code> 的 <code>boot</code> 方法中手动注册基于闭包的这些事件：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token doc-comment comment">/**</span>
<span class="line"> * 注册应用中的其它事件</span>
<span class="line"> *</span>
<span class="line"> * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line"> */</span></span>
<span class="line"><span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">boot</span><span class="token punctuation">(</span><span class="token punctuation">)</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token keyword static-context">parent</span><span class="token operator">::</span><span class="token function">boot</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token class-name static-context">Event</span><span class="token operator">::</span><span class="token function">listen</span><span class="token punctuation">(</span><span class="token string single-quoted-string">&#39;event.name&#39;</span><span class="token punctuation">,</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token variable">$foo</span><span class="token punctuation">,</span> <span class="token variable">$bar</span><span class="token punctuation">)</span> <span class="token punctuation">{</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="通配符事件监听器" tabindex="-1"><a class="header-anchor" href="#通配符事件监听器"><span>通配符事件监听器</span></a></h4><p>你可以在注册监听器时使用 * 作为通配符参数，这样可以在同一个监听器上捕获多个事件。通配符监听器接收事件名作为其第一个参数，并将整个事件数据数组作为其第二个参数：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token class-name static-context">Event</span><span class="token operator">::</span><span class="token function">listen</span><span class="token punctuation">(</span><span class="token string single-quoted-string">&#39;event.*&#39;</span><span class="token punctuation">,</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token variable">$eventName</span><span class="token punctuation">,</span> <span class="token keyword type-hint">array</span> <span class="token variable">$data</span><span class="token punctuation">)</span> <span class="token punctuation">{</span></span>
<span class="line">    <span class="token comment">//</span></span>
<span class="line"><span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="事件发现" tabindex="-1"><a class="header-anchor" href="#事件发现"><span>事件发现</span></a></h3><blockquote><p>注意：事件发现可用于Laravel 5.8.9或更高版本。</p></blockquote><p>您可以启用自动事件发现，而不是在<code>EventServiceProvider</code>的<code>$listen</code>数组中手动注册事件和监听器。 启用事件发现后，Laravel将通过扫描应用程序的<code>Listeners</code>目录自动查找并注册您的事件和侦听器。 此外，<code>EventServiceProvider</code>中列出的任何明确定义的事件仍将被注册。</p><p>Laravel通过使用反射扫描监听器类来查找事件监听器。 当Laravel找到以<code>handle</code>开头的监听器类方法时，Laravel会将这些方法注册为方法签名中类型提示的事件的事件监听器：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>PodcastProcessed</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">SendPodcastProcessedNotification</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理给定的事件。</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>PodcastProcessed</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handle</span><span class="token punctuation">(</span><span class="token class-name type-declaration">PodcastProcessed</span> <span class="token variable">$event</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>默认情况下禁用事件发现，但您可以通过覆盖应用程序的<code>EventServiceProvider</code>的<code>shouldDiscoverEvents</code>方法来启用它：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token doc-comment comment">/**</span>
<span class="line"> * 确定是否应自动发现事件和侦听器。</span>
<span class="line"> *</span>
<span class="line"> * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">bool</span></span></span>
<span class="line"> */</span></span>
<span class="line"><span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">shouldDiscoverEvents</span><span class="token punctuation">(</span><span class="token punctuation">)</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token keyword">return</span> <span class="token constant boolean">true</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>默认情况下，将扫描应用程序的Listeners目录中的所有监听器。 如果要定义扫描的其他目录，可以覆盖<code>EventServiceProvider</code>中的<code>discoverEventsWithin</code>方法：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token doc-comment comment">/**</span>
<span class="line"> * 获取应该用于发现事件的监听器目录</span>
<span class="line"> *</span>
<span class="line"> * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">array</span></span></span>
<span class="line"> */</span></span>
<span class="line"><span class="token keyword">protected</span> <span class="token keyword">function</span> <span class="token function-definition function">discoverEventsWithin</span><span class="token punctuation">(</span><span class="token punctuation">)</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token keyword">return</span> <span class="token punctuation">[</span></span>
<span class="line">        <span class="token variable">$this</span><span class="token operator">-&gt;</span><span class="token property">app</span><span class="token operator">-&gt;</span><span class="token function">path</span><span class="token punctuation">(</span><span class="token string single-quoted-string">&#39;Listeners&#39;</span><span class="token punctuation">)</span><span class="token punctuation">,</span></span>
<span class="line">    <span class="token punctuation">]</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>在生产中，您可能不希望框架在每个请求上扫描所有监听器。 因此，在部署过程中，您应该运行<code>event：cache</code> Artisan命令来缓存应用程序的所有事件和监听器的列表。 框架将使用此列表来加速事件注册过程。 <code>event：clear</code>命令可用于销毁缓存。</p><blockquote><p>提示：<code>event：list</code>命令可用于显示应用程序注册的所有事件和监听器的列表。</p></blockquote><h2 id="定义事件" tabindex="-1"><a class="header-anchor" href="#定义事件"><span>定义事件</span></a></h2><p>事件类是一个保存与事件相关信息的容器。例如，假设我们生成的 OrderShipped 事件接收一个 <a href="https://learnku.com/docs/laravel/6.x/eloquent" target="_blank" rel="noopener noreferrer">Eloquent ORM</a> 对象：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Events</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Order</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>SerializesModels</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">OrderShipped</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token keyword">use</span> <span class="token package">SerializesModels</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token keyword">public</span> <span class="token variable">$order</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 创建一个事件实例</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Order</span>  <span class="token parameter">$order</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">__construct</span><span class="token punctuation">(</span><span class="token class-name type-declaration">Order</span> <span class="token variable">$order</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token variable">$this</span><span class="token operator">-&gt;</span><span class="token property">order</span> <span class="token operator">=</span> <span class="token variable">$order</span><span class="token punctuation">;</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>如你所见，这个事件类中没有包含其它逻辑。它只是一个购买的 <code>Order</code> 的实例的容器。如果使用 PHP 的 <code>serialize</code> 函数序列化事件对象，事件使用的 <code>SerializesModels</code> trait 将会优雅地序列化任何 Eloquent 模型。</p><h2 id="定义监听器" tabindex="-1"><a class="header-anchor" href="#定义监听器"><span>定义监听器</span></a></h2><p>接下来，让我们看一下例子中事件的监听器。事件监听器在 <code>handle</code> 方法中接收实例。 <code>event:generate</code> 命令会自动加载正确的事件类，并且在 <code>handle</code> 方法中加入事件的类型提示。在 <code>handle</code> 方法中，你可以执行任何必要的响应事件的操作：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">SendShipmentNotification</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 创建事件监听器</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">__construct</span><span class="token punctuation">(</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理事件</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span>  <span class="token parameter">$event</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handle</span><span class="token punctuation">(</span><span class="token class-name type-declaration">OrderShipped</span> <span class="token variable">$event</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">         <span class="token comment">// 使用 $event-&gt;order 来访问 order ...</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>提示: 你的事件监听器也可以在构造函数中加入任何依赖关系的类型提示。所有的事件监听器都是通过 Laravel 的 <a href="https://learnku.com/docs/laravel/6.x/container" target="_blank" rel="noopener noreferrer">服务容器</a>解析的, 因此所有的依赖都将会被自动注入.</p></blockquote><h4 id="停止事件传播" tabindex="-1"><a class="header-anchor" href="#停止事件传播"><span>停止事件传播</span></a></h4><p>有时，你可以通过在监听器的 <code>handle</code> 方法中返回 <code>false</code> 来阻止事件被其它的监听器获取。</p><h2 id="事件监听器队列" tabindex="-1"><a class="header-anchor" href="#事件监听器队列"><span>事件监听器队列</span></a></h2><p>如果你的监听器中要执行诸如发送电子邮件或发出HTTP请求之类的耗时任务，你可以将任务丢给队列处理。在开始使用队列监听器之前，请确保在你的服务器或者本地开发环境中能够 <a href="https://learnku.com/docs/laravel/6.x/queues" target="_blank" rel="noopener noreferrer">配置队列</a>并启动一个队列监听器。</p><p>要指定监听器启动队列，你可以在监听器类中实现 <code>ShouldQueue</code> 接口。由 Artisan 命令 <code>event:generate</code> 生成的监听器已经将此接口导入到当前命名空间中，因此你可以直接使用：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Contracts<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>ShouldQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">SendShipmentNotification</span> <span class="token keyword">implements</span> <span class="token class-name">ShouldQueue</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token comment">//</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>That&#39;s it！ 现在，当这个监听器被事件调用时，事件调度器会自动使用Laravel的<a href="https://learnku.com/docs/laravel/6.x/queues" target="_blank" rel="noopener noreferrer">队列系统</a>自动排队。如果在队列中执行监听器时没有抛出异常，任务会在执行完成后自动从队列中删除。</p><h4 id="自定义队列连接-队列名称" tabindex="-1"><a class="header-anchor" href="#自定义队列连接-队列名称"><span>自定义队列连接 &amp; 队列名称</span></a></h4><p>如果你想要自定义事件监听器所使用的队列的连接和名称，你可以在监听器类中定义 <code>$connection</code>， <code>$queue</code> 或 <code>$delay</code> 属性：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Contracts<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>ShouldQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">SendShipmentNotification</span> <span class="token keyword">implements</span> <span class="token class-name">ShouldQueue</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 任务连接名称。</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@var</span> <span class="token class-name"><span class="token keyword">string</span><span class="token punctuation">|</span><span class="token keyword">null</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token variable">$connection</span> <span class="token operator">=</span> <span class="token string single-quoted-string">&#39;sqs&#39;</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 任务发送到的队列的名称.</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@var</span> <span class="token class-name"><span class="token keyword">string</span><span class="token punctuation">|</span><span class="token keyword">null</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token variable">$queue</span> <span class="token operator">=</span> <span class="token string single-quoted-string">&#39;listeners&#39;</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理任务的延迟时间.</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@var</span> <span class="token class-name"><span class="token keyword">int</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token variable">$delay</span> <span class="token operator">=</span> <span class="token number">60</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h4 id="条件监听队列" tabindex="-1"><a class="header-anchor" href="#条件监听队列"><span>条件监听队列</span></a></h4><p>有时，你可能需要根据某些运行时的数据（满足某些条件）对监听器进行排队， 为此，可以在侦听器中添加<code>shouldQueue</code>方法，以确定是否应该将监听器排队并同步执行：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderPlaced</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Contracts<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>ShouldQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">RewardGiftCard</span> <span class="token keyword">implements</span> <span class="token class-name">ShouldQueue</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 奖励礼品卡给客户</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderPlaced</span>  <span class="token parameter">$event</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handle</span><span class="token punctuation">(</span><span class="token class-name type-declaration">OrderPlaced</span> <span class="token variable">$event</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 确定监听器是否应加入队列</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderPlaced</span>  <span class="token parameter">$event</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">bool</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">shouldQueue</span><span class="token punctuation">(</span><span class="token class-name type-declaration">OrderPlaced</span> <span class="token variable">$event</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token keyword">return</span> <span class="token variable">$event</span><span class="token operator">-&gt;</span><span class="token property">order</span><span class="token operator">-&gt;</span><span class="token property">subtotal</span> <span class="token operator">&gt;=</span> <span class="token number">5000</span><span class="token punctuation">;</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="手动访问队列" tabindex="-1"><a class="header-anchor" href="#手动访问队列"><span>手动访问队列</span></a></h3><p>如果你需要手动访问监听器下面队列任务的 <code>delete</code> 和 <code>release</code> 方法，你可以通过使用 <code>Illuminate\\Queue\\InteractsWithQueue</code> trait 来实现。这个 trait 会默认加载到生成的监听器中，并提供对这些方法的访问：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>InteractsWithQueue</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Contracts<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>ShouldQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">SendShipmentNotification</span> <span class="token keyword">implements</span> <span class="token class-name">ShouldQueue</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token keyword">use</span> <span class="token package">InteractsWithQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理事件.</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span>  <span class="token parameter">$event</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handle</span><span class="token punctuation">(</span><span class="token class-name type-declaration">OrderShipped</span> <span class="token variable">$event</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token constant boolean">true</span><span class="token punctuation">)</span> <span class="token punctuation">{</span></span>
<span class="line">            <span class="token variable">$this</span><span class="token operator">-&gt;</span><span class="token function">release</span><span class="token punctuation">(</span><span class="token number">30</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line">        <span class="token punctuation">}</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="处理失败任务" tabindex="-1"><a class="header-anchor" href="#处理失败任务"><span>处理失败任务</span></a></h3><p>有时事件监听器的队列任务可能会失败。如果监听器的队列任务超过了队列中定义的最大尝试次数，则会在监听器上调用 <code>failed</code> 方法。 <code>failed</code> 方法接收事件实例和导致失败的异常作为参数：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>InteractsWithQueue</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Contracts<span class="token punctuation">\\</span>Queue<span class="token punctuation">\\</span>ShouldQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">SendShipmentNotification</span> <span class="token keyword">implements</span> <span class="token class-name">ShouldQueue</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token keyword">use</span> <span class="token package">InteractsWithQueue</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理事件.</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span>  <span class="token parameter">$event</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handle</span><span class="token punctuation">(</span><span class="token class-name type-declaration">OrderShipped</span> <span class="token variable">$event</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理失败任务</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span>  <span class="token parameter">$event</span></span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>Exception</span>  <span class="token parameter">$exception</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name"><span class="token keyword">void</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">failed</span><span class="token punctuation">(</span><span class="token class-name type-declaration">OrderShipped</span> <span class="token variable">$event</span><span class="token punctuation">,</span> <span class="token variable">$exception</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="分发事件" tabindex="-1"><a class="header-anchor" href="#分发事件"><span>分发事件</span></a></h2><p>如果要分发事件，你可以将事件实例传递给辅助函数 <code>event</code>。该辅助函数将会把事件分发到所有该事件相应的已经注册了的监听器上。<code>event</code> 辅助函数可以全局使用，你可以在应用中的任何位置进行调用：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Http<span class="token punctuation">\\</span>Controllers</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Order</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>OrderShipped</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">App<span class="token punctuation">\\</span>Http<span class="token punctuation">\\</span>Controllers<span class="token punctuation">\\</span>Controller</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">OrderController</span> <span class="token keyword">extends</span> <span class="token class-name">Controller</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 将传递过来的订单发货</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token keyword">int</span></span>  <span class="token parameter">$orderId</span></span>
<span class="line">     * <span class="token keyword">@return</span> <span class="token class-name">Response</span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">ship</span><span class="token punctuation">(</span><span class="token variable">$orderId</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token variable">$order</span> <span class="token operator">=</span> <span class="token class-name static-context">Order</span><span class="token operator">::</span><span class="token function">findOrFail</span><span class="token punctuation">(</span><span class="token variable">$orderId</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">        <span class="token comment">// Order shipment logic...</span></span>
<span class="line"></span>
<span class="line">        <span class="token function">event</span><span class="token punctuation">(</span><span class="token keyword">new</span> <span class="token class-name">OrderShipped</span><span class="token punctuation">(</span><span class="token variable">$order</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>Tip：在测试时，只需要断言特定事件被分发，而不需要真正地触发监听器。 Laravel 的<a href="https://learnku.com/docs/laravel/6.x/mocking#event-fake" target="_blank" rel="noopener noreferrer">内置测试辅助函数</a> 可以轻松做到这一点.</p></blockquote><h2 id="事件订阅者" tabindex="-1"><a class="header-anchor" href="#事件订阅者"><span>事件订阅者</span></a></h2><h3 id="编写事件订阅者" tabindex="-1"><a class="header-anchor" href="#编写事件订阅者"><span>编写事件订阅者</span></a></h3><p>事件订阅者是可以在自身内部订阅多个事件的类，即能够在单个类中定义多个事件处理器。订阅者应该定义一个 <code>subscribe</code> 方法，这个方法接收一个事件分发器实例。你可以调用给定事件分发器上的 <code>listen</code> 方法来注册事件监听器：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Listeners</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">UserEventSubscriber</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理用户登录事件</span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handleUserLogin</span><span class="token punctuation">(</span><span class="token variable">$event</span><span class="token punctuation">)</span> <span class="token punctuation">{</span><span class="token punctuation">}</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 处理用户注销事件</span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">handleUserLogout</span><span class="token punctuation">(</span><span class="token variable">$event</span><span class="token punctuation">)</span> <span class="token punctuation">{</span><span class="token punctuation">}</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 为订阅者注册监听器.</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@param</span>  <span class="token class-name"><span class="token punctuation">\\</span>Illuminate<span class="token punctuation">\\</span>Events<span class="token punctuation">\\</span>Dispatcher</span>  <span class="token parameter">$events</span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function-definition function">subscribe</span><span class="token punctuation">(</span><span class="token variable">$events</span><span class="token punctuation">)</span></span>
<span class="line">    <span class="token punctuation">{</span></span>
<span class="line">        <span class="token variable">$events</span><span class="token operator">-&gt;</span><span class="token function">listen</span><span class="token punctuation">(</span></span>
<span class="line">            <span class="token string single-quoted-string">&#39;Illuminate\\Auth\\Events\\Login&#39;</span><span class="token punctuation">,</span></span>
<span class="line">            <span class="token string single-quoted-string">&#39;App\\Listeners\\UserEventSubscriber@handleUserLogin&#39;</span></span>
<span class="line">        <span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">        <span class="token variable">$events</span><span class="token operator">-&gt;</span><span class="token function">listen</span><span class="token punctuation">(</span></span>
<span class="line">            <span class="token string single-quoted-string">&#39;Illuminate\\Auth\\Events\\Logout&#39;</span><span class="token punctuation">,</span></span>
<span class="line">            <span class="token string single-quoted-string">&#39;App\\Listeners\\UserEventSubscriber@handleUserLogout&#39;</span></span>
<span class="line">        <span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line">    <span class="token punctuation">}</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="注册事件订阅者" tabindex="-1"><a class="header-anchor" href="#注册事件订阅者"><span>注册事件订阅者</span></a></h3><p>在编写完订阅者之后，就可以通过事件分发器对订阅者进行注册。你可以在 <code>EventServiceProvider</code> 中的 <code>$subscribe</code> 属性中注册订阅者。例如，让我们将 <code>UserEventSubscriber</code> 添加到数组列表中：</p><div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre class="language-php"><code><span class="line"><span class="token php language-php"><span class="token delimiter important">&lt;?php</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">namespace</span> <span class="token package">App<span class="token punctuation">\\</span>Providers</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">use</span> <span class="token package">Illuminate<span class="token punctuation">\\</span>Foundation<span class="token punctuation">\\</span>Support<span class="token punctuation">\\</span>Providers<span class="token punctuation">\\</span>EventServiceProvider</span> <span class="token keyword">as</span> ServiceProvider<span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">class</span> <span class="token class-name-definition class-name">EventServiceProvider</span> <span class="token keyword">extends</span> <span class="token class-name">ServiceProvider</span></span>
<span class="line"><span class="token punctuation">{</span></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 应用中事件监听器的映射。</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@var</span> <span class="token class-name"><span class="token keyword">array</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">protected</span> <span class="token variable">$listen</span> <span class="token operator">=</span> <span class="token punctuation">[</span></span>
<span class="line">        <span class="token comment">//</span></span>
<span class="line">    <span class="token punctuation">]</span><span class="token punctuation">;</span></span>
<span class="line"></span>
<span class="line">    <span class="token doc-comment comment">/**</span>
<span class="line">     * 需要注册的订阅者类。</span>
<span class="line">     *</span>
<span class="line">     * <span class="token keyword">@var</span> <span class="token class-name"><span class="token keyword">array</span></span></span>
<span class="line">     */</span></span>
<span class="line">    <span class="token keyword">protected</span> <span class="token variable">$subscribe</span> <span class="token operator">=</span> <span class="token punctuation">[</span></span>
<span class="line">        <span class="token string single-quoted-string">&#39;App\\Listeners\\UserEventSubscriber&#39;</span><span class="token punctuation">,</span></span>
<span class="line">    <span class="token punctuation">]</span><span class="token punctuation">;</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></span></code></pre><div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0;"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接<br> 我们的翻译工作遵照 <a href="https://learnku.com/docs/guide/cc4.0/6589" target="_blank" rel="noopener noreferrer">CC 协议</a>，如果我们的工作有侵犯到您的权益，请及时联系我们。</p></blockquote><hr><blockquote><p>原文地址：<a href="https://learnku.com/docs/laravel/6.x/events/5162" target="_blank" rel="noopener noreferrer">https://learnku.com/docs/laravel/6.x/eve...</a></p><p>译文地址：<a href="https://learnku.com/docs/laravel/6.x/events/5162" target="_blank" rel="noopener noreferrer">https://learnku.com/docs/laravel/6.x/eve...</a></p></blockquote>`,69),i=[l];function c(t,o){return a(),s("div",null,i)}const r=n(p,[["render",c],["__file","05.事件系统.html.vue"]]),u=JSON.parse('{"path":"/laravel-6X-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/07%E7%BB%BC%E5%90%88%E8%AF%9D%E9%A2%98/05.%E4%BA%8B%E4%BB%B6%E7%B3%BB%E7%BB%9F.html","title":"","lang":"zh-CN","frontmatter":{},"headers":[{"level":2,"title":"Events","slug":"events","link":"#events","children":[]},{"level":2,"title":"事件系统介绍","slug":"事件系统介绍","link":"#事件系统介绍","children":[]},{"level":2,"title":"注册事件和监听器","slug":"注册事件和监听器","link":"#注册事件和监听器","children":[{"level":3,"title":"生成事件 & 监听器","slug":"生成事件-监听器","link":"#生成事件-监听器","children":[]},{"level":3,"title":"手动注册事件","slug":"手动注册事件","link":"#手动注册事件","children":[]},{"level":3,"title":"事件发现","slug":"事件发现","link":"#事件发现","children":[]}]},{"level":2,"title":"定义事件","slug":"定义事件","link":"#定义事件","children":[]},{"level":2,"title":"定义监听器","slug":"定义监听器","link":"#定义监听器","children":[]},{"level":2,"title":"事件监听器队列","slug":"事件监听器队列","link":"#事件监听器队列","children":[{"level":3,"title":"手动访问队列","slug":"手动访问队列","link":"#手动访问队列","children":[]},{"level":3,"title":"处理失败任务","slug":"处理失败任务","link":"#处理失败任务","children":[]}]},{"level":2,"title":"分发事件","slug":"分发事件","link":"#分发事件","children":[]},{"level":2,"title":"事件订阅者","slug":"事件订阅者","link":"#事件订阅者","children":[{"level":3,"title":"编写事件订阅者","slug":"编写事件订阅者","link":"#编写事件订阅者","children":[]},{"level":3,"title":"注册事件订阅者","slug":"注册事件订阅者","link":"#注册事件订阅者","children":[]}]}],"git":{"updatedTime":1719473835000,"contributors":[{"name":"Hans","email":"hans01@foxmail.com","commits":1}]},"filePathRelative":"laravel-6X-中文文档/07综合话题/05.事件系统.md"}');export{r as comp,u as data};
