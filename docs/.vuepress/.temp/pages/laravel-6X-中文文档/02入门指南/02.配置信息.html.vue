<template><div><p>本文档最新版为 <a href="https://learnku.com/docs/laravel/10.x" target="_blank" rel="noopener noreferrer">10.x</a>，旧版本可能放弃维护，推荐阅读最新版！</p>
<h2 id="configuration" tabindex="-1"><a class="header-anchor" href="#configuration"><span>Configuration</span></a></h2>
<ul>
<li><a href="#introduction">介绍</a></li>
<li><a href="#environment-configuration">环境配置</a>
<ul>
<li><a href="#environment-variable-types">环境变量类型</a></li>
<li><a href="#retrieving-environment-configuration">检索环境配置</a></li>
<li><a href="#determining-the-current-environment">确定当前环境</a></li>
<li><a href="#hiding-environment-variables-from-debug">在调试页面隐藏环境变量</a></li>
</ul>
</li>
<li><a href="#accessing-configuration-values">访问配置值</a></li>
<li><a href="#configuration-caching">配置缓存</a></li>
<li><a href="#maintenance-mode">维护模式</a></li>
</ul>
<h2 id="介绍" tabindex="-1"><a class="header-anchor" href="#介绍"><span>介绍</span></a></h2>
<p>Laravel 框架的所有配置文件都保存在 <code v-pre>config</code> 目录中。每个选项都有说明，你可随时查看这些文件并熟悉都有哪些配置选项可供你使用。</p>
<h2 id="环境配置" tabindex="-1"><a class="header-anchor" href="#环境配置"><span>环境配置</span></a></h2>
<p>对于应用程序运行的环境来说，不同的环境有不同的配置通常是很有用的。 例如，你可能希望在本地使用的缓存驱动不同于生产服务器所使用的缓存驱动。</p>
<p>Laravel 利用 Vance Lucas 的 PHP 库 <a href="https://github.com/vlucas/phpdotenv" target="_blank" rel="noopener noreferrer">DotEnv</a> 使得此项功能的实现变得非常简单。在新安装好的 Laravel 应用程序中，其根目录会包含一个 <code v-pre>.env.example</code> 文件。如果是通过 Composer 安装的 Laravel，该文件会自动更名为 <code v-pre>.env</code>。否则，需要你手动更改一下文件名。</p>
<p>你的 <code v-pre>.env</code> 文件不应该提交到应用程序的源代码控制系统中，因为每个使用你的应用程序的开发人员 / 服务器可能需要有一个不同的环境配置。此外，在入侵者获得你的源代码控制仓库的访问权的情况下，这会成为一个安全隐患，因为任何敏感的凭据都被暴露了。</p>
<p>如果是团队开发，则可能希望应用程序中仍包含 <code v-pre>.env.example</code> 文件。因为通过在示例配置文件中放置占位值，团队中的其他开发人员可以清楚地看到哪些环境变量是运行应用程序所必需的。你也可以创建一个 <code v-pre>.env.testing</code> 文件，当运行 PHPUnit 测试或以 <code v-pre>--env=testing</code> 为选项执行 Artisan 命令时，该文件将覆盖 <code v-pre>.env</code> 文件中的值。</p>
<blockquote>
<p>Tip：<code v-pre>.env</code> 文件中的所有变量都可被外部环境变量（比如服务器级或系统级环境变量）所覆盖。</p>
</blockquote>
<h3 id="环境变量类型" tabindex="-1"><a class="header-anchor" href="#环境变量类型"><span>环境变量类型</span></a></h3>
<p><code v-pre>.env</code> 文件中的所有变量都被解析为字符串，因此创建了一些保留值以允许你从 <code v-pre>env()</code> 函数中返回更多类型的变量：</p>
<table>
<thead>
<tr>
<th><code v-pre>.env</code> 值</th>
<th><code v-pre>env()</code> 值</th>
</tr>
</thead>
<tbody>
<tr>
<td>true</td>
<td>(bool) true</td>
</tr>
<tr>
<td>(true)</td>
<td>(bool) true</td>
</tr>
<tr>
<td>false</td>
<td>(bool) false</td>
</tr>
<tr>
<td>(false)</td>
<td>(bool) false</td>
</tr>
<tr>
<td>empty</td>
<td>(string) ‘’</td>
</tr>
<tr>
<td>(empty)</td>
<td>(string) ‘’</td>
</tr>
<tr>
<td>null</td>
<td>(null) null</td>
</tr>
<tr>
<td>(null)</td>
<td>(null) null</td>
</tr>
</tbody>
</table>
<p>如果你需要使用包含空格的值定义环境变量，可以通过将值括在双引号中来实现。</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token constant">APP_NAME</span><span class="token operator">=</span><span class="token string double-quoted-string">"我的 应用"</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><h3 id="检索环境配置" tabindex="-1"><a class="header-anchor" href="#检索环境配置"><span>检索环境配置</span></a></h3>
<p>当应用程序收到请求时，<code v-pre>.env</code> 文件中列出的所有变量将被加载到 PHP 的超级全局变量 <code v-pre>$_ENV</code> 中。你可以使用 <code v-pre>env</code> 函数检索这些变量的值。事实上，如果你查看 Laravel 的配置文件，你就能注意到有数个选项已经使用了这个函数：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token string single-quoted-string">'debug'</span> <span class="token operator">=></span> <span class="token function">env</span><span class="token punctuation">(</span><span class="token string single-quoted-string">'APP_DEBUG'</span><span class="token punctuation">,</span> <span class="token constant boolean">false</span><span class="token punctuation">)</span><span class="token punctuation">,</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><p>传递给 <code v-pre>env</code> 函数的第二个值是「默认值」。如果给定的键不存在环境变量，则会使用该值。</p>
<h3 id="确定当前环境" tabindex="-1"><a class="header-anchor" href="#确定当前环境"><span>确定当前环境</span></a></h3>
<p>应用程序当前所处环境是通过 <code v-pre>.env</code> 文件中的 <code v-pre>APP_ENV</code> 变量确定的。你可以通过 <code v-pre>App</code> <a href="https://learnku.com/docs/laravel/6.x/facades" target="_blank" rel="noopener noreferrer">facade</a> 中的 <code v-pre>environment</code> 方法来访问此值：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token variable">$environment</span> <span class="token operator">=</span> <span class="token class-name static-context">App</span><span class="token operator">::</span><span class="token function">environment</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><p>你还可以传递参数给 <code v-pre>environment</code> 方法，以检查当前的环境配置是否与给定值匹配。 如果与给定的任意值匹配，该方法将返回 <code v-pre>true</code>：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token class-name static-context">App</span><span class="token operator">::</span><span class="token function">environment</span><span class="token punctuation">(</span><span class="token string single-quoted-string">'local'</span><span class="token punctuation">)</span><span class="token punctuation">)</span> <span class="token punctuation">{</span></span>
<span class="line">    <span class="token comment">// 当前环境是 local</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span>
<span class="line"><span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token class-name static-context">App</span><span class="token operator">::</span><span class="token function">environment</span><span class="token punctuation">(</span><span class="token punctuation">[</span><span class="token string single-quoted-string">'local'</span><span class="token punctuation">,</span> <span class="token string single-quoted-string">'staging'</span><span class="token punctuation">]</span><span class="token punctuation">)</span><span class="token punctuation">)</span> <span class="token punctuation">{</span></span>
<span class="line">    <span class="token comment">// 当前的环境是 local 或 staging...</span></span>
<span class="line"><span class="token punctuation">}</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote>
<p>Tip：应用程序当前所处环境检测可以被服务器级的 <code v-pre>APP_ENV</code> 环境变量覆盖。这在为相同的应用程序配置不同的环境时是非常有用的，这样你可以在你的服务器配置中为给定的主机设置与其匹配的给定的环境。</p>
</blockquote>
<h3 id="在调试页面隐藏环境变量" tabindex="-1"><a class="header-anchor" href="#在调试页面隐藏环境变量"><span>在调试页面隐藏环境变量</span></a></h3>
<p>当一个异常未被捕获并且 <code v-pre>APP_DEBUG</code> 环境变量为 <code v-pre>true</code> 时，调试页面会显示所有的环境变量和内容。在某些情况下你可能想隐藏某些变量。你可以通过设置 <code v-pre>config/app.php</code> 配置文件中的 <code v-pre>debug_blacklist</code> 选项来完成这个操作。</p>
<p>环境变量、服务器或者请求数据中都有一些变量是可用的。因此，你可能需要将 <code v-pre>$_ENV</code> 和 <code v-pre>$_SERVER</code> 的变量加入到黑名单中：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token keyword">return</span> <span class="token punctuation">[</span></span>
<span class="line"></span>
<span class="line">    <span class="token comment">// ...</span></span>
<span class="line"></span>
<span class="line">    <span class="token string single-quoted-string">'debug_blacklist'</span> <span class="token operator">=></span> <span class="token punctuation">[</span></span>
<span class="line">        <span class="token string single-quoted-string">'_ENV'</span> <span class="token operator">=></span> <span class="token punctuation">[</span></span>
<span class="line">            <span class="token string single-quoted-string">'APP_KEY'</span><span class="token punctuation">,</span></span>
<span class="line">            <span class="token string single-quoted-string">'DB_PASSWORD'</span><span class="token punctuation">,</span></span>
<span class="line">        <span class="token punctuation">]</span><span class="token punctuation">,</span></span>
<span class="line"></span>
<span class="line">        <span class="token string single-quoted-string">'_SERVER'</span> <span class="token operator">=></span> <span class="token punctuation">[</span></span>
<span class="line">            <span class="token string single-quoted-string">'APP_KEY'</span><span class="token punctuation">,</span></span>
<span class="line">            <span class="token string single-quoted-string">'DB_PASSWORD'</span><span class="token punctuation">,</span></span>
<span class="line">        <span class="token punctuation">]</span><span class="token punctuation">,</span></span>
<span class="line"></span>
<span class="line">        <span class="token string single-quoted-string">'_POST'</span> <span class="token operator">=></span> <span class="token punctuation">[</span></span>
<span class="line">            <span class="token string single-quoted-string">'password'</span><span class="token punctuation">,</span></span>
<span class="line">        <span class="token punctuation">]</span><span class="token punctuation">,</span></span>
<span class="line">    <span class="token punctuation">]</span><span class="token punctuation">,</span></span>
<span class="line"><span class="token punctuation">]</span><span class="token punctuation">;</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="访问配置值" tabindex="-1"><a class="header-anchor" href="#访问配置值"><span>访问配置值</span></a></h2>
<p>你可以轻松地在应用程序的任何位置使用全局 config 函数来访问配置值。配置值的访问可以使用「点」语法，这其中包含了要访问的文件和选项的名称。还可以指定默认值，如果配置选项不存在，则返回默认值：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token variable">$value</span> <span class="token operator">=</span> <span class="token function">config</span><span class="token punctuation">(</span><span class="token string single-quoted-string">'app.timezone'</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><p>要在运行时设置配置值，传递一个数组给 <code v-pre>config</code> 函数：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line"><span class="token function">config</span><span class="token punctuation">(</span><span class="token punctuation">[</span><span class="token string single-quoted-string">'app.timezone'</span> <span class="token operator">=></span> <span class="token string single-quoted-string">'America/Chicago'</span><span class="token punctuation">]</span><span class="token punctuation">)</span><span class="token punctuation">;</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><h2 id="配置缓存" tabindex="-1"><a class="header-anchor" href="#配置缓存"><span>配置缓存</span></a></h2>
<p>为了给你的应用程序提升速度，你应该使用 Artisan 命令 <code v-pre>config:cache</code> 将所有的配置文件缓存到单个文件中。这会把你的应用程序中所有的配置选项合并成一个单一的文件，然后框架会快速加载这个文件。</p>
<p>通常来说，你应该把运行 <code v-pre>php artisan config:cache</code> 命令作为生产环境部署常规工作的一部分。这个命令不应在本地开发环境下运行，因为配置选项在应用程序开发过程中是经常需要被更改的。</p>
<blockquote>
<p>注意：如果在部署过程中执行 <code v-pre>config:cache</code> 命令，那你应该确保只从配置文件内部调用 env 函数。一旦配置被缓存，<code v-pre>.env</code> 文件将不再被加载，所有对 env 函数的调用都将返回 <code v-pre>null</code></p>
</blockquote>
<h2 id="维护模式" tabindex="-1"><a class="header-anchor" href="#维护模式"><span>维护模式</span></a></h2>
<p>当应用程序处于维护模式时，所有对应用程序的请求都显示为一个自定义视图。这样可以在更新或执行维护时轻松地「关闭」你的应用程序。 维护模式检查包含在应用程序的默认中间件栈中。如果应用程序处于维护模式，则将抛出一个状态码为 503 的 <code v-pre>MaintenanceModeException</code> 异常。</p>
<p>要启用维护模式，只需执行下面的 Artisan 的 <code v-pre>down</code> 命令：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line">php artisan down</span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><p>你还可以向 <code v-pre>down</code> 命令提供 <code v-pre>message</code> 和 <code v-pre>retry</code> 选项。其中 <code v-pre>message</code> 选项的值可用于显示或记录自定义消息，而 <code v-pre>retry</code> 值可用于设置 <code v-pre>HTTP</code> 响应头中 <code v-pre>Retry-After</code> 的值：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line">php artisan down <span class="token operator">--</span>message<span class="token operator">=</span><span class="token string double-quoted-string">"Upgrading Database"</span> <span class="token operator">--</span>retry<span class="token operator">=</span><span class="token number">60</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><p>即使在维护模式下，也可以使用命令 <code v-pre>allow</code> 选项允许特定的 IP 地址或网络访问应用程序：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line">php artisan down <span class="token operator">--</span>allow<span class="token operator">=</span><span class="token number">127.0</span><span class="token number">.0</span><span class="token number">.1</span> <span class="token operator">--</span>allow<span class="token operator">=</span><span class="token number">192.168</span><span class="token number">.0</span><span class="token number">.0</span><span class="token operator">/</span><span class="token number">16</span></span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><p>要关闭维护模式，请使用 <code v-pre>up</code> 命令：</p>
<div class="language-php line-numbers-mode" data-highlighter="prismjs" data-ext="php" data-title="php"><pre v-pre class="language-php"><code><span class="line">php artisan up</span>
<span class="line"></span></code></pre>
<div class="line-numbers" aria-hidden="true" style="counter-reset:line-number 0"><div class="line-number"></div></div></div><blockquote>
<p>Tip：你可以通过修改 <code v-pre>resources/views/errors/503.blade.php</code> 模板文件来自定义默认维护模式模板。</p>
</blockquote>
<h4 id="维护模式-队列" tabindex="-1"><a class="header-anchor" href="#维护模式-队列"><span>维护模式 &amp; 队列</span></a></h4>
<p>当应用程序处于维护模式时，不会处理 <a href="https://learnku.com/docs/laravel/6.x/queues" target="_blank" rel="noopener noreferrer">队列任务</a>。而这些任务会在应用程序退出维护模式后再继续处理。</p>
<h4 id="维护模式的替代方案" tabindex="-1"><a class="header-anchor" href="#维护模式的替代方案"><span>维护模式的替代方案</span></a></h4>
<p>维护模式会导致应用程序有数秒的停机（不响应）时间，因此你可以考虑使用像 <a href="https://envoyer.io/" target="_blank" rel="noopener noreferrer">Envoyer</a> 这样的替代方案，以便与 Laravel 完成零停机时间部署。</p>
<blockquote>
<p>本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接<br>
我们的翻译工作遵照 <a href="https://learnku.com/docs/guide/cc4.0/6589" target="_blank" rel="noopener noreferrer">CC 协议</a>，如果我们的工作有侵犯到您的权益，请及时联系我们。</p>
</blockquote>
<hr>
<blockquote>
<p>原文地址：<a href="https://learnku.com/docs/laravel/6.x/configuration/5125" target="_blank" rel="noopener noreferrer">https://learnku.com/docs/laravel/6.x/con...</a></p>
<p>译文地址：<a href="https://learnku.com/docs/laravel/6.x/configuration/5125" target="_blank" rel="noopener noreferrer">https://learnku.com/docs/laravel/6.x/con...</a></p>
</blockquote>
</div></template>


