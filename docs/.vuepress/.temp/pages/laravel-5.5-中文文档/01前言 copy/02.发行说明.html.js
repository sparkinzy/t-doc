import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-5.5-中文文档/01前言 copy/02.发行说明.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-5.5-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/01%E5%89%8D%E8%A8%80%20copy/02.%E5%8F%91%E8%A1%8C%E8%AF%B4%E6%98%8E.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Laravel 发行说明\",\"slug\":\"laravel-发行说明\",\"link\":\"#laravel-发行说明\",\"children\":[]},{\"level\":2,\"title\":\"版本控制方案\",\"slug\":\"版本控制方案\",\"link\":\"#版本控制方案\",\"children\":[]},{\"level\":2,\"title\":\"支持策略\",\"slug\":\"支持策略\",\"link\":\"#支持策略\",\"children\":[]},{\"level\":2,\"title\":\"Laravel 5.5\",\"slug\":\"laravel-5-5\",\"link\":\"#laravel-5-5\",\"children\":[{\"level\":3,\"title\":\"Laravel Horizon\",\"slug\":\"laravel-horizon\",\"link\":\"#laravel-horizon\",\"children\":[]},{\"level\":3,\"title\":\"包自动发现\",\"slug\":\"包自动发现\",\"link\":\"#包自动发现\",\"children\":[]},{\"level\":3,\"title\":\"API 资源\",\"slug\":\"api-资源\",\"link\":\"#api-资源\",\"children\":[]},{\"level\":3,\"title\":\"控制台命令自动注册\",\"slug\":\"控制台命令自动注册\",\"link\":\"#控制台命令自动注册\",\"children\":[]},{\"level\":3,\"title\":\"新前端预配置\",\"slug\":\"新前端预配置\",\"link\":\"#新前端预配置\",\"children\":[]},{\"level\":3,\"title\":\"队列任务链\",\"slug\":\"队列任务链\",\"link\":\"#队列任务链\",\"children\":[]},{\"level\":3,\"title\":\"Queued 任务速率限制\",\"slug\":\"queued-任务速率限制\",\"link\":\"#queued-任务速率限制\",\"children\":[]},{\"level\":3,\"title\":\"基于时间的任务尝试\",\"slug\":\"基于时间的任务尝试\",\"link\":\"#基于时间的任务尝试\",\"children\":[]},{\"level\":3,\"title\":\"验证规则对象\",\"slug\":\"验证规则对象\",\"link\":\"#验证规则对象\",\"children\":[]},{\"level\":3,\"title\":\"可信代理集成\",\"slug\":\"可信代理集成\",\"link\":\"#可信代理集成\",\"children\":[]},{\"level\":3,\"title\":\"按需通知\",\"slug\":\"按需通知\",\"link\":\"#按需通知\",\"children\":[]},{\"level\":3,\"title\":\"可渲染的邮件\",\"slug\":\"可渲染的邮件\",\"link\":\"#可渲染的邮件\",\"children\":[]},{\"level\":3,\"title\":\"自定义异常报告\",\"slug\":\"自定义异常报告\",\"link\":\"#自定义异常报告\",\"children\":[]},{\"level\":3,\"title\":\"请求验证\",\"slug\":\"请求验证\",\"link\":\"#请求验证\",\"children\":[]},{\"level\":3,\"title\":\"异常处理规范化\",\"slug\":\"异常处理规范化\",\"link\":\"#异常处理规范化\",\"children\":[]},{\"level\":3,\"title\":\"缓存锁\",\"slug\":\"缓存锁\",\"link\":\"#缓存锁\",\"children\":[]},{\"level\":3,\"title\":\"Blade 改进\",\"slug\":\"blade-改进\",\"link\":\"#blade-改进\",\"children\":[]},{\"level\":3,\"title\":\"新路由方法\",\"slug\":\"新路由方法\",\"link\":\"#新路由方法\",\"children\":[]},{\"level\":3,\"title\":\"「Sticky」数据库连接\",\"slug\":\"「sticky」数据库连接\",\"link\":\"#「sticky」数据库连接\",\"children\":[]}]},{\"level\":2,\"title\":\"译者署名\",\"slug\":\"译者署名\",\"link\":\"#译者署名\",\"children\":[]}],\"git\":{\"updatedTime\":null,\"contributors\":[]},\"filePathRelative\":\"laravel-5.5-中文文档/01前言 copy/02.发行说明.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
