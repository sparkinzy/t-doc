import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-5.5-中文文档/01前言 copy/01.翻译说明.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-5.5-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/01%E5%89%8D%E8%A8%80%20copy/01.%E7%BF%BB%E8%AF%91%E8%AF%B4%E6%98%8E.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Laravel 5.5 中文文档\",\"slug\":\"laravel-5-5-中文文档\",\"link\":\"#laravel-5-5-中文文档\",\"children\":[]},{\"level\":2,\"title\":\"说明\",\"slug\":\"说明\",\"link\":\"#说明\",\"children\":[{\"level\":3,\"title\":\"相关讨论\",\"slug\":\"相关讨论\",\"link\":\"#相关讨论\",\"children\":[]}]},{\"level\":2,\"title\":\"排版规范\",\"slug\":\"排版规范\",\"link\":\"#排版规范\",\"children\":[]},{\"level\":2,\"title\":\"翻译对照列表\",\"slug\":\"翻译对照列表\",\"link\":\"#翻译对照列表\",\"children\":[{\"level\":3,\"title\":\"A\",\"slug\":\"a\",\"link\":\"#a\",\"children\":[]},{\"level\":3,\"title\":\"B\",\"slug\":\"b\",\"link\":\"#b\",\"children\":[]},{\"level\":3,\"title\":\"C\",\"slug\":\"c\",\"link\":\"#c\",\"children\":[]},{\"level\":3,\"title\":\"D\",\"slug\":\"d\",\"link\":\"#d\",\"children\":[]},{\"level\":3,\"title\":\"E\",\"slug\":\"e\",\"link\":\"#e\",\"children\":[]},{\"level\":3,\"title\":\"F\",\"slug\":\"f\",\"link\":\"#f\",\"children\":[]},{\"level\":3,\"title\":\"G\",\"slug\":\"g\",\"link\":\"#g\",\"children\":[]},{\"level\":3,\"title\":\"H\",\"slug\":\"h\",\"link\":\"#h\",\"children\":[]},{\"level\":3,\"title\":\"I\",\"slug\":\"i\",\"link\":\"#i\",\"children\":[]},{\"level\":3,\"title\":\"J\",\"slug\":\"j\",\"link\":\"#j\",\"children\":[]},{\"level\":3,\"title\":\"K\",\"slug\":\"k\",\"link\":\"#k\",\"children\":[]},{\"level\":3,\"title\":\"L\",\"slug\":\"l\",\"link\":\"#l\",\"children\":[]},{\"level\":3,\"title\":\"M\",\"slug\":\"m\",\"link\":\"#m\",\"children\":[]},{\"level\":3,\"title\":\"N\",\"slug\":\"n\",\"link\":\"#n\",\"children\":[]},{\"level\":3,\"title\":\"O\",\"slug\":\"o\",\"link\":\"#o\",\"children\":[]},{\"level\":3,\"title\":\"P\",\"slug\":\"p\",\"link\":\"#p\",\"children\":[]},{\"level\":3,\"title\":\"Q\",\"slug\":\"q\",\"link\":\"#q\",\"children\":[]},{\"level\":3,\"title\":\"R\",\"slug\":\"r\",\"link\":\"#r\",\"children\":[]},{\"level\":3,\"title\":\"S\",\"slug\":\"s\",\"link\":\"#s\",\"children\":[]},{\"level\":3,\"title\":\"T\",\"slug\":\"t\",\"link\":\"#t\",\"children\":[]},{\"level\":3,\"title\":\"V\",\"slug\":\"v\",\"link\":\"#v\",\"children\":[]},{\"level\":3,\"title\":\"W\",\"slug\":\"w\",\"link\":\"#w\",\"children\":[]}]}],\"git\":{\"updatedTime\":null,\"contributors\":[]},\"filePathRelative\":\"laravel-5.5-中文文档/01前言 copy/01.翻译说明.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
