import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-5.5-中文文档/01前言 copy/04.贡献导引.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-5.5-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/01%E5%89%8D%E8%A8%80%20copy/04.%E8%B4%A1%E7%8C%AE%E5%AF%BC%E5%BC%95.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"贡献导引\",\"slug\":\"贡献导引\",\"link\":\"#贡献导引\",\"children\":[]},{\"level\":2,\"title\":\"Laravel 源代码贡献指南\",\"slug\":\"laravel-源代码贡献指南\",\"link\":\"#laravel-源代码贡献指南\",\"children\":[]},{\"level\":2,\"title\":\"错误反馈\",\"slug\":\"错误反馈\",\"link\":\"#错误反馈\",\"children\":[]},{\"level\":2,\"title\":\"核心开发讨论\",\"slug\":\"核心开发讨论\",\"link\":\"#核心开发讨论\",\"children\":[]},{\"level\":2,\"title\":\"选择分支？\",\"slug\":\"选择分支\",\"link\":\"#选择分支\",\"children\":[]},{\"level\":2,\"title\":\"安全漏洞\",\"slug\":\"安全漏洞\",\"link\":\"#安全漏洞\",\"children\":[]},{\"level\":2,\"title\":\"编码风格\",\"slug\":\"编码风格\",\"link\":\"#编码风格\",\"children\":[{\"level\":3,\"title\":\"PHPDoc\",\"slug\":\"phpdoc\",\"link\":\"#phpdoc\",\"children\":[]},{\"level\":3,\"title\":\"StyleCI\",\"slug\":\"styleci\",\"link\":\"#styleci\",\"children\":[]}]},{\"level\":2,\"title\":\"译者署名\",\"slug\":\"译者署名\",\"link\":\"#译者署名\",\"children\":[]}],\"git\":{\"updatedTime\":null,\"contributors\":[]},\"filePathRelative\":\"laravel-5.5-中文文档/01前言 copy/04.贡献导引.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
