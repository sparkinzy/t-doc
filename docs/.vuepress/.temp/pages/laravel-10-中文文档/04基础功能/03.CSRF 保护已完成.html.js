import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/04基础功能/03.CSRF 保护已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/04%E5%9F%BA%E7%A1%80%E5%8A%9F%E8%83%BD/03.CSRF%20%E4%BF%9D%E6%8A%A4%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"CSRF 保护\",\"slug\":\"csrf-保护\",\"link\":\"#csrf-保护\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"阻止 CSRF 请求\",\"slug\":\"阻止-csrf-请求\",\"link\":\"#阻止-csrf-请求\",\"children\":[{\"level\":3,\"title\":\"CSRF Tokens & SPAs\",\"slug\":\"csrf-tokens-spas\",\"link\":\"#csrf-tokens-spas\",\"children\":[]},{\"level\":3,\"title\":\"从 CSRF 保护中排除 URI\",\"slug\":\"从-csrf-保护中排除-uri\",\"link\":\"#从-csrf-保护中排除-uri\",\"children\":[]}]},{\"level\":2,\"title\":\"X-CSRF-TOKEN\",\"slug\":\"x-csrf-token\",\"link\":\"#x-csrf-token\",\"children\":[]},{\"level\":2,\"title\":\"X-XSRF-TOKEN\",\"slug\":\"x-xsrf-token\",\"link\":\"#x-xsrf-token\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/04基础功能/03.CSRF 保护已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
