import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/04基础功能/10.生成 URL已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/04%E5%9F%BA%E7%A1%80%E5%8A%9F%E8%83%BD/10.%E7%94%9F%E6%88%90%20URL%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"生成 URL\",\"slug\":\"生成-url\",\"link\":\"#生成-url\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"基础\",\"slug\":\"基础\",\"link\":\"#基础\",\"children\":[{\"level\":3,\"title\":\"生成基础 URLs\",\"slug\":\"生成基础-urls\",\"link\":\"#生成基础-urls\",\"children\":[]},{\"level\":3,\"title\":\"访问当前 URL\",\"slug\":\"访问当前-url\",\"link\":\"#访问当前-url\",\"children\":[]}]},{\"level\":2,\"title\":\"命名路由的 URLs\",\"slug\":\"命名路由的-urls\",\"link\":\"#命名路由的-urls\",\"children\":[{\"level\":3,\"title\":\"签名 URLs\",\"slug\":\"签名-urls\",\"link\":\"#签名-urls\",\"children\":[]}]},{\"level\":2,\"title\":\"控制器行为的 URL\",\"slug\":\"控制器行为的-url\",\"link\":\"#控制器行为的-url\",\"children\":[]},{\"level\":2,\"title\":\"默认值\",\"slug\":\"默认值\",\"link\":\"#默认值\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/04基础功能/10.生成 URL已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
