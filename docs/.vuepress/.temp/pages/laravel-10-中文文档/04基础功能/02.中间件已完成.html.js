import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/04基础功能/02.中间件已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/04%E5%9F%BA%E7%A1%80%E5%8A%9F%E8%83%BD/02.%E4%B8%AD%E9%97%B4%E4%BB%B6%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"中间件\",\"slug\":\"中间件\",\"link\":\"#中间件\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[]},{\"level\":2,\"title\":\"定义中间件\",\"slug\":\"定义中间件\",\"link\":\"#定义中间件\",\"children\":[]},{\"level\":2,\"title\":\"注册中间件\",\"slug\":\"注册中间件\",\"link\":\"#注册中间件\",\"children\":[{\"level\":3,\"title\":\"全局中间件\",\"slug\":\"全局中间件\",\"link\":\"#全局中间件\",\"children\":[]},{\"level\":3,\"title\":\"将中间件分配给路由\",\"slug\":\"将中间件分配给路由\",\"link\":\"#将中间件分配给路由\",\"children\":[]},{\"level\":3,\"title\":\"中间件组\",\"slug\":\"中间件组\",\"link\":\"#中间件组\",\"children\":[]},{\"level\":3,\"title\":\"排序中间件\",\"slug\":\"排序中间件\",\"link\":\"#排序中间件\",\"children\":[]}]},{\"level\":2,\"title\":\"中间件参数\",\"slug\":\"中间件参数\",\"link\":\"#中间件参数\",\"children\":[]},{\"level\":2,\"title\":\"可终止的中间件\",\"slug\":\"可终止的中间件\",\"link\":\"#可终止的中间件\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/04基础功能/02.中间件已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
