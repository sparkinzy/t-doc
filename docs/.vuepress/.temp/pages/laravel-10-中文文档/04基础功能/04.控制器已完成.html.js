import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/04基础功能/04.控制器已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/04%E5%9F%BA%E7%A1%80%E5%8A%9F%E8%83%BD/04.%E6%8E%A7%E5%88%B6%E5%99%A8%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"控制器\",\"slug\":\"控制器\",\"link\":\"#控制器\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[]},{\"level\":2,\"title\":\"编写控制器\",\"slug\":\"编写控制器\",\"link\":\"#编写控制器\",\"children\":[{\"level\":3,\"title\":\"基本控制器\",\"slug\":\"基本控制器\",\"link\":\"#基本控制器\",\"children\":[]},{\"level\":3,\"title\":\"单动作控制器\",\"slug\":\"单动作控制器\",\"link\":\"#单动作控制器\",\"children\":[]}]},{\"level\":2,\"title\":\"控制器中间件\",\"slug\":\"控制器中间件\",\"link\":\"#控制器中间件\",\"children\":[]},{\"level\":2,\"title\":\"资源型控制器\",\"slug\":\"资源型控制器\",\"link\":\"#资源型控制器\",\"children\":[{\"level\":3,\"title\":\"部分资源路由\",\"slug\":\"部分资源路由\",\"link\":\"#部分资源路由\",\"children\":[]},{\"level\":3,\"title\":\"嵌套资源\",\"slug\":\"嵌套资源\",\"link\":\"#嵌套资源\",\"children\":[]},{\"level\":3,\"title\":\"命名资源路由\",\"slug\":\"命名资源路由\",\"link\":\"#命名资源路由\",\"children\":[]},{\"level\":3,\"title\":\"命名资源路由参数\",\"slug\":\"命名资源路由参数\",\"link\":\"#命名资源路由参数\",\"children\":[]},{\"level\":3,\"title\":\"限定范围的资源路由\",\"slug\":\"限定范围的资源路由\",\"link\":\"#限定范围的资源路由\",\"children\":[]},{\"level\":3,\"title\":\"本地化资源 URIs\",\"slug\":\"本地化资源-uris\",\"link\":\"#本地化资源-uris\",\"children\":[]},{\"level\":3,\"title\":\"补充资源控制器\",\"slug\":\"补充资源控制器\",\"link\":\"#补充资源控制器\",\"children\":[]},{\"level\":3,\"title\":\"单例资源控制器\",\"slug\":\"单例资源控制器\",\"link\":\"#单例资源控制器\",\"children\":[]}]},{\"level\":2,\"title\":\"依赖注入和控制器\",\"slug\":\"依赖注入和控制器\",\"link\":\"#依赖注入和控制器\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/04基础功能/04.控制器已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
