import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/04基础功能/14.日志已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/04%E5%9F%BA%E7%A1%80%E5%8A%9F%E8%83%BD/14.%E6%97%A5%E5%BF%97%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":3,\"title\":\"记录弃用警告\",\"slug\":\"记录弃用警告\",\"link\":\"#记录弃用警告\",\"children\":[]},{\"level\":2,\"title\":\"构建日志堆栈\",\"slug\":\"构建日志堆栈\",\"link\":\"#构建日志堆栈\",\"children\":[]},{\"level\":2,\"title\":\"写入日志消息\",\"slug\":\"写入日志消息\",\"link\":\"#写入日志消息\",\"children\":[{\"level\":3,\"title\":\"上下文信息\",\"slug\":\"上下文信息\",\"link\":\"#上下文信息\",\"children\":[]},{\"level\":3,\"title\":\"写入特定频道\",\"slug\":\"写入特定频道\",\"link\":\"#写入特定频道\",\"children\":[]}]},{\"level\":2,\"title\":\"Monolog 通道定制\",\"slug\":\"monolog-通道定制\",\"link\":\"#monolog-通道定制\",\"children\":[{\"level\":3,\"title\":\"为通道定制 Monolog\",\"slug\":\"为通道定制-monolog\",\"link\":\"#为通道定制-monolog\",\"children\":[]},{\"level\":3,\"title\":\"创建 Monolog 处理程序通道\",\"slug\":\"创建-monolog-处理程序通道\",\"link\":\"#创建-monolog-处理程序通道\",\"children\":[]},{\"level\":3,\"title\":\"通过工厂创建通道\",\"slug\":\"通过工厂创建通道\",\"link\":\"#通过工厂创建通道\",\"children\":[]}]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/04基础功能/14.日志已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
