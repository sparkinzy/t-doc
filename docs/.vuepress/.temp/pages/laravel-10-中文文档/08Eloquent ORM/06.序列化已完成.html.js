import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/08Eloquent ORM/06.序列化已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/08Eloquent%20ORM/06.%E5%BA%8F%E5%88%97%E5%8C%96%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Eloquent: 序列化\",\"slug\":\"eloquent-序列化\",\"link\":\"#eloquent-序列化\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"序列化模型 & 集合\",\"slug\":\"序列化模型-集合\",\"link\":\"#序列化模型-集合\",\"children\":[{\"level\":3,\"title\":\"序列化为数组\",\"slug\":\"序列化为数组\",\"link\":\"#序列化为数组\",\"children\":[]},{\"level\":3,\"title\":\"序列化为 JSON\",\"slug\":\"序列化为-json\",\"link\":\"#序列化为-json\",\"children\":[]}]},{\"level\":2,\"title\":\"隐藏 JSON 属性\",\"slug\":\"隐藏-json-属性\",\"link\":\"#隐藏-json-属性\",\"children\":[]},{\"level\":2,\"title\":\"追加 JSON 值\",\"slug\":\"追加-json-值\",\"link\":\"#追加-json-值\",\"children\":[]},{\"level\":2,\"title\":\"日期序列化\",\"slug\":\"日期序列化\",\"link\":\"#日期序列化\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/08Eloquent ORM/06.序列化已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
