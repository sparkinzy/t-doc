import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/08Eloquent ORM/03.Eloquent 集合已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/08Eloquent%20ORM/03.Eloquent%20%E9%9B%86%E5%90%88%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Eloquent：集合\",\"slug\":\"eloquent-集合\",\"link\":\"#eloquent-集合\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[]},{\"level\":2,\"title\":\"可用的方法\",\"slug\":\"可用的方法\",\"link\":\"#可用的方法\",\"children\":[]},{\"level\":2,\"title\":\"自定义集合\",\"slug\":\"自定义集合\",\"link\":\"#自定义集合\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/08Eloquent ORM/03.Eloquent 集合已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
