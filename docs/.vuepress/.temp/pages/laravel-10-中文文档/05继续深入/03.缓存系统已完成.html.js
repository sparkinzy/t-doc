import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/05继续深入/03.缓存系统已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/05%E7%BB%A7%E7%BB%AD%E6%B7%B1%E5%85%A5/03.%E7%BC%93%E5%AD%98%E7%B3%BB%E7%BB%9F%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"缓存系统\",\"slug\":\"缓存系统\",\"link\":\"#缓存系统\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"配置\",\"slug\":\"配置\",\"link\":\"#配置\",\"children\":[{\"level\":3,\"title\":\"驱动先决条件\",\"slug\":\"驱动先决条件\",\"link\":\"#驱动先决条件\",\"children\":[]}]},{\"level\":2,\"title\":\"缓存用法\",\"slug\":\"缓存用法\",\"link\":\"#缓存用法\",\"children\":[{\"level\":3,\"title\":\"获取缓存实例\",\"slug\":\"获取缓存实例\",\"link\":\"#获取缓存实例\",\"children\":[]},{\"level\":3,\"title\":\"从缓存中检索项目\",\"slug\":\"从缓存中检索项目\",\"link\":\"#从缓存中检索项目\",\"children\":[]},{\"level\":3,\"title\":\"在缓存中存储项目\",\"slug\":\"在缓存中存储项目\",\"link\":\"#在缓存中存储项目\",\"children\":[]},{\"level\":3,\"title\":\"从缓存中删除项目\",\"slug\":\"从缓存中删除项目\",\"link\":\"#从缓存中删除项目\",\"children\":[]},{\"level\":3,\"title\":\"缓存助手函数\",\"slug\":\"缓存助手函数\",\"link\":\"#缓存助手函数\",\"children\":[]}]},{\"level\":2,\"title\":\"缓存标签\",\"slug\":\"缓存标签\",\"link\":\"#缓存标签\",\"children\":[{\"level\":3,\"title\":\"存储缓存标签\",\"slug\":\"存储缓存标签\",\"link\":\"#存储缓存标签\",\"children\":[]},{\"level\":3,\"title\":\"访问缓存标签\",\"slug\":\"访问缓存标签\",\"link\":\"#访问缓存标签\",\"children\":[]},{\"level\":3,\"title\":\"删除被标记的缓存数据\",\"slug\":\"删除被标记的缓存数据\",\"link\":\"#删除被标记的缓存数据\",\"children\":[]},{\"level\":3,\"title\":\"清理过期的缓存标记\",\"slug\":\"清理过期的缓存标记\",\"link\":\"#清理过期的缓存标记\",\"children\":[]}]},{\"level\":2,\"title\":\"原子锁\",\"slug\":\"原子锁\",\"link\":\"#原子锁\",\"children\":[{\"level\":3,\"title\":\"驱动程序先决条件\",\"slug\":\"驱动程序先决条件\",\"link\":\"#驱动程序先决条件\",\"children\":[]},{\"level\":3,\"title\":\"管理锁\",\"slug\":\"管理锁\",\"link\":\"#管理锁\",\"children\":[]},{\"level\":3,\"title\":\"跨进程管理锁\",\"slug\":\"跨进程管理锁\",\"link\":\"#跨进程管理锁\",\"children\":[]}]},{\"level\":2,\"title\":\"添加自定义缓存驱动\",\"slug\":\"添加自定义缓存驱动\",\"link\":\"#添加自定义缓存驱动\",\"children\":[{\"level\":3,\"title\":\"编写驱动\",\"slug\":\"编写驱动\",\"link\":\"#编写驱动\",\"children\":[]},{\"level\":3,\"title\":\"注册驱动\",\"slug\":\"注册驱动\",\"link\":\"#注册驱动\",\"children\":[]}]},{\"level\":2,\"title\":\"事件\",\"slug\":\"事件\",\"link\":\"#事件\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/05继续深入/03.缓存系统已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
