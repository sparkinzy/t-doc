import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/05继续深入/13.扩展包开发已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/05%E7%BB%A7%E7%BB%AD%E6%B7%B1%E5%85%A5/13.%E6%89%A9%E5%B1%95%E5%8C%85%E5%BC%80%E5%8F%91%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"包开发\",\"slug\":\"包开发\",\"link\":\"#包开发\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[{\"level\":3,\"title\":\"关于 Facades\",\"slug\":\"关于-facades\",\"link\":\"#关于-facades\",\"children\":[]}]},{\"level\":2,\"title\":\"包发现\",\"slug\":\"包发现\",\"link\":\"#包发现\",\"children\":[{\"level\":3,\"title\":\"退出包发现\",\"slug\":\"退出包发现\",\"link\":\"#退出包发现\",\"children\":[]}]},{\"level\":2,\"title\":\"服务提供者\",\"slug\":\"服务提供者\",\"link\":\"#服务提供者\",\"children\":[]},{\"level\":2,\"title\":\"资源\",\"slug\":\"资源\",\"link\":\"#资源\",\"children\":[{\"level\":3,\"title\":\"配置\",\"slug\":\"配置\",\"link\":\"#配置\",\"children\":[]},{\"level\":3,\"title\":\"路由\",\"slug\":\"路由\",\"link\":\"#路由\",\"children\":[]},{\"level\":3,\"title\":\"迁移\",\"slug\":\"迁移\",\"link\":\"#迁移\",\"children\":[]},{\"level\":3,\"title\":\"语言文件\",\"slug\":\"语言文件\",\"link\":\"#语言文件\",\"children\":[]},{\"level\":3,\"title\":\"视图\",\"slug\":\"视图\",\"link\":\"#视图\",\"children\":[]},{\"level\":3,\"title\":\"视图组件\",\"slug\":\"视图组件\",\"link\":\"#视图组件\",\"children\":[]},{\"level\":3,\"title\":\"“About” Artisan 命令\",\"slug\":\"about-artisan-命令\",\"link\":\"#about-artisan-命令\",\"children\":[]}]},{\"level\":2,\"title\":\"命令\",\"slug\":\"命令\",\"link\":\"#命令\",\"children\":[]},{\"level\":2,\"title\":\"公共资源\",\"slug\":\"公共资源\",\"link\":\"#公共资源\",\"children\":[]},{\"level\":2,\"title\":\"发布文件组\",\"slug\":\"发布文件组\",\"link\":\"#发布文件组\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/05继续深入/13.扩展包开发已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
