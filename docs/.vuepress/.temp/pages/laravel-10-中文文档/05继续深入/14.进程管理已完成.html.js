import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/05继续深入/14.进程管理已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/05%E7%BB%A7%E7%BB%AD%E6%B7%B1%E5%85%A5/14.%E8%BF%9B%E7%A8%8B%E7%AE%A1%E7%90%86%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"进程管理\",\"slug\":\"进程管理\",\"link\":\"#进程管理\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[]},{\"level\":2,\"title\":\"调用过程\",\"slug\":\"调用过程\",\"link\":\"#调用过程\",\"children\":[{\"level\":3,\"title\":\"进程选项\",\"slug\":\"进程选项\",\"link\":\"#进程选项\",\"children\":[]},{\"level\":3,\"title\":\"进程输出\",\"slug\":\"进程输出\",\"link\":\"#进程输出\",\"children\":[]}]},{\"level\":2,\"title\":\"异步进程\",\"slug\":\"异步进程\",\"link\":\"#异步进程\",\"children\":[{\"level\":3,\"title\":\"进程 ID 和信号\",\"slug\":\"进程-id-和信号\",\"link\":\"#进程-id-和信号\",\"children\":[]},{\"level\":3,\"title\":\"异步进程输出\",\"slug\":\"异步进程输出\",\"link\":\"#异步进程输出\",\"children\":[]}]},{\"level\":2,\"title\":\"并行处理\",\"slug\":\"并行处理\",\"link\":\"#并行处理\",\"children\":[{\"level\":3,\"title\":\"命名进程池中的进程\",\"slug\":\"命名进程池中的进程\",\"link\":\"#命名进程池中的进程\",\"children\":[]},{\"level\":3,\"title\":\"进程池进程 ID 和信号\",\"slug\":\"进程池进程-id-和信号\",\"link\":\"#进程池进程-id-和信号\",\"children\":[]}]},{\"level\":2,\"title\":\"测试\",\"slug\":\"测试\",\"link\":\"#测试\",\"children\":[{\"level\":3,\"title\":\"伪造进程\",\"slug\":\"伪造进程\",\"link\":\"#伪造进程\",\"children\":[]},{\"level\":3,\"title\":\"伪造指定进程\",\"slug\":\"伪造指定进程\",\"link\":\"#伪造指定进程\",\"children\":[]},{\"level\":3,\"title\":\"伪造进程序列\",\"slug\":\"伪造进程序列\",\"link\":\"#伪造进程序列\",\"children\":[]},{\"level\":3,\"title\":\"伪造异步进程的生命周期\",\"slug\":\"伪造异步进程的生命周期\",\"link\":\"#伪造异步进程的生命周期\",\"children\":[]},{\"level\":3,\"title\":\"可用的断言\",\"slug\":\"可用的断言\",\"link\":\"#可用的断言\",\"children\":[]},{\"level\":3,\"title\":\"防止运行未被伪造的进程\",\"slug\":\"防止运行未被伪造的进程\",\"link\":\"#防止运行未被伪造的进程\",\"children\":[]}]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/05继续深入/14.进程管理已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
