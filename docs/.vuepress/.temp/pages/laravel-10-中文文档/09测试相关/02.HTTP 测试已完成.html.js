import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/09测试相关/02.HTTP 测试已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/09%E6%B5%8B%E8%AF%95%E7%9B%B8%E5%85%B3/02.HTTP%20%E6%B5%8B%E8%AF%95%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"HTTP 测试\",\"slug\":\"http-测试\",\"link\":\"#http-测试\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"创建请求\",\"slug\":\"创建请求\",\"link\":\"#创建请求\",\"children\":[{\"level\":3,\"title\":\"自定义请求头\",\"slug\":\"自定义请求头\",\"link\":\"#自定义请求头\",\"children\":[]},{\"level\":3,\"title\":\"Cookies\",\"slug\":\"cookies\",\"link\":\"#cookies\",\"children\":[]},{\"level\":3,\"title\":\"会话 (Session) / 认证 (Authentication)\",\"slug\":\"会话-session-认证-authentication\",\"link\":\"#会话-session-认证-authentication\",\"children\":[]},{\"level\":3,\"title\":\"调试响应\",\"slug\":\"调试响应\",\"link\":\"#调试响应\",\"children\":[]},{\"level\":3,\"title\":\"异常处理\",\"slug\":\"异常处理\",\"link\":\"#异常处理\",\"children\":[]}]},{\"level\":2,\"title\":\"测试 JSON APIs\",\"slug\":\"测试-json-apis\",\"link\":\"#测试-json-apis\",\"children\":[{\"level\":3,\"title\":\"JSON 流式测试\",\"slug\":\"json-流式测试\",\"link\":\"#json-流式测试\",\"children\":[]}]},{\"level\":2,\"title\":\"测试文件上传\",\"slug\":\"测试文件上传\",\"link\":\"#测试文件上传\",\"children\":[]},{\"level\":2,\"title\":\"测试视图\",\"slug\":\"测试视图\",\"link\":\"#测试视图\",\"children\":[{\"level\":3,\"title\":\"渲染模板 & 组件\",\"slug\":\"渲染模板-组件\",\"link\":\"#渲染模板-组件\",\"children\":[]}]},{\"level\":2,\"title\":\"可用断言\",\"slug\":\"可用断言\",\"link\":\"#可用断言\",\"children\":[{\"level\":3,\"title\":\"响应断言\",\"slug\":\"响应断言\",\"link\":\"#响应断言\",\"children\":[]},{\"level\":3,\"title\":\"身份验证断言\",\"slug\":\"身份验证断言\",\"link\":\"#身份验证断言\",\"children\":[]}]},{\"level\":2,\"title\":\"验证断言\",\"slug\":\"验证断言\",\"link\":\"#验证断言\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/09测试相关/02.HTTP 测试已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
