import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/09测试相关/01.快速入门已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/09%E6%B5%8B%E8%AF%95%E7%9B%B8%E5%85%B3/01.%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"测试：入门\",\"slug\":\"测试-入门\",\"link\":\"#测试-入门\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[]},{\"level\":2,\"title\":\"环境\",\"slug\":\"环境\",\"link\":\"#环境\",\"children\":[]},{\"level\":2,\"title\":\"创建测试\",\"slug\":\"创建测试\",\"link\":\"#创建测试\",\"children\":[{\"level\":3,\"title\":\"并行测试和数据库\",\"slug\":\"并行测试和数据库\",\"link\":\"#并行测试和数据库\",\"children\":[]},{\"level\":3,\"title\":\"并行测试钩子\",\"slug\":\"并行测试钩子\",\"link\":\"#并行测试钩子\",\"children\":[]},{\"level\":3,\"title\":\"访问并行测试令牌\",\"slug\":\"访问并行测试令牌\",\"link\":\"#访问并行测试令牌\",\"children\":[]},{\"level\":3,\"title\":\"报告测试覆盖率\",\"slug\":\"报告测试覆盖率\",\"link\":\"#报告测试覆盖率\",\"children\":[]},{\"level\":3,\"title\":\"最小覆盖率阈值限制\",\"slug\":\"最小覆盖率阈值限制\",\"link\":\"#最小覆盖率阈值限制\",\"children\":[]},{\"level\":3,\"title\":\"测试性能分析\",\"slug\":\"测试性能分析\",\"link\":\"#测试性能分析\",\"children\":[]}]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/09测试相关/01.快速入门已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
