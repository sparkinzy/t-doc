import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/07数据库/01.快速入门已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/07%E6%95%B0%E6%8D%AE%E5%BA%93/01.%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"数据库: 快速入门\",\"slug\":\"数据库-快速入门\",\"link\":\"#数据库-快速入门\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[{\"level\":3,\"title\":\"配置\",\"slug\":\"配置\",\"link\":\"#配置\",\"children\":[]},{\"level\":3,\"title\":\"读写分离\",\"slug\":\"读写分离\",\"link\":\"#读写分离\",\"children\":[]}]},{\"level\":2,\"title\":\"执行原生SQL查询\",\"slug\":\"执行原生sql查询\",\"link\":\"#执行原生sql查询\",\"children\":[{\"level\":3,\"title\":\"使用多数据库连接\",\"slug\":\"使用多数据库连接\",\"link\":\"#使用多数据库连接\",\"children\":[]},{\"level\":3,\"title\":\"监听查询事件\",\"slug\":\"监听查询事件\",\"link\":\"#监听查询事件\",\"children\":[]},{\"level\":3,\"title\":\"监控累积查询时间\",\"slug\":\"监控累积查询时间\",\"link\":\"#监控累积查询时间\",\"children\":[]}]},{\"level\":2,\"title\":\"数据库事务\",\"slug\":\"数据库事务\",\"link\":\"#数据库事务\",\"children\":[]},{\"level\":2,\"title\":\"连接到数据库 CLI\",\"slug\":\"连接到数据库-cli\",\"link\":\"#连接到数据库-cli\",\"children\":[]},{\"level\":2,\"title\":\"检查你的数据库\",\"slug\":\"检查你的数据库\",\"link\":\"#检查你的数据库\",\"children\":[]},{\"level\":2,\"title\":\"监视数据库\",\"slug\":\"监视数据库\",\"link\":\"#监视数据库\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/07数据库/01.快速入门已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
