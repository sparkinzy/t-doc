import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/03核心架构/03.服务提供者已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/03%E6%A0%B8%E5%BF%83%E6%9E%B6%E6%9E%84/03.%E6%9C%8D%E5%8A%A1%E6%8F%90%E4%BE%9B%E8%80%85%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"服务提供者\",\"slug\":\"服务提供者\",\"link\":\"#服务提供者\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"编写服务提供者\",\"slug\":\"编写服务提供者\",\"link\":\"#编写服务提供者\",\"children\":[{\"level\":3,\"title\":\"注册方法\",\"slug\":\"注册方法\",\"link\":\"#注册方法\",\"children\":[]},{\"level\":3,\"title\":\"引导方法\",\"slug\":\"引导方法\",\"link\":\"#引导方法\",\"children\":[]}]},{\"level\":2,\"title\":\"注册服务提供者\",\"slug\":\"注册服务提供者\",\"link\":\"#注册服务提供者\",\"children\":[]},{\"level\":2,\"title\":\"延迟加载提供者\",\"slug\":\"延迟加载提供者\",\"link\":\"#延迟加载提供者\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/03核心架构/03.服务提供者已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
