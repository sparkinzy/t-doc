import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/06安全相关/06.重置密码已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/06%E5%AE%89%E5%85%A8%E7%9B%B8%E5%85%B3/06.%E9%87%8D%E7%BD%AE%E5%AF%86%E7%A0%81%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"重置密码\",\"slug\":\"重置密码\",\"link\":\"#重置密码\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[{\"level\":3,\"title\":\"模型准备\",\"slug\":\"模型准备\",\"link\":\"#模型准备\",\"children\":[]},{\"level\":3,\"title\":\"数据库准备\",\"slug\":\"数据库准备\",\"link\":\"#数据库准备\",\"children\":[]},{\"level\":3,\"title\":\"配置受信任的主机\",\"slug\":\"配置受信任的主机\",\"link\":\"#配置受信任的主机\",\"children\":[]}]},{\"level\":2,\"title\":\"路由\",\"slug\":\"路由\",\"link\":\"#路由\",\"children\":[{\"level\":3,\"title\":\"请求密码重置链接\",\"slug\":\"请求密码重置链接\",\"link\":\"#请求密码重置链接\",\"children\":[]},{\"level\":3,\"title\":\"重置密码\",\"slug\":\"重置密码-1\",\"link\":\"#重置密码-1\",\"children\":[]}]},{\"level\":2,\"title\":\"删除过期令牌\",\"slug\":\"删除过期令牌\",\"link\":\"#删除过期令牌\",\"children\":[]},{\"level\":2,\"title\":\"自定义\",\"slug\":\"自定义\",\"link\":\"#自定义\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/06安全相关/06.重置密码已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
