import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/10官方扩展包/14.Sanctum API 授权已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/10%E5%AE%98%E6%96%B9%E6%89%A9%E5%B1%95%E5%8C%85/14.Sanctum%20API%20%E6%8E%88%E6%9D%83%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Laravel Sanctum\",\"slug\":\"laravel-sanctum\",\"link\":\"#laravel-sanctum\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[{\"level\":3,\"title\":\"工作原理\",\"slug\":\"工作原理\",\"link\":\"#工作原理\",\"children\":[]}]},{\"level\":2,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[]},{\"level\":2,\"title\":\"配置\",\"slug\":\"配置\",\"link\":\"#配置\",\"children\":[{\"level\":3,\"title\":\"覆盖默认模型\",\"slug\":\"覆盖默认模型\",\"link\":\"#覆盖默认模型\",\"children\":[]}]},{\"level\":2,\"title\":\"API 令牌认证\",\"slug\":\"api-令牌认证\",\"link\":\"#api-令牌认证\",\"children\":[{\"level\":3,\"title\":\"发行 API 令牌\",\"slug\":\"发行-api-令牌\",\"link\":\"#发行-api-令牌\",\"children\":[]},{\"level\":3,\"title\":\"令牌能力\",\"slug\":\"令牌能力\",\"link\":\"#令牌能力\",\"children\":[]},{\"level\":3,\"title\":\"保护路由\",\"slug\":\"保护路由\",\"link\":\"#保护路由\",\"children\":[]},{\"level\":3,\"title\":\"撤销令牌\",\"slug\":\"撤销令牌\",\"link\":\"#撤销令牌\",\"children\":[]},{\"level\":3,\"title\":\"令牌有效期\",\"slug\":\"令牌有效期\",\"link\":\"#令牌有效期\",\"children\":[]}]},{\"level\":2,\"title\":\"SPA 身份验证\",\"slug\":\"spa-身份验证\",\"link\":\"#spa-身份验证\",\"children\":[{\"level\":3,\"title\":\"配置\",\"slug\":\"配置-1\",\"link\":\"#配置-1\",\"children\":[]},{\"level\":3,\"title\":\"身份验证\",\"slug\":\"身份验证\",\"link\":\"#身份验证\",\"children\":[]},{\"level\":3,\"title\":\"保护路由\",\"slug\":\"保护路由-1\",\"link\":\"#保护路由-1\",\"children\":[]},{\"level\":3,\"title\":\"授权私有广播频道\",\"slug\":\"授权私有广播频道\",\"link\":\"#授权私有广播频道\",\"children\":[]}]},{\"level\":2,\"title\":\"移动应用程序身份验证\",\"slug\":\"移动应用程序身份验证\",\"link\":\"#移动应用程序身份验证\",\"children\":[{\"level\":3,\"title\":\"发布 API 令牌\",\"slug\":\"发布-api-令牌\",\"link\":\"#发布-api-令牌\",\"children\":[]},{\"level\":3,\"title\":\"路由保护\",\"slug\":\"路由保护\",\"link\":\"#路由保护\",\"children\":[]},{\"level\":3,\"title\":\"撤销令牌\",\"slug\":\"撤销令牌-1\",\"link\":\"#撤销令牌-1\",\"children\":[]}]},{\"level\":2,\"title\":\"测试\",\"slug\":\"测试\",\"link\":\"#测试\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/10官方扩展包/14.Sanctum API 授权已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
