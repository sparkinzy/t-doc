import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/10官方扩展包/04.Fortify 授权生成器已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/10%E5%AE%98%E6%96%B9%E6%89%A9%E5%B1%95%E5%8C%85/04.Fortify%20%E6%8E%88%E6%9D%83%E7%94%9F%E6%88%90%E5%99%A8%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Laravel Fortify\",\"slug\":\"laravel-fortify\",\"link\":\"#laravel-fortify\",\"children\":[]},{\"level\":2,\"title\":\"介绍\",\"slug\":\"介绍\",\"link\":\"#介绍\",\"children\":[{\"level\":3,\"title\":\"Fortify 是什么？\",\"slug\":\"fortify-是什么\",\"link\":\"#fortify-是什么\",\"children\":[]},{\"level\":3,\"title\":\"何时使用 Fortify？\",\"slug\":\"何时使用-fortify\",\"link\":\"#何时使用-fortify\",\"children\":[]}]},{\"level\":2,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[{\"level\":3,\"title\":\"Fortify 服务提供商\",\"slug\":\"fortify-服务提供商\",\"link\":\"#fortify-服务提供商\",\"children\":[]},{\"level\":3,\"title\":\"Fortify 包含的功能\",\"slug\":\"fortify-包含的功能\",\"link\":\"#fortify-包含的功能\",\"children\":[]},{\"level\":3,\"title\":\"禁用视图\",\"slug\":\"禁用视图\",\"link\":\"#禁用视图\",\"children\":[]}]},{\"level\":2,\"title\":\"身份认证\",\"slug\":\"身份认证\",\"link\":\"#身份认证\",\"children\":[{\"level\":3,\"title\":\"自定义用户认证\",\"slug\":\"自定义用户认证\",\"link\":\"#自定义用户认证\",\"children\":[]},{\"level\":3,\"title\":\"自定义身份验证管道\",\"slug\":\"自定义身份验证管道\",\"link\":\"#自定义身份验证管道\",\"children\":[]},{\"level\":3,\"title\":\"自定义跳转\",\"slug\":\"自定义跳转\",\"link\":\"#自定义跳转\",\"children\":[]}]},{\"level\":2,\"title\":\"双因素认证\",\"slug\":\"双因素认证\",\"link\":\"#双因素认证\",\"children\":[{\"level\":3,\"title\":\"启用双因素身份验证\",\"slug\":\"启用双因素身份验证\",\"link\":\"#启用双因素身份验证\",\"children\":[]},{\"level\":3,\"title\":\"使用双因素身份验证进行身份验证\",\"slug\":\"使用双因素身份验证进行身份验证\",\"link\":\"#使用双因素身份验证进行身份验证\",\"children\":[]},{\"level\":3,\"title\":\"禁用两因素身份验证\",\"slug\":\"禁用两因素身份验证\",\"link\":\"#禁用两因素身份验证\",\"children\":[]}]},{\"level\":2,\"title\":\"注册\",\"slug\":\"注册\",\"link\":\"#注册\",\"children\":[{\"level\":3,\"title\":\"定制注册\",\"slug\":\"定制注册\",\"link\":\"#定制注册\",\"children\":[]}]},{\"level\":2,\"title\":\"重设密码\",\"slug\":\"重设密码\",\"link\":\"#重设密码\",\"children\":[{\"level\":3,\"title\":\"请求密码重置链接\",\"slug\":\"请求密码重置链接\",\"link\":\"#请求密码重置链接\",\"children\":[]},{\"level\":3,\"title\":\"重设密码\",\"slug\":\"重设密码-1\",\"link\":\"#重设密码-1\",\"children\":[]},{\"level\":3,\"title\":\"自定义密码重置\",\"slug\":\"自定义密码重置\",\"link\":\"#自定义密码重置\",\"children\":[]}]},{\"level\":2,\"title\":\"电子邮件验证\",\"slug\":\"电子邮件验证\",\"link\":\"#电子邮件验证\",\"children\":[{\"level\":3,\"title\":\"保护路由\",\"slug\":\"保护路由\",\"link\":\"#保护路由\",\"children\":[]}]},{\"level\":2,\"title\":\"确认密码\",\"slug\":\"确认密码\",\"link\":\"#确认密码\",\"children\":[]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/10官方扩展包/04.Fortify 授权生成器已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
