import comp from "D:/www/t-doc/docs/.vuepress/.temp/pages/laravel-10-中文文档/10官方扩展包/03.Envoy 部署工具已完成.html.vue"
const data = JSON.parse("{\"path\":\"/laravel-10-%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3/10%E5%AE%98%E6%96%B9%E6%89%A9%E5%B1%95%E5%8C%85/03.Envoy%20%E9%83%A8%E7%BD%B2%E5%B7%A5%E5%85%B7%E5%B7%B2%E5%AE%8C%E6%88%90.html\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{},\"headers\":[{\"level\":2,\"title\":\"Laravel Envoy\",\"slug\":\"laravel-envoy\",\"link\":\"#laravel-envoy\",\"children\":[]},{\"level\":2,\"title\":\"简介\",\"slug\":\"简介\",\"link\":\"#简介\",\"children\":[]},{\"level\":2,\"title\":\"安装\",\"slug\":\"安装\",\"link\":\"#安装\",\"children\":[]},{\"level\":2,\"title\":\"编写任务\",\"slug\":\"编写任务\",\"link\":\"#编写任务\",\"children\":[{\"level\":3,\"title\":\"定义任务\",\"slug\":\"定义任务\",\"link\":\"#定义任务\",\"children\":[]},{\"level\":3,\"title\":\"多服务器\",\"slug\":\"多服务器\",\"link\":\"#多服务器\",\"children\":[]},{\"level\":3,\"title\":\"配置\",\"slug\":\"配置\",\"link\":\"#配置\",\"children\":[]},{\"level\":3,\"title\":\"变量\",\"slug\":\"变量\",\"link\":\"#变量\",\"children\":[]},{\"level\":3,\"title\":\"故事\",\"slug\":\"故事\",\"link\":\"#故事\",\"children\":[]},{\"level\":3,\"title\":\"任务钩子\",\"slug\":\"任务钩子\",\"link\":\"#任务钩子\",\"children\":[]},{\"level\":3,\"title\":\"钩子\",\"slug\":\"钩子\",\"link\":\"#钩子\",\"children\":[]}]},{\"level\":2,\"title\":\"运行任务\",\"slug\":\"运行任务\",\"link\":\"#运行任务\",\"children\":[{\"level\":3,\"title\":\"确认任务执行\",\"slug\":\"确认任务执行\",\"link\":\"#确认任务执行\",\"children\":[]}]},{\"level\":2,\"title\":\"通知\",\"slug\":\"通知\",\"link\":\"#通知\",\"children\":[{\"level\":3,\"title\":\"Slack\",\"slug\":\"slack\",\"link\":\"#slack\",\"children\":[]},{\"level\":3,\"title\":\"Discord\",\"slug\":\"discord\",\"link\":\"#discord\",\"children\":[]},{\"level\":3,\"title\":\"Telegram\",\"slug\":\"telegram\",\"link\":\"#telegram\",\"children\":[]},{\"level\":3,\"title\":\"Microsoft Teams\",\"slug\":\"microsoft-teams\",\"link\":\"#microsoft-teams\",\"children\":[]}]}],\"git\":{},\"filePathRelative\":\"laravel-10-中文文档/10官方扩展包/03.Envoy 部署工具已完成.md\"}")
export { comp, data }

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
