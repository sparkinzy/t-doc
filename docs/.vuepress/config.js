import { defaultTheme } from '@vuepress/theme-default'
import { defineUserConfig } from 'vuepress/cli'
import { viteBundler } from '@vuepress/bundler-vite'
import { searchPlugin } from '@vuepress/plugin-search'
import { laravel10zh } from '../laravel-10-中文文档/sidebar'
import { laravel5_5zh } from '../laravel-5.5-中文文档/sidebar'
import { laravel6Xzh } from '../laravel-6X-中文文档/sidebar'
import { dcatAdmin2x } from '../dcat-admin-2x/sidebar'
import { laravel11zh } from '../laravel-11-中文文档/sidebar'
import { yii2 } from '../Yii2/sidebar'
import { refactoring2 } from '../refactoring2/sidebar'

export default defineUserConfig({
  lang: 'zh-CN',

  title: 'T-DOC',
  description: '私有技术文档',

  theme: defaultTheme({
    // logo: 'https://vuejs.press/images/hero.png',
    navbar: [
      {
        text: 'Laravel',
        children: [
          {
            text: 'Laravel-11',
            link: '/laravel-11-中文文档/01前言/01.发行说明.html'
          },
          {
            text: 'Laravel-10',
            link: '/laravel-10-中文文档/01前言/01.发行说明.html'
          },
          {
            text: 'Laravel-6X',
            link: '/laravel-6X-中文文档/01前言/01.发行说明.html'
          },
          {
            text: 'Laravel-5.5',
            link: '/laravel-5.5-中文文档/01前言/02.发行说明.html'
          }
        ]
      },
      {
        text: 'Dcat-Admin',
        link: '/dcat-admin-2x/01入门/01.简介.html'
      },
      {
        text: '其他',
        children: [
          {
            text: 'Yii2',
            link: '/Yii2/01前言/01.关于 Yii.html'
          },
          {
            text: '重构2',
            link: '/refactoring2/序言.html'
          }
        ]
      }
    ],
    sidebar: {
      ...laravel11zh,
      ...laravel10zh,
      ...laravel5_5zh,
      ...laravel6Xzh,
      ...dcatAdmin2x,
      ...yii2,
      ...refactoring2,
    },
  }),

  bundler: viteBundler(),

  plugins: [
    searchPlugin({
      // 配置项
    }),
  ],
})
