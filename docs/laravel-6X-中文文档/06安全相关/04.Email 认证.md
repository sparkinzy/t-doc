## Email 认证

Laravel 6 中文文档 /  

本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## Email 认证

+   [简介](#introduction)
+   [数据库注意事项](#verification-database)
+   [路由](#verification-routing)
    +   [保护路由](#protecting-routes)
+   [视图](#verification-views)
+   [Email认证之后](#after-verifying-emails)
+   [事件](#events)

## 简介

很多 Web 应用会要求用户在使用之前进行 Email 地址验证。Laravel 不会强迫你在每个应用中重复实现它，而是提供了便捷的方法来发送和校验电子邮件的验证请求。

### Model 准备

在开始之前，需要验证你的 `App\User` 模型是否实现了 `Illuminate\Contracts\Auth\MustVerifyEmail`契约：

```php
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    // ...
}
```

## 数据库注意事项

#### Email 验证字段

接下来，你的 `user` 表必须包含一个 `email_verified_at` 字段用来存储 Email 地址通过验证的时间。默认情况下，Laravel 框架中 `users` 表的数据迁移已经包含了这个字段。所以，您需要做的就只是执行数据库迁移：

```php
php artisan migrate
```

## 路由

Laravel 的 `Auth\VerificationController` 类包含了发送验证链接和验证 Email 的必要逻辑。通过将 `verify` 选项传给 `Auth::routes` 方法，就能为这个控制器注册所需要的路由：

```php
Auth::routes(['verify' => true]);
```

### 保护路由

[路由中间件](https://learnku.com/docs/laravel/6.x/middleware) 可用于仅允许经过验证的用户访问指定路由。Laravel 附带了 `verified` 中间件，它定义在 `Illuminate\Auth\Middleware\EnsureEmailIsVerified`。由于此中间件已在应用程序的 HTTP 内核中注册，因此您需要做的就是将中间件附加到路由定义：

```php
Route::get('profile', function () {
    // 只有经过验证的用户才能进..
})->middleware('verified');
```

## 视图

如果要生成Email验证的所有必要视图，您可以使用`laravel / ui` Composer包：

```php
composer require laravel/ui

php artisan ui vue --auth
```

Email验证视图文件位于`resources / views / auth / verify.blade.php`中。 你可以根据自己的应用自由地调整这些视图的样式。

## 邮箱认证之后

在邮箱认证之后，用户会自动被重定向至 `/home`。你可通过在 `VerificationController` 中定义一个 `redirectTo` 方法或者属性来调整认证之后的跳转位置。

```php
protected $redirectTo = '/dashboard';
```

## 事件

Laravel在Email验证过程中发送[事件](https://learnku.com/docs/laravel/6.x/events)。 你应该在 EventServiceProvider 中注册监听者：

```php
/**
 * 应用程序的事件监听器
 *
 * @var array
 */
protected $listen = [
    'Illuminate\Auth\Events\Verified' => [
        'App\Listeners\LogVerifiedUser',
    ],
];
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/ver...](https://learnku.com/docs/laravel/6.x/verification/5154)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/ver...](https://learnku.com/docs/laravel/6.x/verification/5154)