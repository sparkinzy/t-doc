本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 错误处理

+   [介绍](#introduction)
+   [配置](#configuration)
+   [异常处理](#the-exception-handler)
    +   [Report 方法](#report-method)
    +   [Render 方法](#render-method)
    +   [Reportable & Renderable 异常](#renderable-exceptions)
+   [HTTP 异常](#http-exceptions)
    +   [自定义 HTTP 错误页面](#custom-http-error-pages)

## 介绍

当你开始一个新的 `Laravel` 项目时, 它已经为您配置了错误和异常处理. `app\exceptions\handler` 类用于记录应用程序触发的所有异常，然后将其呈现回用户。我们将在本文中深入讨论这个类。

## 配置

你的 `config/app.php` 配置文件中的 `debug` 选项决定了对于一个错误实际上将显示多少信息给用户。默认情况下，该选项的设置将遵照存储在 `.env` 文件中的 `APP_DEBUG` 环境变量的值。

对于本地开发，你应该将 `APP_DEBUG` 环境变量的值设置为 `true` 。在生产环境中，该值应始终为 `false` 。如果在生产中将该值设置为 `true` ，则可能会将敏感配置值暴露给应用程序的终端用户。

## 异常处理

### Report 方法

所有异常都是由 `App\Exceptions\Handler` 处理。 这个类包含了两个方法：`report` 和 `render`。我们将详细的剖析这些方法。`report` 方法用于记录异常或将它们发送给如 [Bugsnag](https://bugsnag.com/) 或 [Sentry](https://github.com/getsentry/sentry-laravel) 等外部服务。默认情况下，`report` 方法将异常传递给记录异常的基类。不过，你可以用任何自己喜欢的方式来记录异常。

例如，如果你需要以不同方式报告不同类型的异常，则可以使用 `PHP` 的 `instanceof` 比较运算符：

```php
/**
 * 报告或记录异常
 *
 * 这是一个向 Sentry、Bugsnag 等外部服务发送异常信息的好位置
 *
 * @param  \Exception  $exception
 * @return void
 */
public function report(Exception $exception)
{
    if ($exception instanceof CustomException) {
        //
    }

    parent::report($exception);
}
```

> Tip：不要在 `report` 方法中进行太多的 `instanceof` 检查，而应考虑使用 [可报告异常（Reportable exception）](https://learnku.com/docs/laravel/6.x/errors#renderable-exceptions) 。

#### 全局日志

在正常情况下，`Laravel` 会自动将当前用户的 `ID` 作为数据添加到每一条异常日志中。 你可以在通过重写 `App\Exceptions\Handler` 类中的 `context` 方法来定义你的全局环境变量。 之后，这个变量将包含在每一条异常日志中：

```php
/**
 * 定义默认的环境变量
 *
 * @return array
 */
protected function context()
{
    return array_merge(parent::context(), [
        'foo' => 'bar',
    ]);
}
```

#### `report` 辅助函数

有时你可能需要报告异常，但又不希望终止当前请求的处理。 `report` 辅助函数允许你使用异常处理器的 `report` 方法在不显示错误页面的情况下快速报告异常：

```php
public function isValid($value)
{
    try {
        // 验证值...
    } catch (Exception $e) {
        report($e);

        return false;
    }
}
```

#### 按类型忽略异常

异常处理器的 `$dontReport` 属性包含一组不会被记录的异常类型。例如，由 404 错误导致的异常以及其他几种类型的错误不会写入日志文件。你可以根据需要添加其他异常类型到此数组中：

```php
/**
 * 不应被报告的异常类型清单render 方法负责将给定的异常转换为将被发送回浏览器的 HTTP 响应。默认情况下，异常将传递给为你生成响应的基类。不过，你可以按自己意愿检查异常类型或返回自己的自定义响应：render 方法负责将给定的异常转换为将被发送回浏览器的 HTTP 响应。默认情况下，异常将传递给为你生成响应的基类。不过，你可以按自己意愿检查异常类型或返回自己的自定义响应：
 *
 * @var array
 */
protected $dontReport = [
    \Illuminate\Auth\AuthenticationException::class,
    \Illuminate\Auth\Access\AuthorizationException::class,
    \Symfony\Component\HttpKernel\Exception\HttpException::class,
    \Illuminate\Database\Eloquent\ModelNotFoundException::class,
    \Illuminate\Validation\ValidationException::class,
];
```

### Render 方法

`render` 方法负责将给定的异常转换为将被发送回浏览器的 `HTTP` 响应。默认情况下，异常将传递给为你生成响应的基类。不过，你可以按自己意愿检查异常类型或返回自己的自定义响应：

```php
/**
 * 将异常转换为 HTTP 响应。
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Exception  $exception
 * @return \Illuminate\Http\Response
 */
public function render($request, Exception $exception)
{
    if ($exception instanceof CustomException) {
        return response()->view('errors.custom', [], 500);
    }

    return parent::render($request, $exception);
}
```

### Reportable & Renderable 异常

除了在异常处理器的 `report` 和 `render` 方法中检查异常类型，你还可以直接在自定义异常上定义 `report` 和 `render` 方法。当定义了这些方法时，它们会被框架自动调用：

```php
<?php

namespace App\Exceptions;

use Exception;

class RenderException extends Exception
{
    /**
     * 报告异常
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * 转换异常为 HTTP 响应
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response(...);
    }
}
```

> Tip：你可以声明 `report` 方法和必要参数，它们将通过 Laravel 的 [服务容器](https://learnku.com/docs/laravel/6.x/container) 自动注入方法中 .

## HTTP 异常

一些异常用于描述产生自服务器的 HTTP 错误代码。例如，「页面未找到」错误 (404), 「未经授权的错误」(401) ，甚至可以是开发人员引起的 500 错误。 你可以使用 `abort` 辅助函数从应用程序的任何地方生成这样的响应：

```php
abort(404);
```

辅助函数 `abort` 会立即引发一个由异常处理器渲染的异常。你还可选择性地提供响应文本：

```php
abort(403, 'Unauthorized action.');
```

### 自定义 HTTP 错误页面

`Laravel` 可以轻松显示各种 `HTTP` 状态代码的自定义错误页面。例如，如果你希望自定义 404 `HTTP` 状态码的错误页面，可以创建一个 `resources/views/errors/404.blade.php` 视图文件。该文件将被用于你的应用程序产生的所有 404 错误。此目录中的视图文件的命名应匹配它们对应的 `HTTP` 状态码。由 abort 函数引发的 `HttpException` 实例将作为 `$exception` 变量传递给视图：

```php
<h2>{{ $exception->getMessage() }}</h2>
```

你可以使用 `vendor:publish` Artisan 命令来定义错误模板页面。模板页面生成后，就可以自定义模板页面的内容：

```php
php artisan vendor:publish --tag=laravel-errors
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/err...](https://learnku.com/docs/laravel/6.x/errors/5145)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/err...](https://learnku.com/docs/laravel/6.x/errors/5145)