本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## Laravel 社会化登录

+   [简介](#introduction)
+   [升级社会化登录](#upgrading-socialite)
+   [安装](#installation)
+   [配置](#configuration)
+   [路由](#routing)
+   [可选参数](#optional-parameters)
+   [访问范围](#access-scopes)
+   [无状态认证](#stateless-authentication)
+   [检索用户详细信息](#retrieving-user-details)

## 简介

除了典型的基于表单的身份验证之外，Laravel 还提供了一种使用 [Laravel 社会化登录](https://github.com/laravel/socialite) 对 OAuth providers 进行身份验证的简单方便的方法。 Socialite 目前支持 Facebook，Twitter，LinkedIn，Google，GitHub，GitLab 和 Bitbucket 的身份验证。

> Tip：其他平台的驱动器可以在 [Socialite Providers](https://socialiteproviders.netlify.com/) 社区驱动网站查找。

## 升级社会化登录

升级到 Socialite 的新主要版本时，请务必仔细查看 [升级指南](https://github.com/laravel/socialite/blob/master/UPGRADE.md)。

## 安装

在开始使用社会化登录功能之前，通过 Composer 将 laravel/socialite 包添加到你的项目依赖里面：

```php
composer require laravel/socialite
```

## 配置

在使用 Socialite 之前，您还需要为应用程序使用的 OAuth 服务添加凭据。 这些凭证应该放在你的 `config / services.php` 配置文件中，并且应该使用密钥 `facebook`，`twitter`，`linkedin`，`google`，`github`，`gitlab` 或 `bitbucket`， 取决于您的应用程序所需的提供商。 例如：

```php
'github' => [
    'client_id' => env('GITHUB_CLIENT_ID'),
    'client_secret' => env('GITHUB_CLIENT_SECRET'),
    'redirect' => 'http://your-callback-url',
],
```

> Tip：如果 `redirect` 项的值是个相对路径，它会自动解析为全称 URL 。

## 路由

接下来，就要对用户认证了！这需要两个路由：一个路由用于把用户重定向到 OAuth provider，另一个则用于在认证完成后接收相应 provider 的回调请求。可以通过 `Socialite` facade 的方式来访问 Socialite：

```php
<?php

namespace App\Http\Controllers\Auth;

use Socialite;

class LoginController extends Controller
{
    /**
     * 将用户重定向到 GitHub 的授权页面
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * 从 GitHub 获取用户信息
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();

        // $user->token;
    }
}
```

`redirect` 方法负责将用户发送到 OAuth provider，而 `user` 方法将读取传入请求并从 provider 中检索用户信息。

当然，还需要在你的控制器方法中定义好路由规则：

```php
Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');
```

## 可选参数

许多 OAuth providers 支持重定向请求中的可选参数。 要在请求中包含任何可选参数，请使用关联数组调用 `with` 方法：

```php
return Socialite::driver('google')
    ->with(['hd' => 'example.com'])
    ->redirect();
```

> 注意：使用 `with` 方法时，注意不要传递任何保留的关键字，如 `state` 或 `response_type` 。

## 访问作用域

在重定向用户之前，您还可以使用 `scopes` 方法在请求中添加其他「作用域」。 此方法将所有现有范围与您提供的范围合并：

```php
return Socialite::driver('github')
    ->scopes(['read:user', 'public_repo'])
    ->redirect();
```

你可以使用 `setScopes` 方法覆盖所有现有范围：

```php
return Socialite::driver('github')
    ->setScopes(['read:user', 'public_repo'])
    ->redirect();
```

## 无认证状态

`stateless` 方法可用于禁用会话状态验证。 这在向 API 添加社交身份验证时非常有用：

```php
return Socialite::driver('google')->stateless()->user();
```

## 获取用户实例

有了用户实例之后，就可以获取更多用户详情：

```php
$user = Socialite::driver('github')->user();

// OAuth2 Providers
$token = $user->token;
$refreshToken = $user->refreshToken; // not always provided
$expiresIn = $user->expiresIn;

// OAuth1 Providers
$token = $user->token;
$tokenSecret = $user->tokenSecret;

// 所有 Providers
$user->getId();
$user->getNickname();
$user->getName();
$user->getEmail();
$user->getAvatar();
```

#### 从令牌中检索用户详细信息 (OAuth2)

如果你已经有了一个用户的有效访问令牌，你可以使用 `userFromToken` 方法检索用户的详细信息：

```php
$user = Socialite::driver('github')->userFromToken($token);
```

#### 从令牌和秘钥中检索用户详细信息 (OAuth1)

如果你已经有了一个有效的用户令牌 / 秘钥，你可以使用 `userFromTokenAndSecret` 方法检索他们的详细信息：

```php
$user = Socialite::driver('twitter')->userFromTokenAndSecret($token, $secret);
```

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/soc...](https://learnku.com/docs/laravel/6.x/socialite/5192)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/soc...](https://learnku.com/docs/laravel/6.x/socialite/5192)