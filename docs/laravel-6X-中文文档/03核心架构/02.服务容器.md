本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 服务容器

+   [简介](#introduction)
+   [绑定](#binding)
    +   [基本绑定](#binding-basics)
    +   [绑定接口到实现](#binding-interfaces-to-implementations)
    +   [上下文绑定](#contextual-binding)
    +   [标记](#tagging)
    +   [扩展绑定](#extending-bindings)
+   [解析](#resolving)
    +   [Make 方法](#the-make-method)
    +   [自动注入](#automatic-injection)
+   [容器事件](#container-events)
+   [PSR-11](#psr-11)

## 简介

Laravel 服务容器是一个用于管理类的依赖和执行依赖注入的强大工具。依赖注入这个花哨名词实质上是指：类的依赖通过构造函数，或者某些情况下通过 「setter」 方法 「注入」到类中。

来看一个简单的例子：

```php
<?php

namespace App\Http\Controllers;

use App\User;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * 用户存储库的实现。
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * 创建新的控制器实例。
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     *显示指定用户的 profile。
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->users->find($id);

        return view('user.profile', ['user' => $user]);
    }
}
```

在这个例子中，控制器 `UserController` 需要从数据源获取 users。因此，我们要 **注入** 一个能够获取 users 的服务。在当前上下文中，我们的 `UserRepository` 很可能是使用 [Eloquent](https://learnku.com/docs/laravel/6.x/eloquent) 从数据库中获取 user 信息。 然而，由于 repository 是被注入的，所以我们可以轻易地将其切换为另一个的实现。这种注入方式的便利之处还体现在当我们为应用编写测试时，我们还可以轻松地 “模拟” 或创建 `UserRepository` 的虚拟实现。

想要构建强大的大型应用，至关重要的一件事是：要深刻地理解 Laravel 服务容器。当然，为 Laravel 的核心代码做出贡献也一样。

## 服务绑定

### 基础绑定

几乎所有的服务容器绑定都会在 [服务提供者](https://learnku.com/docs/laravel/6.x/providers) 中注册，下面示例中的大多数将演示如何在该上下文（服务提供者）中使用容器。

> Tip：如果某个容器不依赖于任何接口就没必要去绑定类在这个容器里。容器不需要指定如何构建这些对象，因为它可以使用反射来自动解析这些对象。

#### 简单绑定

在服务提供器中，你总是可以通过 `$this->app` 属性访问容器。我们可以通过容器的 `bind` 方法注册绑定，`bind` 方法的第一个参数为要绑定的类/接口名，第二个参数是一个返回类实例的 `Closure` ：

```php
$this->app->bind('HelpSpot\API', function ($app) {
    return new HelpSpot\API($app->make('HttpClient'));
});
```

注意，我们接受容器本身作为解析器的参数。然后，我们可以使用容器来解析正在构建的对象的子依赖。

#### 绑定一个单例

`singleton` 方法将类或接口绑定到只解析一次的容器中。一旦单例绑定被解析，相同的对象实例会在随后的调用中返回到容器中：

```php
$this->app->singleton('HelpSpot\API', function ($app) {
    return new HelpSpot\API($app->make('HttpClient'));
});
```

#### 绑定实例

你也可以使用 `instance` 方法将现有对象实例绑定到容器中。给定的实例会始终在随后的调用中返回到容器中：

```php
$api = new HelpSpot\API(new HttpClient);

$this->app->instance('HelpSpot\API', $api);
```

#### 绑定基本值

当你有一个类不仅需要接受一个注入类，还需要注入一个基本值（比如整数）。你可以使用上下文绑定来轻松注入你的类需要的任何值：

```php
$this->app->when('App\Http\Controllers\UserController')
          ->needs('$variableName')
          ->give($value);
```

### 绑定接口到实现

服务容器有一个很强大的功能，就是支持绑定接口到给定的实现。例如，如果我们有个 `EventPusher` 接口 和一个 `RedisEventPusher` 实现。一旦我们写完了 `EventPusher` 接口的 `RedisEventPusher` 实现，我们就可以在服务容器中注册它，像这样：

```php
$this->app->bind(
    'App\Contracts\EventPusher',
    'App\Services\RedisEventPusher'
);
```

这么做相当于告诉容器：当一个类需要实现 `EventPusher` 时，应该注入 `RedisEventPusher`。现在我们就可以在构造函数或者任何其他通过服务容器注入依赖项的地方使用类型提示注入 `EventPusher` 接口：

```php
use App\Contracts\EventPusher;

/**
 * Create a new class instance.
 *
 * @param  EventPusher  $pusher
 * @return void
 */
public function __construct(EventPusher $pusher)
{
    $this->pusher = $pusher;
}
```

### 上下文绑定

有时你可能有两个类使用了相同的接口，但你希望各自注入不同的实现。例如， 有两个控制器可能依赖了 `Illuminate\Contracts\Filesystem\Filesystem` [契约](https://learnku.com/docs/laravel/6.x/contracts). Laravel 提供了一个简单的，优雅的接口来定义这个行为：

```php
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\VideoController;
use Illuminate\Contracts\Filesystem\Filesystem;

$this->app->when(PhotoController::class)
          ->needs(Filesystem::class)
          ->give(function () {
              return Storage::disk('local');
          });

$this->app->when([VideoController::class, UploadController::class])
          ->needs(Filesystem::class)
          ->give(function () {
              return Storage::disk('s3');
          });
```

### 标记

有时候，你可能需要解析某个「分类」下的所有绑定。 比如， 你可能正在构建一个报表的聚合器，它接收一个包含不同`Report` 接口实现的数组。注册`Report`实现之后，你可以使用`tag`方法给他们分配一个标签：

```php
$this->app->bind('SpeedReport', function () {
    //
});

$this->app->bind('MemoryReport', function () {
    //
});

$this->app->tag(['SpeedReport', 'MemoryReport'], 'reports');
```

一旦服务被标记，你就可以通过 `tagged`方法轻松地解析它们：

```php
$this->app->bind('ReportAggregator', function ($app) {
    return new ReportAggregator($app->tagged('reports'));
});
```

### 扩展绑定

`extend` 方法可以修改已解析的服务。比如，当一个服务被解析后，你可以添加额外的代码来修饰或者配置它。 `extend` 方法接受一个闭包，该闭包唯一的参数就是这个服务， 并返回修改过的服务：

```php
$this->app->extend(Service::class, function ($service) {
    return new DecoratedService($service);
});
```

## 解析实例

#### `make` 方法

你可以使用 `make` 方法从容器中解析出类实例。 `make` 方法接收你想要解析的类或接口的名字：

```php
$api = $this->app->make('HelpSpot\API');
```

如果你的代码处于无法访问 `$app` 变量的位置，则可用全局辅助函数`resolve`来解析：

```php
$api = resolve('HelpSpot\API');
```

如果类依赖不能通过容器解析，你可以通过将它们作为关联数组作为 `makeWith` 方法的参数注入：

```php
$api = $this->app->makeWith('HelpSpot\API', ['id' => 1]);
```

#### 自动注入

另外，并且更重要的是，你可以简单地使用「类型提示」 的方式在类的构造函数中注入那些需要容器解析的依赖项，包括 [控制器](https://learnku.com/docs/laravel/6.x/controllers)，[事件监听器](https://learnku.com/docs/laravel/6.x/events)， [队列任务](https://learnku.com/docs/laravel/6.x/queues)，[中间件](https://learnku.com/docs/laravel/6.x/middleware)，等 。实际上，这才是大多数对象应该被容器解析的方式。

例如，你可以在控制器的构造函数中添加一个 repository 的类型提示，然后这个 repository 将会被自动解析并注入类中：

```php
<?php

namespace App\Http\Controllers;

use App\Users\Repository as UserRepository;

class UserController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Show the user with the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
}
```

## 容器事件

服务容器每次解析对象会触发一个事件，你可以使用 `resolving` 方法监听这个事件 :

```php
$this->app->resolving(function ($object, $app) {
    // Called when container resolves object of any type...
});

$this->app->resolving(HelpSpot\API::class, function ($api, $app) {
    // Called when container resolves objects of type "HelpSpot\API"...
});
```

正如你所看到的，被解析的对象将会被传入回调函数，这使得你能够在对象被传给调用者之前给它设置额外的属性。

## PSR-11

Laravel 的服务容器实现了 [PSR-11](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-11-container.md) 接口。因此，你可以使用 PSR-11 容器 『接口类型提示』 来获取 Laravel 容器的实例：

```php
use Psr\Container\ContainerInterface;

Route::get('/', function (ContainerInterface $container) {
    $service = $container->get('Service');

    //
});
```

如果无法解析给定的标识符，则将会引发异常。未绑定标识符时，会抛出 `Psr\Container\NotFoundExceptionInterface` 异常。如果标识符已绑定但无法解析，会抛出 `Psr\Container\ContainerExceptionInterface` 异常。

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/con...](https://learnku.com/docs/laravel/6.x/container/5131)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/con...](https://learnku.com/docs/laravel/6.x/container/5131)