本文档最新版为 [10.x](https://learnku.com/docs/laravel/10.x)，旧版本可能放弃维护，推荐阅读最新版！

## 本地化

+   [简介](#introduction)
    +   [区域配置](#configuring-the-locale)
+   [定义翻译字符串](#defining-translation-strings)
    +   [如何使用短键](#using-short-keys)
    +   [如何使用翻译字符串作为键](#using-translation-strings-as-keys)
+   [检索翻译字符串](#retrieving-translation-strings)
    +   [替换翻译字符串中的参数](#replacing-parameters-in-translation-strings)
    +   [复数](#pluralization)
+   [重写扩展包的语言文件](#overriding-package-language-files)

## 简介

Laravel 本地化特性提供了从不同语言文件中检索字符串的简单方法，让您的应用更好地支持多语言开发。语言文件默认都放在 `resources/lang` 目录中。在此目录中，相应的语言文件存放在相应的子目录下，例如：

```php
/resources
    /lang
        /en
            messages.php
        /es
            messages.php
```

所有的语言文件都返回一个键值对数组，例如：

```php
<?php

return [
    'welcome' => 'Welcome to our application'
];
```

### 区域设置

应用的默认语言设置保存在 `config/app.php` 配置文件中。你可以根据需要修改当前设置，还可以使用 `App` Facade 的 `setLocale` 方法动态地更改应用运行过程中使用的语言：

```php
Route::get('welcome/{locale}', function ($locale) {
    App::setLocale($locale);

    //
});
```

你也可以设置『备用语言』，它会在当前语言不包含给定的翻译字符串时被使用。像默认语言一样，备用语言也可以在 `config/app.php` 配置文件中设置：

```php
'fallback_locale' => 'en',
```

#### 确定当前语言环境

你可以使用 `App` Facade的 `getLocale` 和 `isLocale` 方法确定当前的区域设置或者检查语言环境是否为给定值：

```php
$locale = App::getLocale();

if (App::isLocale('en')) {
    //
}
```

## 定义翻译字符串

### 使用短键

通常，翻译字符串都存放在 `resources/lang` 目录下的文件里。在此目录中，但凡应用支持的每种语言都应该有一个对应的子目录：

```php
/resources
    /lang
        /en
            messages.php
        /es
            messages.php
```

所有语言文件都返回键值对数组，例如：

```php
<?php

// resources/lang/en/messages.php

return [
    'welcome' => 'Welcome to our application'
];
```

### 使用翻译字符串作为键

对于有大量翻译需求的应用，如果每条翻译语句都要一一使用 『短键』 来定义，那么当你在视图中尝试去引用这些 『短键』 的时候，很容易变得混乱。因此， Laravel 也支持使用字符串 『默认』 翻译作为关键字来定义翻译字符串。

使用翻译字符串作为键的翻译文件以 JSON 格式存储在 `resources/lang` 目录中。例如，如果你的应用中有西班牙语翻译，你应该在该目录下新建一个 `resources/lang/es.json` 文件：

```php
{
    "I love programming.": "Me encanta programar."
}
```

## 检索翻译字符串

你可以使用辅助函数 `__` 从语言文件中检索， `__` 函数接受翻译字符串所在的文件名加键名作为其第一个参数。例如，我们要检索 `resources/lang/messages.php` 语言文件中的翻译字符串 `welcome` ：

```php
echo __('messages.welcome');

echo __('I love programming.');
```

如果你正使用 [Blade 模板引擎](https://learnku.com/docs/laravel/6.x/blade) ，你可以在视图文件中使用 `{{ }}` 语法或者使用 `@lang` 指令来打印翻译字符串：

```php
{{ __('messages.welcome') }}

@lang('messages.welcome')
```

如果指定的翻译字符串不存在，那么 `__` 函数会直接返回该翻译字符串的键名。所以，如果上述示例中的翻译字符串对应的键值对不存在， `__` 函数将会直接返回 `messages.welcome` 。

> 注意：`@lang` 指令不会对任何输出进行转义。当你使用这个指令时，你必须 **完全由自己承担** 对输出内容的转义工作。

### 翻译字符串中的参数替换

如果需要，你可以在翻译字符串中定义占位符。所有的占位符都有一个 `:` 前缀。例如，你可以使用占位符 name 定义欢迎消息：

```php
'welcome' => 'Welcome, :name',
```

你可以在 `__` 函数中传递一个数组作为第二个参数，它会将数组中的值替换到翻译字符串的占位符中：

```php
echo __('messages.welcome', ['name' => 'dayle']);
```

如果你的占位符中包含了首字母大写或者全部为大写，翻译过来的内容也会做相应的大写处理：

```php
'welcome' => 'Welcome, :NAME', // Welcome, DAYLE
'goodbye' => 'Goodbye, :Name', // Goodbye, Dayle
```

### 复数

复数是一个复杂的问题，因为不同的语言对复数有不同的规则， 使用 『管道符』 `|` ，可以区分字符串的单复数形式：

```php
'apples' => 'There is one apple|There are many apples',
```

你甚至可以创建更复杂的复数规则，为多个数字范围指定翻译字符串：

```php
'apples' => '{0} There are none|[1,19] There are some|[20,*] There are many',
```

在定义具有复数选项的翻译字符串之后，你可以使用 `trans_choice` 函数来检索给定『数量』 的内容。例如, 设置 『总数』 为 10 ，符合数量范围 1 至 19 ，所以会得到 There are some 这条复数语句：

```php
echo trans_choice('messages.apples', 10);
```

你也可以在复数字符串中插入占位符。 `trans_choice` 函数第三个参数所传递数组将会替换占位符：

```php
'minutes_ago' => '{1} :value minute ago|[2,*] :value minutes ago',

echo trans_choice('time.minutes_ago', 5, ['value' => 5]);
```

如果你想让传递给 `trans_choice` 函数的 『数量』 参数显示在翻译字符串中，你可以使用 `:count` 占位符：

```php
'apples' => '{0} There are none|{1} There is one|[2,*] There are :count',
```

## 重写扩展包的语言文件

部分扩展包可能会附带自己的语言文件。你可以通过在 `resources/lang/vendor/{package}/{locale}` 目录放置文件来重写它们，而不要直接修改扩展包的核心文件。

例如，当你需要重写 `skyrim/hearthfire` 扩展包的英语语言文件 `messages.php` ，则需要把文件存放为 `resources/lang/vendor/hearthfire/en/messages.php` 。在这个文件中，你只需要定义你想要修改的翻译字符串。任何没有被重写的翻译字符串仍将从扩展包的原始语言文件中加载。

> 本译文仅用于学习和交流目的，转载请务必注明文章译者、出处、和本文链接  
> 我们的翻译工作遵照 [CC 协议](https://learnku.com/docs/guide/cc4.0/6589)，如果我们的工作有侵犯到您的权益，请及时联系我们。

* * *

> 原文地址：[https://learnku.com/docs/laravel/6.x/loc...](https://learnku.com/docs/laravel/6.x/localization/5148)
> 
> 译文地址：[https://learnku.com/docs/laravel/6.x/loc...](https://learnku.com/docs/laravel/6.x/localization/5148)